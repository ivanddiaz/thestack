terraform {
  required_version = "~> 1.1.7"
  required_providers {
    aws = "~> 4.5.0"
    datadog = {
      source = "DataDog/datadog"
    }
  }
}

provider "datadog" {
  api_key = var.datadog_api_key
  app_key = var.datadog_app_key
  api_url = "https://api.${var.datadog_site}"
}
