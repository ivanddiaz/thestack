<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |
| <a name="provider_datadog"></a> [datadog](#provider\_datadog) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_datadog_secrets"></a> [datadog\_secrets](#module\_datadog\_secrets) | ..//aws/secrets | n/a |
| <a name="module_label"></a> [label](#module\_label) | ..//aws/common-label | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.datadog_aws_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.datadog_aws_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.datadog_aws_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [datadog_integration_aws.sandbox](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/integration_aws) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.datadog_aws_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.datadog_aws_integration_assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_name"></a> [application\_name](#input\_application\_name) | For bucket name as per naming standard | `string` | `"project"` | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS region env config for the provider | `string` | `"us-east-1"` | no |
| <a name="input_company_name"></a> [company\_name](#input\_company\_name) | For bucket name as per naming standard | `string` | `"acme"` | no |
| <a name="input_datadog_api_key"></a> [datadog\_api\_key](#input\_datadog\_api\_key) | An API key is required by the Datadog Agent to submit metrics and events to Datadog | `string` | n/a | yes |
| <a name="input_datadog_app_key"></a> [datadog\_app\_key](#input\_datadog\_app\_key) | Application keys are associated with the user account that created them and by default have the permissions and scopes of the user who created them | `string` | n/a | yes |
| <a name="input_datadog_site"></a> [datadog\_site](#input\_datadog\_site) | Datadog Site - Visit: https://docs.datadoghq.com/getting_started/site/ | `string` | `true` | no |
| <a name="input_datadog_snowflake_password"></a> [datadog\_snowflake\_password](#input\_datadog\_snowflake\_password) | Datadog Snowflake password | `string` | `true` | no |
| <a name="input_email_for_tagging"></a> [email\_for\_tagging](#input\_email\_for\_tagging) | The email address of a group or team that owns the resource for tagging | `string` | `"coyote@acme.com"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | For bucket name as per naming standard | `string` | `"testing"` | no |
| <a name="input_module_name_for_tagging"></a> [module\_name\_for\_tagging](#input\_module\_name\_for\_tagging) | The module that owns the resource for tagging | `string` | `""` | no |
| <a name="input_parameters"></a> [parameters](#input\_parameters) | Keys and values with parameters passed to Sytems Manage Parameter Store | `any` | `{}` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | For bucket name as per naming standard | `string` | `"project"` | no |
| <a name="input_resource_parameters"></a> [resource\_parameters](#input\_resource\_parameters) | Keys and values with parameters passed from resources to Sytems Manage Parameter Store | `any` | `{}` | no |
| <a name="input_stack_for_tagging"></a> [stack\_for\_tagging](#input\_stack\_for\_tagging) | The stack that owns the resource for tagging | `string` | `""` | no |
| <a name="input_use_prefix"></a> [use\_prefix](#input\_use\_prefix) | Add tags to the resource name. Example: /stage/project | `bool` | `true` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
