variable "aws_region" {
  type        = string
  description = "AWS region env config for the provider"
  default     = "us-east-1"
}

variable "company_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "acme"
}

variable "product_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "environment" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "testing"
}

variable "application_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
  default     = "coyote@acme.com"
}

variable "stack_for_tagging" {
  type        = string
  description = "The stack that owns the resource for tagging"
  default     = ""
}

variable "module_name_for_tagging" {
  type        = string
  description = "The module that owns the resource for tagging"
  default     = ""
}

variable "parameters" {
  type        = any
  description = "Keys and values with parameters passed to Sytems Manage Parameter Store"
  default     = {}
}

variable "resource_parameters" {
  type        = any
  description = "Keys and values with parameters passed from resources to Sytems Manage Parameter Store"
  default     = {}
}

variable "use_prefix" {
  type        = bool
  description = "Add tags to the resource name. Example: /stage/project"
  default     = true
}

variable "datadog_api_key" {
  type        = string
  description = "An API key is required by the Datadog Agent to submit metrics and events to Datadog"
}

variable "datadog_app_key" {
  type        = string
  description = "Application keys are associated with the user account that created them and by default have the permissions and scopes of the user who created them"
}

variable "datadog_site" {
  type        = string
  description = "Datadog Site - Visit: https://docs.datadoghq.com/getting_started/site/"
  default     = true
}

variable "datadog_snowflake_password" {
  type        = string
  description = "Datadog Snowflake password"
  default     = true
}
