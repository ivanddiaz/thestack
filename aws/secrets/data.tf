data "aws_region" "current" {}

data "aws_iam_role" "dev_admin_role" {
  name = local.dev_admin_role_name
}

data "aws_iam_user" "terraform_user" {
  user_name = var.terraform_user
}

data "aws_iam_role" "github_oidc_role" {
  name = local.github_oidc_role
}
