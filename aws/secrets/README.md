<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_label"></a> [label](#module\_label) | ..//aws/common-label | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_secretsmanager_secret.aws_secret](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret) | resource |
| [aws_secretsmanager_secret_policy.cluster_info_secret_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_policy) | resource |
| [aws_secretsmanager_secret_version.aws_secret_version](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_version) | resource |
| [random_string.random](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [aws_iam_role.dev_admin_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_role) | data source |
| [aws_iam_role.github_oidc_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_role) | data source |
| [aws_iam_user.terraform_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_user) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_name"></a> [application\_name](#input\_application\_name) | For bucket name as per naming standard | `string` | `"project"` | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS region env config for the provider | `string` | `"us-east-1"` | no |
| <a name="input_company_name"></a> [company\_name](#input\_company\_name) | For bucket name as per naming standard | `string` | `"acme"` | no |
| <a name="input_email_for_tagging"></a> [email\_for\_tagging](#input\_email\_for\_tagging) | The email address of a group or team that owns the resource for tagging | `string` | `"coyote@acme.com"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | For bucket name as per naming standard | `string` | `"undefined-env"` | no |
| <a name="input_policy_enable"></a> [policy\_enable](#input\_policy\_enable) | Restriction Policy Enable | `bool` | `true` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | For bucket name as per naming standard | `string` | `"project"` | no |
| <a name="input_resource_secrets"></a> [resource\_secrets](#input\_resource\_secrets) | Keys and values with parameters passed from resources to AWS Secrets Manage | `map(any)` | `{}` | no |
| <a name="input_secret_name"></a> [secret\_name](#input\_secret\_name) | The string for the secret namepace | `string` | `null` | no |
| <a name="input_secret_recovery_window_days"></a> [secret\_recovery\_window\_days](#input\_secret\_recovery\_window\_days) | Number (in days) for secret recovery after deletion | `number` | `7` | no |
| <a name="input_secrets"></a> [secrets](#input\_secrets) | Keys and values with parameters passed to AWS Secrets Manager | `map(any)` | `{}` | no |
| <a name="input_stack_for_tagging"></a> [stack\_for\_tagging](#input\_stack\_for\_tagging) | The stack that owns the resource for tagging | `string` | `""` | no |
| <a name="input_terraform_user"></a> [terraform\_user](#input\_terraform\_user) | Terraform User | `string` | n/a | yes |
| <a name="input_use_prefix"></a> [use\_prefix](#input\_use\_prefix) | Add tags to the resource name. Example: /stage/project | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_arn"></a> [arn](#output\_arn) | n/a |
| <a name="output_id"></a> [id](#output\_id) | n/a |
| <a name="output_name"></a> [name](#output\_name) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
