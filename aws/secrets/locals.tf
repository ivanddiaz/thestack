locals {
  dev_admin_role_name = "DEVADMIN"
  developer_role_name = "DEVELOPER"
  reader_role_name    = "READONLY"
  github_oidc_role    = "acme-github-oidc"
}
