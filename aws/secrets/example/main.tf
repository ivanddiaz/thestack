module "secret" {
  source                      = "../"
  use_prefix                  = true
  secret_name                 = "unicorn"
  secret_recovery_window_days = 0
  company_name                = "acme"
  environment                 = "testing"
  product_name                = "project-experiments"
  email_for_tagging           = "coyote@acme.com"
  stack_for_tagging           = "aws"
  terraform_user              = "IAC_USER"

  # Use this map as pipeline main input
  secrets = {
    comment     = "Please contact @bill for support"
    description = "Secret SFTP access"
  }

  # Use this auxiliary map for resource generated secrets - keys must match with user_secrets_map
  resource_secrets = {
    username = "root"
    host     = lower("sftp://${random_string.example_host.result}.example.org")
    password = random_password.password.result
  }
}

resource "random_string" "example_host" {
  length  = 5
  special = false
}

resource "random_password" "password" {
  length           = 27
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}


output "arn" {
  value = module.secret.arn
}

output "id" {
  value = module.secret.id
}

output "name" {
  value = module.secret.name
}
