variable "aws_region" {
  type        = string
  description = "AWS region env config for the provider"
  default     = "us-east-1"
}

variable "company_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "acme"
}

variable "product_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "environment" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "undefined-env"
}

variable "application_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
  default     = "coyote@acme.com"
}

variable "stack_for_tagging" {
  type        = string
  description = "The stack that owns the resource for tagging"
  default     = ""
}

variable "secret_name" {
  type        = string
  description = "The string for the secret namepace"
  default     = null
}

variable "secret_recovery_window_days" {
  type        = number
  description = "Number (in days) for secret recovery after deletion"
  default     = 7
}
variable "secrets" {
  type        = map(any)
  description = "Keys and values with parameters passed to AWS Secrets Manager"
  default     = {}
}

variable "resource_secrets" {
  type        = map(any)
  description = "Keys and values with parameters passed from resources to AWS Secrets Manage"
  default     = {}
}

variable "terraform_user" {
  type        = string
  description = "Terraform User"
}

variable "use_prefix" {
  type        = bool
  description = "Add tags to the resource name. Example: /stage/project"
  default     = true
}

variable "policy_enable" {
  type        = bool
  description = "Restriction Policy Enable"
  default     = true
}

