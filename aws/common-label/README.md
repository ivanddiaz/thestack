<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |

## Providers

No providers.

## Modules

No modules.

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additional_tag_map"></a> [additional\_tag\_map](#input\_additional\_tag\_map) | Additional tags for appending to each tag map | `map(string)` | `{}` | no |
| <a name="input_application_name"></a> [application\_name](#input\_application\_name) | Application name, e.g. 'api' | `string` | `""` | no |
| <a name="input_attributes"></a> [attributes](#input\_attributes) | Additional attributes (e.g. `1`) | `list(string)` | `[]` | no |
| <a name="input_company_name"></a> [company\_name](#input\_company\_name) | Organization name or abbreviation, e.g. 'acme' | `string` | `""` | no |
| <a name="input_context"></a> [context](#input\_context) | Default context to use for passing state between label invocations | <pre>object({<br>    region                  = string<br>    company_name            = string<br>    product_name            = string<br>    environment             = string<br>    application_name        = string<br>    module_name             = string<br>    email                   = string<br>    stack                   = string<br>    enabled                 = bool<br>    camel_case              = bool<br>    delimiter               = string<br>    attributes              = list(string)<br>    label_order             = list(string)<br>    tags                    = map(string)<br>    additional_tag_map      = map(string)<br>    regex_replace_chars     = string<br>    regex_replace_chars_att = string<br>  })</pre> | <pre>{<br>  "additional_tag_map": {},<br>  "application_name": "",<br>  "attributes": [],<br>  "camel_case": false,<br>  "company_name": "",<br>  "delimiter": "",<br>  "email": "",<br>  "enabled": true,<br>  "environment": "",<br>  "label_order": [],<br>  "module_name": "",<br>  "product_name": "",<br>  "regex_replace_chars": "",<br>  "regex_replace_chars_att": "",<br>  "region": "",<br>  "stack": "",<br>  "tags": {}<br>}</pre> | no |
| <a name="input_delimiter"></a> [delimiter](#input\_delimiter) | Delimiter to be used between `companyName`,`name`, `environment`, and `attributes` | `string` | `"-"` | no |
| <a name="input_email"></a> [email](#input\_email) | The email address of a group or team that owns the resource for tagging | `string` | `""` | no |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Set to false to prevent the module from creating any resources | `bool` | `true` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment, e.g. 'dev' or 'QA' or 'UAT' or 'PROD' | `string` | `""` | no |
| <a name="input_label_order"></a> [label\_order](#input\_label\_order) | The naming order of the id output and Name tag | `list(string)` | `[]` | no |
| <a name="input_map_migrated_family"></a> [map\_migrated\_family](#input\_map\_migrated\_family) | Stack family for AWS MAP | `string` | `"Puffin"` | no |
| <a name="input_module_name"></a> [module\_name](#input\_module\_name) | Module name, e.g. 'Data-warehouse' | `string` | `""` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | Product name, e.g. 'FB' or 'MC' | `string` | `""` | no |
| <a name="input_regex_replace_chars"></a> [regex\_replace\_chars](#input\_regex\_replace\_chars) | Regex to replace chars with empty string in `companyName`, `environment`, and `productName`. By default only hyphens, letters and digits are allowed, all other chars are removed | `string` | `"/[^a-zA-Z0-9-]/"` | no |
| <a name="input_regex_replace_chars_att"></a> [regex\_replace\_chars\_att](#input\_regex\_replace\_chars\_att) | Regex to replace chars with empty string in `companyName`, `environment`, and `productName`. By default only hyphens, letters and digits are allowed, all other chars are removed | `string` | `"/[^a-zA-Z0-9-@.]/"` | no |
| <a name="input_region"></a> [region](#input\_region) | region for S3 bucket creation , e.g. 'us-east-1' | `string` | `""` | no |
| <a name="input_stack"></a> [stack](#input\_stack) | Stack name for a collection of resources for tagging | `string` | `""` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Additional tags (e.g. `map('BusinessUnit','XYZ')` | `map(string)` | `{}` | no |
| <a name="input_upper_case"></a> [upper\_case](#input\_upper\_case) | Set the resource name in the upper case format | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_attributes"></a> [attributes](#output\_attributes) | List of attributes |
| <a name="output_company_name"></a> [company\_name](#output\_company\_name) | Normalized Company Name |
| <a name="output_context"></a> [context](#output\_context) | Context of this module to pass to other label modules |
| <a name="output_delimiter"></a> [delimiter](#output\_delimiter) | Delimiter between `companyName`, `environment`,  `produtName` and `attributes` |
| <a name="output_email"></a> [email](#output\_email) | Normalized Email |
| <a name="output_environment"></a> [environment](#output\_environment) | Normalized environment |
| <a name="output_id"></a> [id](#output\_id) | Disambiguated ID |
| <a name="output_label_order"></a> [label\_order](#output\_label\_order) | The naming order of the id output and Name tag |
| <a name="output_product_name"></a> [product\_name](#output\_product\_name) | Normalized name |
| <a name="output_region"></a> [region](#output\_region) | Normalized region |
| <a name="output_stack"></a> [stack](#output\_stack) | Normalized stack |
| <a name="output_tags"></a> [tags](#output\_tags) | Normalized Tag map |
| <a name="output_tags_as_list_of_maps"></a> [tags\_as\_list\_of\_maps](#output\_tags\_as\_list\_of\_maps) | Additional tags as a list of maps, which can be used in several AWS resources |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
