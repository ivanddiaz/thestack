locals {

  defaults = {
    label_order = ["region", "companyName", "productName", "environment", "applicationName", "attributes"]
    delimiter   = "-"
    replacement = ""
    # The `sentinel` should match the `regex_replace_chars`, so it will be replaced with the `replacement` value
    sentinel   = "~"
    attributes = [""]
  }

  # The values provided by variables supersede the values inherited from the context
  created_by              = "Bitbucket pipelines through Terraform"
  enabled                 = var.enabled
  upper_case_required     = var.upper_case
  regex_replace_chars     = coalesce(var.regex_replace_chars, var.context.regex_replace_chars)
  regex_replace_chars_att = coalesce(var.regex_replace_chars_att, var.context.regex_replace_chars_att)

  region           = replace(coalesce(var.region, var.context.region, local.defaults.sentinel), local.regex_replace_chars, local.defaults.replacement)
  company_name     = replace(coalesce(var.company_name, var.context.company_name, local.defaults.sentinel), local.regex_replace_chars, local.defaults.replacement)
  product_name     = replace(coalesce(var.product_name, var.context.product_name, local.defaults.sentinel), local.regex_replace_chars, local.defaults.replacement)
  environment      = replace(coalesce(var.environment, var.context.environment, local.defaults.sentinel), local.regex_replace_chars, local.defaults.replacement)
  application_name = replace(coalesce(var.application_name, var.context.application_name, local.defaults.sentinel), local.regex_replace_chars, local.defaults.replacement)
  module_name      = replace(coalesce(var.module_name, var.context.module_name, local.defaults.sentinel), local.regex_replace_chars, local.defaults.replacement)
  email            = replace(coalesce(var.email, var.context.email, local.defaults.sentinel), local.regex_replace_chars_att, local.defaults.replacement)
  stack            = replace(coalesce(var.stack, var.context.stack, local.defaults.sentinel), local.regex_replace_chars, local.defaults.replacement)
  deploy_repo      = lookup(var.additional_tag_map, "deploy_repo", "undefined")

  delimiter   = coalesce(var.delimiter, var.context.delimiter, local.defaults.delimiter)
  label_order = length(var.label_order) > 0 ? var.label_order : (length(var.context.label_order) > 0 ? var.context.label_order : local.defaults.label_order)

  additional_tag_map    = merge(var.context.additional_tag_map, var.additional_tag_map)
  label_migrated_family = lookup(var.additional_tag_map, "map_migrated_family", var.map_migrated_family)

  ## Those variables were implemented to manage AWS costs and should follow the pattern:
  # Flamingo: EC2, EKS > d-server-00wxg2bollfdxu
  # ETL: Glue, S3 > d-server-01xrqnj8kqc8qf
  # Puffin: All the others > d-server-02kfborw3huhr9
  # SVM: all related with SVM 

  puffin   = lower(local.label_migrated_family) == "puffin" ? "d-server-02kfborw3huhr9" : ""
  etl      = lower(local.label_migrated_family) == "etl" ? "d-server-01xrqnj8kqc8qf" : ""
  flamingo = lower(local.label_migrated_family) == "flamingo" ? "d-server-00wxg2bollfdxu" : ""
  svm      = lower(local.label_migrated_family) == "svm" ? "mig2LLFPEBQ82" : ""

  map_migrated = coalesce(local.puffin, local.etl, local.flamingo, local.svm)

  map_migrated_tag = {
    "map-migrated" = local.map_migrated
    "MapProject"   = local.label_migrated_family
  }

  # Merge attributes
  attributes = compact(distinct(concat(var.attributes, var.context.attributes, local.defaults.attributes)))

  tags = merge(var.context.tags, local.generated_tags, var.tags, local.map_migrated_tag)


  tags_as_list_of_maps = flatten([
    for key in keys(local.tags) : merge(
      {
        key   = key
        value = local.tags[key]
    }, var.additional_tag_map)
  ])

  tags_context = {
    # For AWS we need `Name` to be disambiguated since it has a special meaning
    Name        = upper(local.id)
    Product     = upper(local.product_name)
    Environment = upper(local.environment)
    Email       = lower(local.email)
    Stack       = upper(local.stack)
    CreatedBy   = upper(local.created_by)
    Vertical    = upper(local.company_name)
    ModuleName  = upper(local.module_name)
    DeployRepo  = upper(local.deploy_repo)
    //attributes  = local.id_context.attributes
  }

  generated_tags = { for l in keys(local.tags_context) : title(l) => local.tags_context[l] if length(local.tags_context[l]) > 0 }

  id_context = {
    region          = local.upper_case_required ? upper(local.region) : lower(local.region)
    companyName     = local.upper_case_required ? upper(local.company_name) : lower(local.company_name)
    productName     = local.upper_case_required ? upper(local.product_name) : lower(local.product_name)
    environment     = local.upper_case_required ? upper(local.environment) : lower(local.environment)
    applicationName = local.upper_case_required ? upper(local.application_name) : lower(local.application_name)
    moduleName      = local.upper_case_required ? upper(local.module_name) : lower(local.module_name)
    attributes      = local.upper_case_required ? upper(replace(join(local.delimiter, local.attributes), local.regex_replace_chars_att, local.defaults.replacement)) : lower(replace(join(local.delimiter, local.attributes), local.regex_replace_chars_att, local.defaults.replacement))
  }

  labels = [for l in local.label_order : local.id_context[l] if length(local.id_context[l]) > 0]

  id = join(local.delimiter, local.labels)

  # Context of this label to pass to other label modules
  output_context = {
    enabled                 = local.enabled
    region                  = lower(local.region)
    companyName             = lower(local.company_name)
    productName             = upper(local.product_name)
    environment             = lower(local.environment)
    applicationName         = lower(local.application_name)
    moduleName              = lower(local.module_name)
    email                   = lower(local.email)
    stack                   = lower(local.stack)
    attributes              = local.attributes
    createdBy               = local.created_by
    tags                    = local.tags
    delimiter               = local.delimiter
    label_order             = local.label_order
    regex_replace_chars     = local.regex_replace_chars
    regex_replace_chars_att = local.regex_replace_chars_att
    additional_tag_map      = local.additional_tag_map
  }

}
