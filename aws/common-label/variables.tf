variable "region" {
  type        = string
  default     = ""
  description = "region for S3 bucket creation , e.g. 'us-east-1'"
}

variable "company_name" {
  type        = string
  default     = ""
  description = "Organization name or abbreviation, e.g. 'acme'"
}

variable "product_name" {
  type        = string
  default     = ""
  description = "Product name, e.g. 'FB' or 'MC'"
}

variable "application_name" {
  type        = string
  default     = ""
  description = "Application name, e.g. 'api'"
}

variable "module_name" {
  type        = string
  default     = ""
  description = "Module name, e.g. 'Data-warehouse'"
}

variable "environment" {
  type        = string
  default     = ""
  description = "Environment, e.g. 'dev' or 'QA' or 'UAT' or 'PROD'"
}

variable "email" {
  type        = string
  default     = ""
  description = "The email address of a group or team that owns the resource for tagging"
}

variable "stack" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = ""
}

variable "enabled" {
  type        = bool
  default     = true
  description = "Set to false to prevent the module from creating any resources"
}

variable "upper_case" {
  type        = bool
  default     = true
  description = "Set the resource name in the upper case format"
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `companyName`,`name`, `environment`, and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit','XYZ')`"
}

variable "additional_tag_map" {
  type        = map(string)
  default     = {}
  description = "Additional tags for appending to each tag map"
}

variable "context" {
  type = object({
    region                  = string
    company_name            = string
    product_name            = string
    environment             = string
    application_name        = string
    module_name             = string
    email                   = string
    stack                   = string
    enabled                 = bool
    camel_case              = bool
    delimiter               = string
    attributes              = list(string)
    label_order             = list(string)
    tags                    = map(string)
    additional_tag_map      = map(string)
    regex_replace_chars     = string
    regex_replace_chars_att = string
  })
  default = {
    region                  = ""
    company_name            = ""
    product_name            = ""
    environment             = ""
    application_name        = ""
    module_name             = ""
    enabled                 = true
    camel_case              = false
    email                   = ""
    stack                   = ""
    delimiter               = ""
    attributes              = []
    label_order             = []
    tags                    = {}
    additional_tag_map      = {}
    regex_replace_chars     = ""
    regex_replace_chars_att = ""
  }
  description = "Default context to use for passing state between label invocations"
}

variable "label_order" {
  type        = list(string)
  default     = []
  description = "The naming order of the id output and Name tag"
}

variable "regex_replace_chars" {
  type        = string
  default     = "/[^a-zA-Z0-9-]/"
  description = "Regex to replace chars with empty string in `companyName`, `environment`, and `productName`. By default only hyphens, letters and digits are allowed, all other chars are removed"
}


variable "regex_replace_chars_att" {
  type        = string
  default     = "/[^a-zA-Z0-9-@.]/"
  description = "Regex to replace chars with empty string in `companyName`, `environment`, and `productName`. By default only hyphens, letters and digits are allowed, all other chars are removed"
}

## This variable was implemented to manage AWS costs and should follow the pattern:
# Flamingo: EC2, EKS > d-server-00wxg2bollfdxu
# ETL: Glue, S3 > d-server-01xrqnj8kqc8qf
# Puffin: All the others > d-server-02kfborw3huhr9
## Read more at bucket.tf file

variable "map_migrated_family" {
  type        = string
  default     = "Puffin"
  description = "Stack family for AWS MAP"
}
