module "IAM_Role" {
  source           = "../"
  company_name     = ""
  environment      = "testing"
  application_name = ""
  product_name     = "road_runner"
  attributes       = ["S3", "ReadOnlyAccess", "Role"]
  delimiter        = "-"
  email            = "user@company.test"
  tags = {
    "Vertical" = "acme"
  }
}

output "IAM_Role" {
  value = {
    id           = module.IAM_Role.id
    product_name = module.IAM_Role.product_name
    company_name = module.IAM_Role.company_name
    environment  = module.IAM_Role.environment
    attributes   = module.IAM_Role.attributes
    delimiter    = module.IAM_Role.delimiter
  }
}

output "IAM_Role_tags" {
  value = module.IAM_Role.tags
}

output "IAM_Role_context" {
  value = module.IAM_Role.context
}



