module "s3_bucket" {
  source       = "../"
  region       = "us-east-1"
  company_name = "acme"
  environment  = "testing"
  product_name = "acme"
  attributes   = ["data.s3.amazonaws.com"]
  delimiter    = "-"
  email        = "user@company.test"
  upper_case   = false
  tags = {
    "Vertical" = "acme"
  }
}

output "s3_bucket" {
  value = {
    id           = module.s3_bucket.id
    product_name = module.s3_bucket.product_name
    company_name = module.s3_bucket.company_name
    environment  = module.s3_bucket.environment
    attributes   = module.s3_bucket.attributes
    delimiter    = module.s3_bucket.delimiter
  }
}

output "s3_bucket_tags" {
  value = module.s3_bucket.tags
}

output "s3_bucket_context" {
  value = module.s3_bucket.context
}



