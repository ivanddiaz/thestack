module "ec2_label" {
  source           = "../"
  environment      = "testing"
  application_name = "api"
  product_name     = "acme"
  attributes       = ["elb"]
  delimiter        = "-"
  email            = "user@company.test"
  tags = {
    "Vertical" = "acme"
  }
}

output "ec2_label" {
  value = {
    id           = module.ec2_label.id
    product_name = module.ec2_label.product_name
    company_name = module.ec2_label.company_name
    environment  = module.ec2_label.environment
    attributes   = module.ec2_label.attributes
    delimiter    = module.ec2_label.delimiter
  }
}

output "ec2_label_tags" {
  value = module.ec2_label.tags
}

output "ec2_label_context" {
  value = module.ec2_label.context
}



