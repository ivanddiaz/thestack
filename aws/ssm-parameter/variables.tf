variable "aws_region" {
  type        = string
  description = "AWS region env config for the provider"
  default     = "us-east-1"
}

variable "company_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "acme"
}

variable "product_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "environment" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "testing"
}

variable "application_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
  default     = "coyote@acme.com"
}

variable "stack_for_tagging" {
  type        = string
  description = "The stack that owns the resource for tagging"
  default     = ""
}

variable "parameter_basename" {
  type        = string
  description = "The string for the parameter namepace"
  default     = null
}

variable "parameters" {
  type        = any
  description = "Keys and values with parameters passed to Sytems Manage Parameter Store"
  default     = {}
}

variable "resource_parameters" {
  type        = any
  description = "Keys and values with parameters passed from resources to Sytems Manage Parameter Store"
  default     = {}
}

variable "use_prefix" {
  type        = bool
  description = "Add tags to the resource name. Example: /stage/project"
  default     = true
}
