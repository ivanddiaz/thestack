<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_label"></a> [label](#module\_label) | ..//aws/common-label | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_ssm_parameter.aws_parameter](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [random_string.random](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_name"></a> [application\_name](#input\_application\_name) | For bucket name as per naming standard | `string` | `"project"` | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS region env config for the provider | `string` | `"us-east-1"` | no |
| <a name="input_company_name"></a> [company\_name](#input\_company\_name) | For bucket name as per naming standard | `string` | `"acme"` | no |
| <a name="input_email_for_tagging"></a> [email\_for\_tagging](#input\_email\_for\_tagging) | The email address of a group or team that owns the resource for tagging | `string` | `"coyote@acme.com"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | For bucket name as per naming standard | `string` | `"testing"` | no |
| <a name="input_parameter_basename"></a> [parameter\_basename](#input\_parameter\_basename) | The string for the parameter namepace | `string` | `null` | no |
| <a name="input_parameters"></a> [parameters](#input\_parameters) | Keys and values with parameters passed to Sytems Manage Parameter Store | `any` | `{}` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | For bucket name as per naming standard | `string` | `"project"` | no |
| <a name="input_resource_parameters"></a> [resource\_parameters](#input\_resource\_parameters) | Keys and values with parameters passed from resources to Sytems Manage Parameter Store | `any` | `{}` | no |
| <a name="input_stack_for_tagging"></a> [stack\_for\_tagging](#input\_stack\_for\_tagging) | The stack that owns the resource for tagging | `string` | `""` | no |
| <a name="input_use_prefix"></a> [use\_prefix](#input\_use\_prefix) | Add tags to the resource name. Example: /stage/project | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_arns"></a> [arns](#output\_arns) | n/a |
| <a name="output_ids"></a> [ids](#output\_ids) | n/a |
| <a name="output_names"></a> [names](#output\_names) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
