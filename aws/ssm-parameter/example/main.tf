locals {
  password = random_password.example_password.result
}

module "parameter" {
  source             = "../"
  use_prefix         = true
  parameter_basename = "unicorn"
  company_name       = "acme"
  environment        = "testing"
  product_name       = "project-experiments"
  email_for_tagging  = "coyote@acme.com"
  stack_for_tagging  = "aws"

  # Use this map as pipeline main input
  parameters = {
    java_version = {
      value       = "11"
      description = "Java built version"
      type        = "String"
    },
    compatible_formats = {
      value = "CSV,TSV,CLF,ELF,JSON"
      type  = "StringList"
    },
    db_url = {
      value       = "jdbc:oracle:thin:@localhost:1571:MyDBSID"
      description = "JDBC path"
      type        = "SecureString"
    },
    contact = {
      value = "Please contact @larry for support"
      type  = "String"
    }
  }

  # Use this auxiliary map for resource generated parameters - keys must match with user_parameters_map
  resource_parameters = {
    db_user = {
      value       = "admin"
      description = "JDBC user account for connection"
      type        = "String"
    },
    db_password = {
      value = "${local.password}"
      type  = "SecureString"
    }
  }
}

resource "random_password" "example_password" {
  length           = 27
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

output "arns" {
  value = module.parameter.arns
}

output "ids" {
  value = module.parameter.ids
}

output "names" {
  value = module.parameter.names
}
