variable "product_name" {
  type        = string
  description = "Product Name, e.g. 'mc','fb','dm', 'project'"
}
variable "environment" {
  type        = string
  description = "Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod'"
}
variable "application_name" {
  type        = string
  default     = "Application"
  description = "Application name, e.g. 'api'"
}
variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
}
variable "stack_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = "Container-Orchestration"
}
variable "module_name_for_tagging" {
  type        = string
  description = "The module name for tagging"
  default     = ""
}
variable "vertical_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = "acme"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID"
}

variable "eks_version" {
  description = "Desired Kubernetes master version. If you do not specify a value, the latest available version at resource creation is used"
  type        = string
  default     = "1.24"
}

variable "ingress_cidr_blocks" {
  description = "List of IPv4 CIDR ranges to use on all ingress rules"
  type        = list(string)
  default     = []
}

variable "ingress_rules" {
  description = "List of ingress rules to create by name"
  type        = list(string)
  default     = []
}

variable "egress_cidr_blocks" {
  description = "List of IPv4 CIDR ranges to use on all egress rules"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "egress_with_cidr_blocks" {
  description = "List of egress rules to create where 'cidr_blocks' is used"
  type        = list(map(string))
  default     = []
}
