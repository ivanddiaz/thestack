<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_cluster_label"></a> [cluster\_label](#module\_cluster\_label) | ..//aws/common-label | n/a |
| <a name="module_security_group"></a> [security\_group](#module\_security\_group) | ..//aws/sg | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.eks-log-group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_eks_cluster.eks_cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster) | resource |
| [aws_iam_role.eks_cluster_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.eks-master-role-AmazonEKSClusterPolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.eks-master-role-AmazonEKSServicePolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_subnets.private](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnets) | data source |
| [aws_vpc.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_name"></a> [application\_name](#input\_application\_name) | Application name, e.g. 'api' | `string` | `"Application"` | no |
| <a name="input_egress_cidr_blocks"></a> [egress\_cidr\_blocks](#input\_egress\_cidr\_blocks) | List of IPv4 CIDR ranges to use on all egress rules | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_egress_with_cidr_blocks"></a> [egress\_with\_cidr\_blocks](#input\_egress\_with\_cidr\_blocks) | List of egress rules to create where 'cidr\_blocks' is used | `list(map(string))` | `[]` | no |
| <a name="input_eks_version"></a> [eks\_version](#input\_eks\_version) | Desired Kubernetes master version. If you do not specify a value, the latest available version at resource creation is used | `string` | `"1.24"` | no |
| <a name="input_email_for_tagging"></a> [email\_for\_tagging](#input\_email\_for\_tagging) | The email address of a group or team that owns the resource for tagging | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod' | `string` | n/a | yes |
| <a name="input_ingress_cidr_blocks"></a> [ingress\_cidr\_blocks](#input\_ingress\_cidr\_blocks) | List of IPv4 CIDR ranges to use on all ingress rules | `list(string)` | `[]` | no |
| <a name="input_ingress_rules"></a> [ingress\_rules](#input\_ingress\_rules) | List of ingress rules to create by name | `list(string)` | `[]` | no |
| <a name="input_module_name_for_tagging"></a> [module\_name\_for\_tagging](#input\_module\_name\_for\_tagging) | The module name for tagging | `string` | `""` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | Product Name, e.g. 'mc','fb','dm', 'project' | `string` | n/a | yes |
| <a name="input_stack_for_tagging"></a> [stack\_for\_tagging](#input\_stack\_for\_tagging) | Stack name for a collection of resources for tagging | `string` | `"Container-Orchestration"` | no |
| <a name="input_vertical_for_tagging"></a> [vertical\_for\_tagging](#input\_vertical\_for\_tagging) | Stack name for a collection of resources for tagging | `string` | `"acme"` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC ID | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_name"></a> [name](#output\_name) | The name of the Cluster |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
