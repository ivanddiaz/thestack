# ---------------------------------------------------------------------------------------------------------------------
# MODULES
# ---------------------------------------------------------------------------------------------------------------------

module "cluster_label" {
  source           = "..//aws/common-label"
  product_name     = var.product_name
  environment      = var.environment
  application_name = var.application_name
  attributes       = ["EKS", "Cluster"]
  delimiter        = "-"
  email            = var.email_for_tagging
  stack            = var.stack_for_tagging
  module_name      = var.module_name_for_tagging
  tags = {
    Vertical = var.vertical_for_tagging
  }
  map_migrated_family = "Flamingo"
}

# ---------------------------------------------------------------------------------------------------------------------
# IAM ROLES
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_iam_role" "eks_cluster_role" {
  name                 = module.cluster_label.id
  assume_role_policy   = file("config/aws-iam/eks_cluster_assume_role.json")
  max_session_duration = 3600
  tags                 = module.cluster_label.tags
}

resource "aws_iam_role_policy_attachment" "eks-master-role-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_cluster_role.name
}

resource "aws_iam_role_policy_attachment" "eks-master-role-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks_cluster_role.name
}

# ---------------------------------------------------------------------------------------------------------------------
# Security group
# ---------------------------------------------------------------------------------------------------------------------

module "security_group" {
  source                  = "..//aws/sg"
  environment             = var.environment
  product_name            = var.product_name
  application_name        = ""
  vpc_id                  = var.vpc_id
  resource_type           = "EKS"
  email_for_tagging       = var.email_for_tagging
  stack_for_tagging       = var.stack_for_tagging
  description             = "Security Group for EKS Control Panel"
  ingress_cidr_blocks     = var.ingress_cidr_blocks
  ingress_rules           = var.ingress_rules
  egress_cidr_blocks      = var.egress_cidr_blocks
  egress_with_cidr_blocks = var.egress_with_cidr_blocks
}

# ---------------------------------------------------------------------------------------------------------------------
# Control PLane
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_eks_cluster" "eks_cluster" {
  name                      = module.cluster_label.id
  role_arn                  = aws_iam_role.eks_cluster_role.arn
  version                   = var.eks_version
  enabled_cluster_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  vpc_config {
    security_group_ids      = [module.security_group.this_security_group_id]
    subnet_ids              = data.aws_subnets.private.ids
    endpoint_private_access = false
    endpoint_public_access  = true
  }

  tags = merge(
    module.cluster_label.tags,
    {
      "karpenter.sh/discovery" = module.cluster_label.id
    },
  )

  depends_on = [
    aws_iam_role_policy_attachment.eks-master-role-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.eks-master-role-AmazonEKSServicePolicy,
    aws_cloudwatch_log_group.eks-log-group
  ]

  lifecycle {
    ignore_changes = [
      vpc_config
    ]
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# Cloudwatch Log Group
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_cloudwatch_log_group" "eks-log-group" {
  name              = "/aws/eks/${module.cluster_label.id}/cluster"
  retention_in_days = 60
  tags              = module.cluster_label.tags
}
