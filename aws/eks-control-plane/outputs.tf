output "name" {
  description = "The name of the Cluster"
  value       = aws_eks_cluster.eks_cluster.name
}