resource "kubernetes_cluster_role" "developer" {
  metadata {
    name = lower("${var.product_name}:developers")
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    verbs             = ["get", "list", "watch"]
    non_resource_urls = ["*"]
  }
}

resource "kubernetes_cluster_role_binding" "developer" {
  metadata {
    name = lower("${var.product_name}:developer")
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.developer.id
  }
  subject {
    kind      = "User"
    name      = lower("${var.product_name}:developer")
    api_group = "rbac.authorization.k8s.io"
  }
  depends_on = [
    kubernetes_cluster_role.developer
  ]
}

resource "kubernetes_role_v1" "developer" {
  for_each = toset(var.developer_extra_rules_namespaces)

  metadata {
    name      = lower("${var.product_name}:developers")
    namespace = each.key
  }

  rule {
    api_groups = ["*"]
    resources  = ["pods/exec"]
    verbs      = ["create"]
  }

  rule {
    api_groups = ["*"]
    resources  = ["pods"]
    verbs      = ["delete"]
  }
}

resource "kubernetes_role_binding_v1" "developer" {
  for_each = toset(var.developer_extra_rules_namespaces)

  metadata {
    name      = lower("${var.product_name}:developer")
    namespace = each.key
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = lookup(kubernetes_role_v1.developer[each.key].metadata["0"], "name", "invalid")
  }
  subject {
    kind      = "User"
    name      = lower("${var.product_name}:developer")
    api_group = "rbac.authorization.k8s.io"
  }
  depends_on = [
    kubernetes_role_v1.developer
  ]
}

resource "kubernetes_cluster_role" "reader" {
  metadata {
    name = lower("${var.product_name}:readers")
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    verbs             = ["get", "list", "watch"]
    non_resource_urls = ["*"]
  }
}

resource "kubernetes_cluster_role_binding" "reader" {
  metadata {
    name = lower("${var.product_name}:reader")
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.reader.id
  }
  subject {
    kind      = "User"
    name      = lower("${var.product_name}:reader")
    api_group = "rbac.authorization.k8s.io"
  }
  depends_on = [
    kubernetes_cluster_role.reader
  ]
}
