resource "aws_iam_openid_connect_provider" "eks-oidc-provider" {
  url = data.aws_eks_cluster.cluster_data.identity[0].oidc[0].issuer

  client_id_list = [
    "sts.amazonaws.com"
  ]

  thumbprint_list = [data.tls_certificate.cluster_certificate.certificates.0.sha1_fingerprint]
  tags            = module.cluster_label.tags
}

data "tls_certificate" "cluster_certificate" {
  url = data.aws_eks_cluster.cluster_data.identity[0].oidc[0].issuer
}


resource "aws_secretsmanager_secret" "cluster_info_secret" {
  name                    = local.oidc_provider_cluster_secret_key
  description             = "EKS Cluster Secret Information"
  tags                    = module.node_label.tags
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_policy" "cluster_info_secret_policy" {
  secret_arn = aws_secretsmanager_secret.cluster_info_secret.arn
  policy     = <<POLICY
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Deny",
                "Principal": "*",
                "Action": [
                    "secretsmanager:GetSecretValue"
                ],
                "Resource": [
                    "*"
                ],
                "Condition": {
                    "StringNotLike": {
                        "aws:userId": [
                            "${data.aws_iam_role.dev_admin_role.unique_id}:*",
                            "${data.aws_iam_user.terraform_user.user_id}",
                            "${data.aws_iam_role.github_oidc_role.unique_id}:*"
                        ]
                    }
                }
            }
        ]
    }
POLICY
}