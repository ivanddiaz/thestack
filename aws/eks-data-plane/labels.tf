# ---------------------------------------------------------------------------------------------------------------------
# MODULES
# ---------------------------------------------------------------------------------------------------------------------

module "cluster_label" {
  source           = "..//aws/common-label"
  product_name     = var.product_name
  environment      = var.environment
  application_name = var.application_name
  attributes       = [local.label_product_used_by, "Cluster"]
  delimiter        = "-"
  email            = var.email_for_tagging
  stack            = var.stack_for_tagging
  module_name      = var.module_name_for_tagging
  tags = {
    Vertical = var.vertical_for_tagging
  }
  map_migrated_family = "Flamingo"
}

module "node_label" {
  source           = "..//aws/common-label"
  product_name     = var.product_name
  environment      = var.environment
  application_name = var.application_name
  attributes       = [local.label_product_used_by, "Nodes"]
  delimiter        = "-"
  email            = var.email_for_tagging
  stack            = var.stack_for_tagging
  module_name      = var.module_name_for_tagging
  tags = {
    Vertical = var.vertical_for_tagging
  }
  map_migrated_family = "Flamingo"
}

# Tagging Resources
resource "null_resource" "associate_custom_tag_private_subnet" {
  triggers = {
    daily_run = formatdate("YYYYMMDD", time_static.today.rfc3339)
  }
  count = length(data.aws_subnets.private.ids)
  provisioner "local-exec" {
    command = "aws ec2 create-tags --resources ${data.aws_subnets.private.ids[count.index]} --tags Key=kubernetes.io/cluster/${module.cluster_label.id},Value=shared Key=kubernetes.io/role/internal-elb,Value=1 --region ${var.aws_region}"

  }
}

resource "null_resource" "karpenter_associate_custom_tag_private_subnet" {
  triggers = {
    daily_run = formatdate("YYYYMMDD", time_static.today.rfc3339)
  }
  count = length(data.aws_subnets.private.ids)
  provisioner "local-exec" {
    command = "aws ec2 create-tags --resources ${data.aws_subnets.private.ids[count.index]} --tags Key=kubernetes.io/cluster/${data.aws_eks_cluster.cluster_data.name},Value=shared Key=karpenter.sh/discovery,Value=${data.aws_eks_cluster.cluster_data.name} --region ${var.aws_region}"
  }
}

resource "null_resource" "karpenter_associate_custom_tag_cluster_sg" {
  triggers = {
    daily_run = formatdate("YYYYMMDD", time_static.today.rfc3339)
  }
  provisioner "local-exec" {
    command = "aws ec2 create-tags --resources ${data.aws_eks_cluster.cluster_data.vpc_config[0].cluster_security_group_id} --tags Key=karpenter.sh/discovery,Value=${data.aws_eks_cluster.cluster_data.name} --region ${var.aws_region}"
  }
}
