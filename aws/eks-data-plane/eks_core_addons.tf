resource "aws_eks_addon" "kube-proxy" {
  cluster_name      = data.aws_eks_cluster.cluster_data.name
  addon_name        = "kube-proxy"
  addon_version     = var.kube_proxy_version
  tags              = module.cluster_label.tags
  resolve_conflicts = "OVERWRITE"
}

resource "aws_eks_addon" "coredns" {
  cluster_name      = data.aws_eks_cluster.cluster_data.name
  addon_name        = "coredns"
  addon_version     = var.coredns_version
  tags              = module.cluster_label.tags
  resolve_conflicts = "OVERWRITE"
}

resource "aws_eks_addon" "cni" {
  count             = var.vpc_cni_calico_enabled == true ? 0 : 1
  cluster_name      = data.aws_eks_cluster.cluster_data.name
  addon_name        = "vpc-cni"
  addon_version     = var.vpc_cni_aws_version
  tags              = module.cluster_label.tags
  resolve_conflicts = "OVERWRITE"
}

resource "null_resource" "kubectl_install_cni_plugin_calico" {
  count = var.vpc_cni_calico_enabled == true ? 1 : 0
  triggers = {
    version_changes = var.vpc_cni_calico_version
  }
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region $AWS_REGION --name $CLUSTER_NAME;
      kubectl create -f "$MANIFEST_URL" || kubectl replace -f "$MANIFEST_URL"
    EOT
    environment = {
      AWS_REGION   = var.aws_region
      CLUSTER_NAME = data.aws_eks_cluster.cluster_data.name
      MANIFEST_URL = "https://raw.githubusercontent.com/projectcalico/calico/${var.vpc_cni_calico_version}/manifests/tigera-operator.yaml"
    }
  }
}

resource "kubectl_manifest" "cni_plugin_calico" {
  count      = var.vpc_cni_calico_enabled == true ? 1 : 0
  depends_on = [null_resource.kubectl_install_cni_plugin_calico]
  yaml_body  = <<EOF
kind: Installation
apiVersion: operator.tigera.io/v1
metadata:
  name: default
spec:
  kubernetesProvider: EKS
  cni:
    type: Calico
  calicoNetwork:
    bgp: Disabled
  registry: quay.io
EOF
}

resource "null_resource" "kubectl_clean_cni_plugin_calico" {
  count = var.vpc_cni_calico_enabled == true ? 0 : 1
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region $AWS_REGION --name $CLUSTER_NAME;
      kubectl delete -f "$MANIFEST_URL" --force --grace-period=0 || echo "Remove Calico is unsafe. Check command outputs"
    EOT
    environment = {
      AWS_REGION   = var.aws_region
      CLUSTER_NAME = data.aws_eks_cluster.cluster_data.name
      MANIFEST_URL = "https://raw.githubusercontent.com/projectcalico/calico/${var.vpc_cni_calico_version}/manifests/tigera-operator.yaml"

    }
  }
}
