variable "terraform_user" {
  type        = string
  description = "Terraform User"
}

variable "product_name" {
  type        = string
  description = "Product Name, e.g. 'mc','fb','dm', 'project'"
}
variable "environment" {
  type        = string
  description = "Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod'"
}
variable "application_name" {
  type        = string
  default     = "Application"
  description = "Application name, e.g. 'api'"
}
variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
}
variable "stack_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = "Container-Orchestration"
}
variable "module_name_for_tagging" {
  type        = string
  description = "The module name for tagging"
  default     = ""
}
variable "vertical_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = "acme"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID"
}

variable "eks_cluster_name" {
  description = "Name of EKS cluster"
}

variable "node_instance_type" {
  description = "Instance type for eks cluster nodes"
  default     = "c6in.2xlarge"
}

variable "karpenter_max_nodes" {
  description = "Maximum amount of auto-scaled nodes for eks cluster"
  type        = number
  default     = 2
}

variable "node_group_max_nodes" {
  description = "Maximum amount of nodes for eks cluster"
  type        = number
  default     = 2
}

variable "node_volume_size" {
  description = "Size for EBS node volume"
  type        = number
  default     = 250
}

variable "node_ami_type" {
  description = "Type of the AWS EC2 AMI"
  type        = string
  default     = "AL2_x86_64"
}

variable "namespaces" {
  type        = list(string)
  description = "Namespace required for Application specific"
  default     = ["app", "search"]
}

variable "aws_region" {
  description = "aws region to deploy cluster"
  type        = string
  default     = "us-east-1"
}

variable "jfrog_docker_secret" {
  description = "JFROG Docker Secret"
  type        = string
  default     = "jfrog/devops/docker/credential"
}

variable "karpenter_chart_version" {
  type        = string
  description = "Karpenter is an open-source, flexible, high-performance Kubernetes cluster autoscaler built with AWS."
}

variable "kube_proxy_version" {
  type        = string
  description = "Kube-proxy maintains network rules on your nodes and enables network communication to your Pods"
}

variable "coredns_version" {
  type        = string
  description = "CoreDNS is a flexible, extensible DNS server that can serve as the Kubernetes cluster DNS"
}
variable "vpc_cni_aws_version" {
  type        = string
  description = "Amazon VPC CNI plugin for Kubernetes add-on version"
}

variable "vpc_cni_calico_enabled" {
  type        = bool
  description = "Enable or disable the Calico VPC CNI plugin for Kubernetes"
  default     = false
}
variable "vpc_cni_calico_version" {
  type        = string
  description = "Calico VPC CNI plugin for Kubernetes add-on version"
}

variable "vertical_pod_autoscaler_version" {
  type        = string
  description = "Vertical POD autoscaler version"
  default     = "0.11.0"
}

variable "developer_extra_rules_namespaces" {
  type        = list(any)
  description = "Allow pod exec/delete for developer roles"
  default     = []
}
