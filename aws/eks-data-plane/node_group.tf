# ---------------------------------------------------------------------------------------------------------------------
# IAM ROLES
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_iam_role" "eks_node_role" {
  name                 = module.node_label.id
  assume_role_policy   = file("config/aws_iam/eks_nodes_assume_role.json")
  max_session_duration = local.node_group_max_session_duration
  tags                 = module.cluster_label.tags
}

resource "aws_iam_role_policy_attachment" "eks-nodes-role-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_node_role.name
}

resource "aws_iam_role_policy_attachment" "eks-nodes-role-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_node_role.name
}

resource "aws_iam_role_policy_attachment" "eks-nodes-role-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_node_role.name
}

resource "aws_iam_role_policy" "eks-autoscaling-policy" {
  name   = local.node_group_eks_node_autoscaling
  role   = aws_iam_role.eks_node_role.id
  policy = file("config/aws_iam/eks_autoscaling_policy.json")
}

# ---------------------------------------------------------------------------------------------------------------------
# Node Groups
# ---------------------------------------------------------------------------------------------------------------------



resource "aws_eks_node_group" "node_group" {
  lifecycle {
    create_before_destroy = true
  }
  cluster_name = data.aws_eks_cluster.cluster_data.name
  # local.node_group_lifecycle_short_sha - Ensures cluster safe updates, please don't delete it
  node_group_name      = "${local.node_group_base_name}-${local.node_group_lifecycle_short_sha}"
  node_role_arn        = aws_iam_role.eks_node_role.arn
  subnet_ids           = data.aws_subnets.private.ids
  ami_type             = var.node_ami_type
  force_update_version = true
  disk_size            = var.node_volume_size
  instance_types       = [var.node_instance_type]
  scaling_config {
    # That won't scale, Karpenter module will take care of this
    desired_size = var.node_group_max_nodes
    max_size     = var.node_group_max_nodes
    min_size     = var.node_group_max_nodes
  }

  tags = module.node_label.tags

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.eks-nodes-role-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.eks-nodes-role-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.eks-nodes-role-AmazonEC2ContainerRegistryReadOnly,
    aws_eks_addon.cni,
    kubectl_manifest.cni_plugin_calico,
    null_resource.kubectl_install_cni_plugin_calico
  ]
}
