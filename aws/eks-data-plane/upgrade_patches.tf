resource "null_resource" "kubectl_patches_v1-24_to_v1-25" {
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region $AWS_REGION --name $CLUSTER_NAME;
      kubectl apply -f config/patches/1.24-to-1.25/ClusterRoleBinding_eks:addon-cluster-admin.yaml
    EOT
    environment = {
      AWS_REGION   = var.aws_region
      CLUSTER_NAME = data.aws_eks_cluster.cluster_data.name
    }
  }
}
