resource "aws_secretsmanager_secret_version" "cluster_info_secret_version" {
  secret_id = aws_secretsmanager_secret.cluster_info_secret.id
  secret_string = jsonencode({
    "oidc_arn"         = aws_iam_openid_connect_provider.eks-oidc-provider.arn
    "cluster_name"     = data.aws_eks_cluster.cluster_data.name
    "node_role_name"   = aws_iam_role.eks_node_role.name
    "cni_host_network" = var.vpc_cni_calico_enabled == true ? "true" : "false"
  })
}