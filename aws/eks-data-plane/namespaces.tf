
resource "kubernetes_namespace" "namespace" {
  for_each = toset(local.namespaces)
  metadata {
    name = each.key
    labels = {
      name = each.key
    }
  }
}

resource "kubernetes_secret" "jfrog_docker_secrets" {
  for_each = toset(local.namespaces)
  metadata {
    name      = "secret-acme-devops"
    namespace = each.key
  }
  type = "kubernetes.io/dockerconfigjson"
  data = {
    ".dockerconfigjson" = jsonencode({
      "auths" : {
        (jsondecode(data.aws_secretsmanager_secret_version.jfrog_docker_secret_version.secret_string)["docker-server"]) : {
          "username" : local.data_jfrog_user,
          "password" : local.data_jfrog_password,
          "auth" : base64encode("${local.data_jfrog_user}:${local.data_jfrog_password}")
        }
      }
    })
  }
  depends_on = [kubernetes_namespace.namespace]
}
