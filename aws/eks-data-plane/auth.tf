resource "kubectl_manifest" "aws_auth_config" {
  yaml_body = templatefile(
    "config/kubernetes_manifests/aws_auth_config.yml",
    {
      aws_iam_role_eks_node_role_arn               = aws_iam_role.eks_node_role.arn
      data_aws_iam_role_dev_admin_role_arn         = data.aws_iam_role.dev_admin_role.arn
      data_aws_iam_role_github_oidc_role_arn       = data.aws_iam_role.github_oidc_role.arn
      var_product_name                             = lower(var.product_name)
      data_aws_iam_role_developer_role_arn         = data.aws_iam_role.developer_role.arn
      kubernetes_cluster_role_binding_developer_id = kubernetes_cluster_role_binding.developer.id
      data_aws_iam_role_reader_role_arn            = data.aws_iam_role.reader_role.arn
      kubernetes_cluster_role_binding_reader_id    = kubernetes_cluster_role_binding.reader.id
    }
  )
}