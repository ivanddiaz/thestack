module "iam_assumable_role_karpenter" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "4.7.0"
  create_role                   = true
  role_name                     = "karpenter-controller-${data.aws_eks_cluster.cluster_data.name}"
  provider_url                  = data.aws_eks_cluster.cluster_data.identity[0].oidc[0].issuer
  oidc_fully_qualified_subjects = ["system:serviceaccount:karpenter:karpenter"]
  tags = merge(
    module.cluster_label.tags,
    {
      "karpenter.sh/discovery" = module.node_label.id
    },
  )
}

resource "aws_iam_role_policy_attachment" "eks_cni" {
  role       = aws_iam_role.eks_node_role.name
  policy_arn = data.aws_iam_policy.eks_cni.arn
}

resource "aws_iam_role_policy_attachment" "karpenter_eks_worker_node" {
  role       = aws_iam_role.eks_node_role.name
  policy_arn = data.aws_iam_policy.eks_worker_node.arn
}

resource "aws_iam_role_policy_attachment" "karpenter_ec2_container_registry" {
  role       = aws_iam_role.eks_node_role.name
  policy_arn = data.aws_iam_policy.ec2_container_registry.arn
}

resource "aws_iam_role_policy_attachment" "karpenter_ssm_managed_instance" {
  role       = aws_iam_role.eks_node_role.name
  policy_arn = data.aws_iam_policy.ssm_managed_instance.arn
}

resource "aws_iam_instance_profile" "karpenter" {
  name = "KarpenterNodeInstanceProfile-${data.aws_eks_cluster.cluster_data.name}"
  role = aws_iam_role.eks_node_role.name
}

resource "aws_iam_role_policy" "karpenter_controller" {
  name = data.aws_eks_cluster.cluster_data.name
  role = module.iam_assumable_role_karpenter.iam_role_name
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect   = "Allow"
        Resource = "*"
        Action = [
          # Write Operations
          "ec2:CreateFleet",
          "ec2:CreateLaunchTemplate",
          "ec2:CreateTags",
          "ec2:DeleteLaunchTemplate",
          "ec2:RunInstances",
          "ec2:TerminateInstances",
          # Read Operations
          "ec2:DescribeAvailabilityZones",
          "ec2:DescribeImages",
          "ec2:DescribeInstances",
          "ec2:DescribeInstanceTypeOfferings",
          "ec2:DescribeInstanceTypes",
          "ec2:DescribeLaunchTemplates",
          "ec2:DescribeSecurityGroups",
          "ec2:DescribeSpotPriceHistory",
          "ec2:DescribeSubnets",
          "pricing:GetProducts",
          "ssm:GetParameter",
          # Special permission to pass an AWS Organization explicit deny
          "iam:PassRole"
        ]
      },
    ]
  })
}

resource "helm_release" "karpenter" {
  depends_on       = [aws_eks_node_group.node_group]
  namespace        = "karpenter"
  create_namespace = true
  name             = "karpenter"
  repository       = "oci://public.ecr.aws/karpenter"
  chart            = "karpenter"
  version          = var.karpenter_chart_version
  timeout          = 600
  force_update     = true
  set {
    name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = module.iam_assumable_role_karpenter.iam_role_arn
  }
  set {
    name  = "settings.aws.clusterEndpoint"
    value = data.aws_eks_cluster.cluster_data.endpoint
  }
  set {
    name  = "settings.aws.clusterName"
    value = data.aws_eks_cluster.cluster_data.name
  }
  set {
    name  = "settings.aws.defaultInstanceProfile"
    value = "KarpenterNodeInstanceProfile-${data.aws_eks_cluster.cluster_data.name}"
  }
  set {
    name  = "hostNetwork"
    value = var.vpc_cni_calico_enabled == true ? "true" : "false"
  }
}

resource "kubectl_manifest" "karpenter_provisioner_by_type" {
  depends_on = [aws_eks_node_group.node_group, helm_release.karpenter]
  lifecycle { create_before_destroy = true }
  force_new = false
  yaml_body = templatefile(
    local.karpenter_provisioner_file,
    {
      name          = local.karpenter_provider_name
      instance_type = var.node_instance_type
      vcpu_limit    = local.karpenter_cpu_limit
      memory_limit  = local.karpenter_memory_limit
    }
  )
}

resource "kubectl_manifest" "karpenter_node_template" {
  depends_on = [helm_release.karpenter]
  yaml_body = templatefile(
    local.karpenter_node_template_file,
    {
      CLUSTER_NAME    = data.aws_eks_cluster.cluster_data.name
      NODE_NAME       = module.node_label.id
      EBS_VOLUME_SIZE = var.node_volume_size
      PROVIDER        = local.karpenter_provider_name
    }
  )
}

resource "aws_iam_service_linked_role" "spot" {
  aws_service_name = "spot.amazonaws.com"
}
