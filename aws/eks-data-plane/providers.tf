provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster_data.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster_data.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.auth_data.token
}

provider "kubectl" {
  host                   = data.aws_eks_cluster.cluster_data.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster_data.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.auth_data.token
  load_config_file       = false
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster_data.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster_data.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.auth_data.token
  }
}
