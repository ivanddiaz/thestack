resource "null_resource" "vpa_enable_shell" {
  triggers = {
    version_changes = var.vertical_pod_autoscaler_version
  }
  provisioner "local-exec" {
    command = <<-EOT
      wget https://github.com/kubernetes/autoscaler/archive/refs/tags/vertical-pod-autoscaler-$VPA_VERSION.zip;
      unzip -o vertical-pod-autoscaler-$VPA_VERSION.zip;
      cd autoscaler-vertical-pod-autoscaler-$VPA_VERSION/vertical-pod-autoscaler;
      aws eks update-kubeconfig --region $AWS_REGION --name $CLUSTER_NAME;
      ./hack/vpa-down.sh;
      sleep 5;
      ./hack/vpa-up.sh;
    EOT
    environment = {
      AWS_REGION   = var.aws_region
      CLUSTER_NAME = data.aws_eks_cluster.cluster_data.name
      VPA_VERSION  = var.vertical_pod_autoscaler_version
    }
  }
}