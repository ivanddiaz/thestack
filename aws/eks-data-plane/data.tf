# ---------------------------------------------------------------------------------------------------------------------
# DATA SOURCES
# ---------------------------------------------------------------------------------------------------------------------
data "aws_vpc" "selected" {
  id = var.vpc_id
}

data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.selected.id]
  }
  tags = {
    Network = "Private"
  }
}

data "aws_eks_cluster" "cluster_data" {
  name = var.eks_cluster_name
}
data "aws_eks_cluster_auth" "auth_data" {
  name = var.eks_cluster_name
}

# Future: Upgrade to latest AWS Provider to fully automate addon updates
# data "aws_eks_addon_version" "vpc-cni-latest" {
#   addon_name         = "vpc-cni"
#   kubernetes_version = aws_eks_cluster.eks_cluster.version
#   most_recent        = true
# }

data "aws_iam_role" "dev_admin_role" {
  name = local.data_dev_admin_role_name
}

data "aws_iam_role" "developer_role" {
  name = local.data_developer_role_name
}

data "aws_iam_role" "reader_role" {
  name = local.data_reader_role_name
}

data "aws_iam_user" "terraform_user" {
  user_name = var.terraform_user
}

data "aws_iam_role" "github_oidc_role" {
  name = local.data_github_oidc_role_name
}

data "aws_secretsmanager_secret" "jfrog_docker_secret" {
  name = var.jfrog_docker_secret
}

data "aws_secretsmanager_secret_version" "jfrog_docker_secret_version" {
  secret_id = data.aws_secretsmanager_secret.jfrog_docker_secret.id
}

data "aws_iam_policy" "eks_cni" {
  arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}

data "aws_iam_policy" "eks_worker_node" {
  arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
}

data "aws_iam_policy" "ec2_container_registry" {
  arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

data "aws_iam_policy" "ssm_managed_instance" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

data "aws_ec2_instance_type" "instance_data" {
  instance_type = var.node_instance_type
}

# This is to control null resource daily runs
resource "time_static" "today" {}

# ---------------------------------------------------------------------------------------------------------------------
# LOCAL VARIABLES
# ---------------------------------------------------------------------------------------------------------------------
locals {
  data_dev_admin_role_name         = "DEVADMIN"
  data_developer_role_name         = "DEVELOPER"
  data_github_oidc_role_name       = "acme-github-oidc"
  data_jfrog_user                  = jsondecode(data.aws_secretsmanager_secret_version.jfrog_docker_secret_version.secret_string)["docker-username"]
  data_jfrog_password              = jsondecode(data.aws_secretsmanager_secret_version.jfrog_docker_secret_version.secret_string)["docker-password"]
  data_reader_role_name            = "READONLY"
  node_group_suffix                = var.vpc_cni_calico_enabled == true ? "tigera-vpc-cni" : "amazon-vpc-cni"
  label_product_used_by            = "EKS"
  namespace_data                   = jsondecode(file("config/namespaces.json"))
  namespaces                       = [for ns in local.namespace_data.namespaces : ns.namespace_name]
  node_group_max_session_duration  = 3600
  node_group_eks_node_autoscaling  = "EKS-NODE-AUTOSCALING"
  oidc_provider_cluster_secret_key = "${var.environment}/eks/cluster-info"
  karpenter_cpu_limit              = (var.node_group_max_nodes + var.karpenter_max_nodes + 1) * data.aws_ec2_instance_type.instance_data.default_vcpus
  karpenter_memory_limit           = ((var.node_group_max_nodes + var.karpenter_max_nodes + 1) * data.aws_ec2_instance_type.instance_data.memory_size) / 1024
  node_group_base_name             = replace("v${data.aws_eks_cluster.cluster_data.version}-${var.product_name}-${var.application_name}-nodes", ".", "_")
  node_group_lifecycle_short_sha = substr(sha256(join("", [
    data.aws_eks_cluster.cluster_data.name,
    aws_iam_role.eks_node_role.arn,
    jsonencode(data.aws_subnets.private.ids),
    var.node_ami_type,
    var.node_volume_size,
    var.node_instance_type,
    var.node_group_max_nodes,
    local.node_group_suffix,
    ]
  )), 0, 7)
  karpenter_provisioner_file   = "config/kubernetes_manifests/karpenter_provisioner_template.yml"
  karpenter_node_template_file = "config/kubernetes_manifests/karpenter_node_template.yml"
  karpenter_lifecycle_short_sha = substr(sha256(join("", [
    base64sha256(file(local.karpenter_provisioner_file)),
    var.node_instance_type,
    var.karpenter_max_nodes,
    local.node_group_suffix,
    ]
  )), 0, 7)
  karpenter_provider_name = "default-${local.karpenter_lifecycle_short_sha}"
}
