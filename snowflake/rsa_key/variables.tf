variable "sf_application_user" {
  type        = string
  description = "Snowflake Application user name"
}

variable "snowflake_account" {
  type        = string
  description = "Snowflake user name"
}

variable "snowflake_region" {
  type        = string
  description = "Snowflake Role Name"
}

variable "product_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "snowflake"
}

variable "secret_name" {
  type        = string
  description = "The secret key where RSA key will be stored"
}

variable "company_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "acme"
}

variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
  default     = "coyote@acme.com"
}

variable "stack_for_tagging" {
  type        = string
  description = "The stack that owns the resource for tagging"
  default     = ""
}

variable "terraform_user" {
  type        = string
  description = "Terraform User"
  default     = "IAC_USER"
}

variable "environment" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "testing"
}

variable "policy_enable" {
  type        = bool
  description = "Restriction Policy Enable"
  default     = false
}

variable "rotation_period" {
  type        = string
  description = "Days until next keypair rotation"
  default     = "90"
}

variable "aws_region" {
  type        = string
  description = "AWS Region"
  default     = "us-east-1"
}
