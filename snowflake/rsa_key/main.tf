module "secret" {
  source                      = "..//aws/secrets"
  secret_name                 = var.secret_name
  secret_recovery_window_days = 0
  company_name                = var.company_name
  environment                 = var.environment
  product_name                = var.product_name
  email_for_tagging           = var.email_for_tagging
  stack_for_tagging           = var.stack_for_tagging
  terraform_user              = var.terraform_user
  use_prefix                  = false
  secrets = {
    description = "RSA key for Snowflake user"
  }
  resource_secrets = {
    snowflake_ssh_key        = data.external.keypair_rotation.result.key_pem
    snowflake_ssh_key_pkcs8  = data.external.keypair_rotation.result.key_pkcs8
    snowflake_ssh_key2       = data.external.keypair_rotation.result.key2_pem
    snowflake_ssh_key2_pkcs8 = data.external.keypair_rotation.result.key2_pkcs8
    lastRotation             = data.external.keypair_rotation.result.lastRotation
  }
  policy_enable = var.policy_enable
}

resource "snowflake_user_public_keys" "snowflake_user_public_key" {
  name             = var.sf_application_user
  rsa_public_key   = trimspace(replace(replace(base64decode(data.external.keypair_rotation.result.key_pub_pkcs8), "-----END PUBLIC KEY-----", ""), "-----BEGIN PUBLIC KEY-----", ""))
  rsa_public_key_2 = trimspace(replace(replace(base64decode(data.external.keypair_rotation.result.key2_pub_pkcs8), "-----END PUBLIC KEY-----", ""), "-----BEGIN PUBLIC KEY-----", ""))
}
