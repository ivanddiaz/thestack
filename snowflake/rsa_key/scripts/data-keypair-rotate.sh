#!/usr/bin/bash

# Usage: <command> [aws secret name] [rotation period in days] [aws region]
# This command generates a JSON output to populate terraform data
# Any invalid input will trigger new keys generation, terraform must be in charge of validations

# Exit if any of the intermediate steps fail
set -eu

# Inputs
ARG1="${1}" # AWS SECRET NAME
ARG2="${2}" # MAX ROTATION DAYS
ARG3="${3}" # AWS REGION

# Globals
export AWS_DEFAULT_REGION="$ARG3"

# main: Main function
main() {
    local aws_secret_name
    local max_rotation_days
    local last_rotation
    local days_since_last_rotation
    local key_pem
    local key_pkcs8
    local key_pub_pem
    local key_pub_pkcs8
    local key2_pem
    local key2_pkcs8
    local key2_pub_pem
    local key2_pub_pkcs8
    local ssh_key_file
    local ssh_key2_file
    aws_secret_name=$(testIfNull "${1}")
    max_rotation_days=$(testIfNull "${2}")
    if [ "$max_rotation_days" == "null" ]; then
        max_rotation_days=90
    fi
    last_rotation=$(getRotationValue "$aws_secret_name")
    days_since_last_rotation=$(calculateNextRotation "${last_rotation}" "${max_rotation_days}")
    if [ "${days_since_last_rotation}" -ge "${max_rotation_days}" ]; then
        # Key -> Key2
        ssh_key2_file=$(downloadKey "key" "$aws_secret_name")
        key2_pem=$(base64_encode "${ssh_key2_file}".pem)
        key2_pkcs8=$(base64_encode "${ssh_key2_file}".pkcs8)
        key2_pub_pem=$(base64_encode "${ssh_key2_file}".pub.pem)
        key2_pub_pkcs8=$(base64_encode "${ssh_key2_file}".pub.pkcs8)

        # Key -> (New Key)
        ssh_key_file=$(downloadKey "new" "$aws_secret_name")
        key_pem=$(base64_encode "${ssh_key_file}".pem)
        key_pkcs8=$(base64_encode "${ssh_key_file}".pkcs8)
        key_pub_pem=$(base64_encode "${ssh_key_file}".pub.pem)
        key_pub_pkcs8=$(base64_encode "${ssh_key_file}".pub.pkcs8)

        # Update last rotation timestamp
        last_rotation=$(date +%s)
    else
        # Key2 -> (No changes)
        ssh_key2_file=$(downloadKey "key2" "$aws_secret_name")
        key2_pem=$(base64_encode "${ssh_key2_file}".pem)
        key2_pkcs8=$(base64_encode "${ssh_key2_file}".pkcs8)
        key2_pub_pem=$(base64_encode "${ssh_key2_file}".pub.pem)
        key2_pub_pkcs8=$(base64_encode "${ssh_key2_file}".pub.pkcs8)
        # Key -> (No changes)
        ssh_key_file=$(downloadKey "key" "$aws_secret_name")
        key_pem=$(base64_encode "${ssh_key_file}".pem)
        key_pkcs8=$(base64_encode "${ssh_key_file}".pkcs8)
        key_pub_pem=$(base64_encode "${ssh_key_file}".pub.pem)
        key_pub_pkcs8=$(base64_encode "${ssh_key_file}".pub.pkcs8)

        # Last rotation timestamp is untouched
    fi
    jsonOut "${key_pem}" "${key_pkcs8}" "${key_pub_pem}" "${key_pub_pkcs8}" "${key2_pem}" "${key2_pkcs8}" "${key2_pub_pem}" "${key2_pub_pkcs8}" "${last_rotation}"
    rm ./*_snowflake_ssh*
}

# randomStr [lenght]: Generate a pseudo-random string
# output = string
randomStr() {
    local lenght
    lenght="${1}"
    tr -dc '0-9a-zA-Z' </dev/urandom | head -c"${lenght}"
}

# downloadKey [substring] [secret_name]: Download the key
# output = key file path
downloadKey () {
    local substring
    local aws_secret_name
    local random
    local key_base64
    local basename
    substring="${1}"
    random=$(randomStr 13)
    basename="${random}_snowflake_ssh_${substring}"
    aws_secret_name="${2}"
    key_base64=$(getSecretValue "$aws_secret_name" "snowflake_ssh_${substring}")
    if [ "${key_base64}" == "null" ]; then
        ssh-keygen -t rsa -q -f "${basename}" -N "" -C "" -m PEM
    else
        base64_decode "${key_base64}" > "${basename}"
    fi
    cp "${basename}" "${basename}.pem" # Copy of key in PEM format
    chmod 600 "${basename}"*
    ssh-keygen -p -f "${basename}" -N "" -C "" -m PKCS8 >/dev/null # Copy of the ley in PKCS8 format
    cp "${basename}" "${basename}.pkcs8"
    ssh-keygen -y -f "${basename}.pem" -C "" > "${basename}.pub.pem" # Public key
    openssl rsa -in "${basename}.pkcs8" -pubout -out "${basename}.pub.pkcs8"
    echo "${basename}"
}

# base64_encode [file]: Encode an input file
# output = base64 string
base64_encode() {
    local file
    file="${1}"
    base64 -w0 "${file}"
}

# base64_decode [string]: Decode an input string
base64_decode() {
    local string
    string="${1}"
    echo "${string}" | base64 -d
}

# calculateNextRotation [unix timestamp] [days]: Return the number of days remaining to the next rotation cycle.
# Usage: calculateNextRotation "${last_rotation}" "${max_rotation_days}"
# output = Integer value (days)
calculateNextRotation() {
    local max_rotation_days
    local now
    local last_rotation
    local days_since_last_rotation
    now="$(date +%s)"
    last_rotation="${1}"
    max_rotation_days="${2}"
    days_since_last_rotation=$((  $((now - last_rotation)) / 86400  ))
    echo $days_since_last_rotation
}

# getRotationValue [aws_secret_name]: Check if rotation value exists in secret payload. If doesn't exists will trigger a rotation
# output = Integer value with unix time in seconed or 0000000000 if value is invalid
getRotationValue() {
    local aws_secret_name
    local aws_secret_rotation_key
    local rotation_value
    aws_secret_name=$(testIfNull "${1}")
    aws_secret_rotation_key="lastRotation"
    rotation_value=$(getSecretValue "${aws_secret_name}" "${aws_secret_rotation_key}")
    if [ "${rotation_value}" == "null" ]; then
        echo "0000000000"
    else
        echo "${rotation_value}"
    fi
}

# getSecretValue [aws_secret_name] [key]: Query a secret value from secret JSON payload
# output = null or secret value
getSecretValue() {
    local aws_secret_name
    local aws_secret_key
    local aws_secret_value
    aws_secret_name="${1}"
    aws_secret_key="${2}"
    aws_secret_value=$(aws secretsmanager get-secret-value --secret-id "${aws_secret_name}" --query "SecretString" --output text 2>/dev/null || echo 'null')
    if [ "${aws_secret_value}" == null ]; then
        echo "null"
    else
        echo "${aws_secret_value}" | jq -r ."${aws_secret_key}"        
    fi
}

# testIfNull [any]: Check is input variable is void
# output = null or the variable value
testIfNull() {
    if [ "null${1}" == "null" ]; then
        echo "null"
    else
        echo "${1}"
    fi
}

# jsonOut [base-64 pem] [base-64 pkcs8] [base-64 pub pem] [base-64 pub pkcs8] [base-64 pem] [base-64 pkcs8] [base-64 pub pem] [base-64 pub pkcs8] [timestamp]
# output = plain JSON key: value
jsonOut() {
    local key_pem
    local key_pkcs8
    local key_pub_pem
    local key_pub_pkcs8
    local key2_pem
    local key2_pkcs8
    local key2_pub_pem
    local key2_pub_pkcs8
    local last_rotation
    key_pem="${1}"
    key_pkcs8="${2}"
    key_pub_pem="${3}"
    key_pub_pkcs8="${4}"
    key2_pem="${5}"
    key2_pkcs8="${6}"
    key2_pub_pem="${7}"
    key2_pub_pkcs8="${8}"
    last_rotation="${9}"
    jq -n \
        --arg key_pem "${key_pem}" --arg key_pkcs8 "${key_pkcs8}" --arg key_pub_pem "${key_pub_pem}" --arg key_pub_pkcs8 "${key_pub_pkcs8}" \
        --arg key2_pem "${key2_pem}" --arg key2_pkcs8 "${key2_pkcs8}" --arg key2_pub_pem "${key2_pub_pem}" --arg key2_pub_pkcs8 "${key2_pub_pkcs8}" \
        --arg lastRotation "${last_rotation}" \
        '{"key_pem":$key_pem,"key_pkcs8":$key_pkcs8,"key_pub_pem":$key_pub_pem,"key_pub_pkcs8":$key_pub_pkcs8,"key2_pem":$key2_pem,"key2_pkcs8":$key2_pkcs8,"key2_pub_pkcs8":$key2_pub_pkcs8,"key2_pub_pem":$key2_pub_pem,"lastRotation",$lastRotation}'
}

## Main Trap
# This enhance script readability
main "$ARG1" "$ARG2"