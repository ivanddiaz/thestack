locals {
  product_name        = title(var.product_name)
  environment         = title(var.environment)
  company_name        = lower(var.company_name)
  sf_user_mgmt_secret = "/snowflake/user-mgmt/credential"
}

data "external" "keypair_rotation" {
  program = ["bash", "scripts/data-keypair-rotate.sh", var.secret_name, var.rotation_period, var.aws_region]
}

data "aws_secretsmanager_secret" "sf_user_secret" {
  name = local.sf_user_mgmt_secret
}

data "aws_secretsmanager_secret_version" "okta_app_secret_version" {
  secret_id = data.aws_secretsmanager_secret.sf_user_secret.id
}