terraform {
  required_providers {
    snowflake = {
      source  = "Snowflake-Labs/snowflake"
      version = "~> 0.68.2"
    }
  }
  required_version = ">= 0.14.0"
}
