## Create User in snowflake using AccountAdmin and store in AWS Secret Manager

1. Create user using Snowflake Account Admin to manage snwoflake user rsa key by following below steps

```sql
create user "SFUSERMGMT" first_name='Snowflake' last_name='UserMgmt' password='<Fill Password>'  must_change_password = false;
grant role OKTA_PROVISIONER to user "SFUSERMGMT";
```

2. store username, password and role under AWS secrete manager under secret key "/snowflake/user-mgmt/credential"
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14.0 |
| <a name="requirement_snowflake"></a> [snowflake](#requirement\_snowflake) | ~> 0.68.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_external"></a> [external](#provider\_external) | n/a |
| <a name="provider_snowflake"></a> [snowflake](#provider\_snowflake) | ~> 0.68.2 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_secret"></a> [secret](#module\_secret) | ..//aws/secrets | n/a |

## Resources

| Name | Type |
|------|------|
| [snowflake_user_public_keys.snowflake_user_public_key](https://registry.terraform.io/providers/Snowflake-Labs/snowflake/latest/docs/resources/user_public_keys) | resource |
| [aws_secretsmanager_secret.sf_user_secret](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.okta_app_secret_version](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [external_external.keypair_rotation](https://registry.terraform.io/providers/hashicorp/external/latest/docs/data-sources/external) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS Region | `string` | `"us-east-1"` | no |
| <a name="input_company_name"></a> [company\_name](#input\_company\_name) | For bucket name as per naming standard | `string` | `"acme"` | no |
| <a name="input_email_for_tagging"></a> [email\_for\_tagging](#input\_email\_for\_tagging) | The email address of a group or team that owns the resource for tagging | `string` | `"coyote@acme.com"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | For bucket name as per naming standard | `string` | `"testing"` | no |
| <a name="input_policy_enable"></a> [policy\_enable](#input\_policy\_enable) | Restriction Policy Enable | `bool` | `false` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | For bucket name as per naming standard | `string` | `"snowflake"` | no |
| <a name="input_rotation_period"></a> [rotation\_period](#input\_rotation\_period) | Days until next keypair rotation | `string` | `"90"` | no |
| <a name="input_secret_name"></a> [secret\_name](#input\_secret\_name) | The secret key where RSA key will be stored | `string` | n/a | yes |
| <a name="input_sf_application_user"></a> [sf\_application\_user](#input\_sf\_application\_user) | Snowflake Application user name | `string` | n/a | yes |
| <a name="input_snowflake_account"></a> [snowflake\_account](#input\_snowflake\_account) | Snowflake user name | `string` | n/a | yes |
| <a name="input_snowflake_region"></a> [snowflake\_region](#input\_snowflake\_region) | Snowflake Role Name | `string` | n/a | yes |
| <a name="input_stack_for_tagging"></a> [stack\_for\_tagging](#input\_stack\_for\_tagging) | The stack that owns the resource for tagging | `string` | `""` | no |
| <a name="input_terraform_user"></a> [terraform\_user](#input\_terraform\_user) | Terraform User | `string` | `"IAC_USER"` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
