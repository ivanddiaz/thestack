provider "snowflake" {
  username = jsondecode(data.aws_secretsmanager_secret_version.okta_app_secret_version.secret_string)["username"]
  password = jsondecode(data.aws_secretsmanager_secret_version.okta_app_secret_version.secret_string)["password"]
  role     = jsondecode(data.aws_secretsmanager_secret_version.okta_app_secret_version.secret_string)["role"]
  account  = var.snowflake_account
  region   = var.snowflake_region
}
