# ---------------------------------------------------------------------------------------------------------------------
# » Locals
# ---------------------------------------------------------------------------------------------------------------------
locals {

  config_path                          = "${path.module}/config"
  iam_updater_script                   = "scripts/update-snflk-iam-role-trust-policy.sh"
  iam_prefix                           = upper("${var.product_name}-${var.environment}-${var.application_name}-${var.stack_for_tagging}")
  snflk_integ_name                     = upper("${replace(var.application_name, "-", "_")}_${var.environment}")
  snflk_integ_etl_name                 = upper("${replace(var.application_name_etl, "-", "_")}_${var.environment}")
  s3_bucket_name_ext_tbl               = "snflk-ext-table"
  s3_bucket_name_backup                = "snflk-backup"
  s3_bucket_name_query_builder         = "query-builder"
  s3_bucket_name_etl_data_dlvry        = "etl-data-dlvry"
  snflk_stage_basename                 = "s3-ext-table"
  snflk_stage_etl_basename             = "s3-etl-data-dlvry"
  snflk_stage_name                     = upper("${replace(local.snflk_stage_basename, "-", "_")}_${var.environment}")
  snflk_stage_etl_name                 = upper("${replace(local.snflk_stage_etl_basename, "-", "_")}_${var.environment}")
  ssm_parameter_prefix                 = lower("/${var.product_name}/${var.environment}/${var.application_name}")
  snflk_iam_role_trust_policy_template = "${local.config_path}/snflk_iam_role_trust_policy.json"
  snflk_iam_policy_template            = "${local.config_path}/snflk_iam_policy.json"
}

# ---------------------------------------------------------------------------------------------------------------------
# » Label Module
# ---------------------------------------------------------------------------------------------------------------------

module "label" {
  source       = "..//aws/common-label"
  environment  = var.environment
  product_name = var.product_name
  attributes   = [var.product_used_by]
  delimiter    = "-"
  email        = var.email_for_tagging
  stack        = var.stack_for_tagging
  tags = {
    Vertical = var.vertical_for_tagging
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# » S3 Module
# ---------------------------------------------------------------------------------------------------------------------

module "s3_bucket_snflk_backup" {
  source                  = "..//aws/s3"
  region                  = var.aws_region
  company_name            = var.company_name
  product_name            = var.product_name
  environment             = var.environment
  application_name        = local.s3_bucket_name_backup
  email_for_tagging       = var.email_for_tagging
  module_name_for_tagging = var.application_name
  public                  = var.public
  force_destroy           = var.force_destroy
  replication_rules       = var.replication_rules
  s3_replica_bucket_arn   = var.s3_replica_bucket_arn
  s3_replication_role_arn = var.s3_replication_role_arn
  lifecycle_rules         = var.lifecycle_rules
}

module "s3_bucket_snflk_external_table" {
  source                  = "..//aws/s3"
  region                  = var.aws_region
  company_name            = var.company_name
  product_name            = var.product_name
  environment             = var.environment
  application_name        = local.s3_bucket_name_ext_tbl
  email_for_tagging       = var.email_for_tagging
  module_name_for_tagging = var.application_name
  public                  = var.public
  force_destroy           = var.force_destroy
  replication_rules       = var.replication_rules
  s3_replica_bucket_arn   = var.s3_replica_bucket_arn
  s3_replication_role_arn = var.s3_replication_role_arn
  lifecycle_rules         = var.lifecycle_rules
}

module "s3_bucket_query_builder" {
  source                  = "..//aws/s3"
  region                  = var.aws_region
  company_name            = var.company_name
  product_name            = var.product_name
  environment             = var.environment
  application_name        = local.s3_bucket_name_query_builder
  email_for_tagging       = var.email_for_tagging
  module_name_for_tagging = var.application_name
  public                  = var.public
  force_destroy           = var.force_destroy
  replication_rules       = var.replication_rules
  s3_replica_bucket_arn   = var.s3_replica_bucket_arn
  s3_replication_role_arn = var.s3_replication_role_arn
  lifecycle_rules         = var.lifecycle_rules
}

module "s3_bucket_etl_data_dlvry" {
  source                  = "..//aws/s3"
  region                  = var.aws_region
  company_name            = var.company_name
  product_name            = var.product_name
  environment             = var.environment
  application_name        = local.s3_bucket_name_etl_data_dlvry
  email_for_tagging       = var.email_for_tagging
  module_name_for_tagging = var.application_name
  public                  = var.public
  force_destroy           = var.force_destroy
  replication_rules       = var.replication_rules
  s3_replica_bucket_arn   = var.s3_replica_bucket_arn
  s3_replication_role_arn = var.s3_replication_role_arn
  lifecycle_rules         = var.lifecycle_rules
  bucket_name             = "${var.aws_region}-acme-project-${var.environment}-${local.s3_bucket_name_etl_data_dlvry}.s3.amazonaws.com"
}

# ---------------------------------------------------------------------------------------------------------------------
# » Resources
# ---------------------------------------------------------------------------------------------------------------------

# A random integer
resource "random_integer" "random_id" {
  min = 1001
  max = 9999
}

# IAM Role
resource "aws_iam_role" "snflk_iam_role" {
  name        = local.iam_prefix
  description = "This role grant access to Snowflake external accout for process local s3 data"
  assume_role_policy = templatefile(
    local.snflk_iam_role_trust_policy_template,
    {
      snflk_account_id  = data.aws_caller_identity.current.account_id
      snflk_external_id = random_integer.random_id.result
    }
  )
  tags = module.label.tags

  lifecycle {
    ignore_changes = [
      assume_role_policy,
    ]
  }
}

# IAM Policy
resource "aws_iam_policy" "snflk_iam_policy" {
  name = "${local.iam_prefix}-POLICY"
  tags = module.label.tags
  policy = templatefile(
    local.snflk_iam_policy_template,
    {
      bucket_arn            = module.s3_bucket_snflk_backup.s3_bucket_arn
      bucket_arn_alt        = module.s3_bucket_snflk_external_table.s3_bucket_arn
      bucket_query_builder  = module.s3_bucket_query_builder.s3_bucket_arn
      bucket_etl_data_dlvry = module.s3_bucket_etl_data_dlvry.s3_bucket_arn
    }
  )
}

# IAM Policy attachment
resource "aws_iam_policy_attachment" "aws_iam_snflk_attach" {
  name       = "${local.iam_prefix}-POLICY-ATTACHMENT"
  roles      = [aws_iam_role.snflk_iam_role.name]
  policy_arn = aws_iam_policy.snflk_iam_policy.arn
}

# Snowflake provider setup
provider "snowflake" {
  username            = var.snowflake_user
  account             = var.snowflake_account
  region              = var.snowflake_region
  role                = var.snowflake_role
  oauth_client_id     = jsondecode(data.aws_secretsmanager_secret_version.snowflake-credential.secret_string)["SNOWFLAKE_OAUTH_CLIENT_ID"]
  oauth_client_secret = jsondecode(data.aws_secretsmanager_secret_version.snowflake-credential.secret_string)["SNOWFLAKE_OAUTH_CLIENT_SECRET"]
  oauth_refresh_token = data.aws_ssm_parameter.oauth_refresh_token.value
  oauth_endpoint      = jsondecode(data.aws_secretsmanager_secret_version.snowflake-credential.secret_string)["SNOWFLAKE_OAUTH_ENDPOINT"]
  oauth_redirect_url  = jsondecode(data.aws_secretsmanager_secret_version.snowflake-credential.secret_string)["SNOWFLAKE_OAUTH_REDIRECT_URL"]
}

# Snowflake integration DDL
resource "snowflake_storage_integration" "s3_storage_integration" {
  name    = local.snflk_integ_name
  comment = "Snowflake Storage Integration to Access Amazon S3"
  type    = "EXTERNAL_STAGE"
  enabled = true
  storage_allowed_locations = [
    "s3://${module.s3_bucket_snflk_backup.s3_bucket_id}/",
    "s3://${module.s3_bucket_snflk_external_table.s3_bucket_id}/",
    "s3://${module.s3_bucket_query_builder.s3_bucket_id}/"
  ]
  storage_provider     = "S3"
  storage_aws_role_arn = aws_iam_role.snflk_iam_role.arn
}

# Snowflake integration ETL
resource "snowflake_storage_integration" "s3_etl_integration" {
  name    = local.snflk_integ_etl_name
  comment = "Snowflake ETL Data Delivery Integration to Access Amazon S3"
  type    = "EXTERNAL_STAGE"
  enabled = true
  storage_allowed_locations = [
    "s3://${module.s3_bucket_etl_data_dlvry.s3_bucket_id}/"
  ]
  storage_provider     = "S3"
  storage_aws_role_arn = aws_iam_role.snflk_iam_role.arn
}

# Snowflake stage DDL
resource "snowflake_stage" "s3_stage" {
  name                = local.snflk_stage_name
  url                 = "s3://${module.s3_bucket_snflk_external_table.s3_bucket_id}/"
  database            = var.snowflake_stage_integration_database
  schema              = var.snowflake_stage_integration_schema
  storage_integration = snowflake_storage_integration.s3_storage_integration.name
  file_format         = "TYPE = PARQUET NULL_IF = []" # https://github.com/Snowflake-Labs/terraform-provider-snowflake/issues/168
}

# Snowflake stage ETL
resource "snowflake_stage" "s3_stage_etl" {
  name                = local.snflk_stage_etl_name
  url                 = "s3://${module.s3_bucket_etl_data_dlvry.s3_bucket_id}/"
  database            = var.snowflake_stage_integration_etl_database
  schema              = var.snowflake_stage_integration_etl_schema
  storage_integration = snowflake_storage_integration.s3_etl_integration.name
  file_format         = "TYPE = CSV NULL_IF = []"
}

# Resource permissions
resource "snowflake_integration_grant" "grant" {
  depends_on = [
    snowflake_role_grants.role_grants,
    snowflake_storage_integration.s3_storage_integration,
  ]
  integration_name = snowflake_storage_integration.s3_storage_integration.name

  privilege         = "USAGE"
  roles             = var.snowflake_storage_integration_ro_roles
  with_grant_option = false
}

resource "snowflake_integration_grant" "grant_etl" {
  depends_on = [
    snowflake_role_grants.role_grants,
    snowflake_storage_integration.s3_etl_integration,
  ]
  integration_name = snowflake_storage_integration.s3_etl_integration.name

  privilege         = "USAGE"
  roles             = var.snowflake_storage_integration_ro_roles
  with_grant_option = false
}

resource "snowflake_stage_grant" "grant" {
  depends_on = [
    snowflake_role_grants.role_grants,
    snowflake_stage.s3_stage,
  ]
  database_name     = var.snowflake_stage_integration_database
  schema_name       = var.snowflake_stage_integration_schema
  stage_name        = snowflake_stage.s3_stage.name
  privilege         = "USAGE"
  roles             = var.snowflake_storage_integration_ro_roles
  with_grant_option = false
}

resource "snowflake_stage_grant" "grant_etl" {
  depends_on = [
    snowflake_role_grants.role_grants,
    snowflake_stage.s3_stage_etl,
  ]
  database_name     = var.snowflake_stage_integration_etl_database
  schema_name       = var.snowflake_stage_integration_etl_schema
  stage_name        = snowflake_stage.s3_stage_etl.name
  privilege         = "USAGE"
  roles             = var.snowflake_storage_integration_ro_roles
  with_grant_option = false
}


# Update snflk_iam_role using awscli
resource "null_resource" "update-snflk_iam_role-trust-policy" {
  triggers = {
    storage_aws_iam_user_arn_changes             = snowflake_storage_integration.s3_storage_integration.storage_aws_iam_user_arn
    aws_iam_user_arn_changes                     = snowflake_storage_integration.s3_storage_integration.storage_aws_external_id
    snflk_iam_role_trust_policy_template_changes = local.snflk_iam_role_trust_policy_template
    snflk_iam_role_id_changes                    = aws_iam_role.snflk_iam_role.id
  }
  provisioner "local-exec" {
    command = "$(which bash) ${local.iam_updater_script} \"${snowflake_storage_integration.s3_storage_integration.storage_aws_iam_user_arn}\" \"${snowflake_storage_integration.s3_storage_integration.storage_aws_external_id}\" \"${local.snflk_iam_role_trust_policy_template}\" \"${aws_iam_role.snflk_iam_role.id}\""
  }
  depends_on = [
    snowflake_storage_integration.s3_storage_integration,
    snowflake_storage_integration.s3_etl_integration
  ]
}
