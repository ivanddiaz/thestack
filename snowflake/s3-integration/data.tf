# ---------------------------------------------------------------------------------------------------------------------
# » Conditional Expressions
# ---------------------------------------------------------------------------------------------------------------------

data "aws_caller_identity" "current" {}

# Snowflake OAUTH Credentials
data "aws_secretsmanager_secret" "snowflake-credential-metadata" {
  name = var.snowflake_credential_name
}
data "aws_secretsmanager_secret_version" "snowflake-credential" {
  secret_id = data.aws_secretsmanager_secret.snowflake-credential-metadata.id
}

data "aws_ssm_parameter" "oauth_refresh_token" {
  name = var.snowflake_refresh_token_key
}