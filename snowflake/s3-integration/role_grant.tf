locals {
  role_creation = { for role in var.roles : role.access_role_name => role }
}

resource "snowflake_role" "role" {
  for_each = local.role_creation
  name     = upper(each.key)
  comment  = each.value.comment
}

resource "snowflake_role_grants" "role_grants" {
  depends_on = [
    snowflake_role.role,
  ]
  for_each  = local.role_creation
  role_name = each.key
  roles     = each.value.grant_to_function_roles
}
