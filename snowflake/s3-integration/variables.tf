# AWS Provider
variable "aws_region" {
  type        = string
  description = "AWS region env config for the provider"
  default     = "us-east-1"
}

# Module: S3
variable "enabled" {
  description = "Is bucket enabled for creation?"
  default     = true
}

variable "force_destroy" {
  description = "Delete all objects in bucket on destroy"
  default     = false
}

variable "public" {
  description = "Allow public read access to bucket"
  default     = false
}

variable "s3_replication_enabled" {
  type        = bool
  default     = false
  description = "Set this to true and specify `s3_replica_bucket_arn` to enable replication. `versioning_enabled` must also be `true`."
}

variable "s3_replica_bucket_arn" {
  type        = string
  default     = ""
  description = "The ARN of the S3 replica bucket (destination)"
}

variable "s3_replication_role_arn" {
  type        = string
  default     = ""
  description = "The ARN of the IAM role for S3 replication bucket"
}

variable "replication_rules" {
  type = list(object({
    id       = string
    priority = number
    prefix   = string
    status   = string
    destination = object({
      storage_class      = string
      replica_kms_key_id = string
      access_control_translation = object({
        owner = string
      })
      account_id = string
    })
    source_selection_criteria = object({
      sse_kms_encrypted_objects = object({
        enabled = string
      })
    })
    filter = object({
      prefix = string
      tags = object({
        key   = string
        value = string
      })
    })
  }))
  #type        = list(any)
  default     = null
  description = "Specifies the replication rules if S3 bucket replication is enabled"
}
variable "lifecycle_rules" {
  description = "A data structure to create lifcycle rules"
  type = list(object({
    id                                     = string
    prefix                                 = string
    tags                                   = map(string)
    status                                 = string
    abort_incomplete_multipart_upload_days = number
    expiration_config = list(object({
      days                         = number
      expired_object_delete_marker = bool
    }))
    noncurrent_version_expiration_config = list(object({
      days = number
    }))
    transitions_config = list(object({
      days          = number
      storage_class = string
    }))
    noncurrent_version_transitions_config = list(object({
      days          = number
      storage_class = string
    }))
  }))
  default = []
}

# Tags & Labels
variable "product_name" {
  type        = string
  description = "Product Name, e.g. 'mc','fb','dm'"
  default     = "genericproduct"
}

variable "environment" {
  type        = string
  description = "Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod'"
  default     = "sandbox"
}

variable "application_name" {
  type        = string
  description = "Application name, e.g. 'api'"
  default     = "genericapp"
}

variable "application_name_etl" {
  type        = string
  description = "Application name, e.g. 'api'"
  default     = "genericapp"
}

variable "email_for_tagging" {
  type        = string
  description = "The email address for tagging the resources"
  default     = "none@acme.xyz"
}

variable "stack_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = "genericstack"
}

variable "company_name" {
  type        = string
  description = "Company name as per naming standard for resources"
  default     = "acme"
}

variable "product_used_by" {
  type        = string
  description = "Product used by [team/app]"
  default     = "genericdependency"
}

variable "vertical_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = "acme"
}

# Snowflake
variable "snowflake_user" {
  type        = string
  description = "Snowflake user name"
}

variable "snowflake_account" {
  type        = string
  description = "Snowflake user name"
}

variable "snowflake_role" {
  type        = string
  description = "Snowflake Role Name"
}

variable "snowflake_region" {
  type        = string
  description = "Snowflake Role Name"
}

variable "snowflake_credential_name" {
  type        = string
  description = "Snowflake Credential Name which is stored in AWS secret manager"
}

variable "snowflake_refresh_token_key" {
  type        = string
  description = "Snowflake Refresh Token SSM Parameter key"
}

variable "roles" {
  type = list(object({
    access_role_name        = string
    comment                 = string
    grant_to_function_roles = list(string)
    }
  ))
  default     = []
  description = <<EOF
Snowflake access role which needs to be granted to function roles:
[
  {
  access_role_name = <access role name>
  comment = <description of role grant>
  grant_to_function_roles = <list of function roles>
  }
]
EOF
}

variable "snowflake_storage_integration_ro_roles" {
  type        = list(string)
  description = "List of roles which need USAGE access to s3 integration"
}

variable "snowflake_stage_integration_database" {
  type        = string
  description = "The database in which to create the stage."
  default     = "PELICAN_DB"
}

variable "snowflake_stage_integration_schema" {
  type        = string
  description = "The schema in which to create the stage."
  default     = "ETL_S"
}

variable "snowflake_stage_integration_etl_database" {
  type        = string
  description = "The database in which to create the stage."
  default     = "PELICAN_DB"
}

variable "snowflake_stage_integration_etl_schema" {
  type        = string
  description = "The schema in which to create the stage."
  default     = "ETL_S"
}
