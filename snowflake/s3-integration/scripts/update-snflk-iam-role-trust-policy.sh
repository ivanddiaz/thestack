#!/bin/bash
# shellcheck disable=SC2001
# A complex char substitution was made
#
# Shell control behavior, can't use -u void variables are expected
set -e

# Locals
TMPTEMPLATE="$(mktemp)"
ARGVCOUNT=0

# Test 1st argument, an ARN is expected
if  [ "$1null" != "null" ] ; then  
  echo "Snowflake ARN expected in ........... ARGV [1] = $1"  
else 
  ARGVCOUNT=$((ARGVCOUNT +1))
fi

# Test 2nd argument, an EXTERNAL-ID is expected
if  [ "$2null" != "null" ] ; then  
  echo "Snowflake EXTERNAL-ID expected in ... ARGV [2] = $2"  
else 
  ARGVCOUNT=$((ARGVCOUNT +1))
fi

# Test 3rd argument, a FILE path is expected
if  [ "$3null" != "null" ] ; then  
  echo "Trust policy template in ............ ARGV [3] = $3"  
else 
  ARGVCOUNT=$((ARGVCOUNT +1))
fi

# Test 4th argument, a IAM ROLE NAME path is expected
if  [ "$4null" != "null" ] ; then  
  echo "Local IAM Role name file in ......... ARGV [4] = $4"  
else 
  ARGVCOUNT=$((ARGVCOUNT +1))
fi

# Command will exit as OK if arguments aren't provided
# That will be useful on destroy cases
if  [ $ARGVCOUNT -gt 0 ] ; then
  echo "Usage: $0 [arn] [external-id] [policy-template] [iam-role-name]"
  exit 0
fi

# Using a temporal file for the modified template
cp "$3" "$TMPTEMPLATE"

# Remove $
sed -i 's/\$//g' "$TMPTEMPLATE"

# File string substitutions
ARN=$(sed 's/[^[:alnum:]]/\\&/g' <<<"$1")
sed -i 's/{snflk_account_id}/'"$ARN"'/g' "$TMPTEMPLATE"

EXTERNALID=$(sed 's/[^[:alnum:]]/\\&/g' <<<"$2")
sed -i 's/{snflk_external_id}/'"$EXTERNALID"'/g' "$TMPTEMPLATE"

# Echo template
echo "Calculated template:"
cat $TMPTEMPLATE; echo ""; echo ""

# Execute aws cli
echo "Running AWS CLI..."
RUNCMD="aws iam update-assume-role-policy --role-name $4 --policy-document file://$TMPTEMPLATE"
echo "$RUNCMD"
$RUNCMD
echo "AWS cli command OK"

# End
echo "Task completed!"
