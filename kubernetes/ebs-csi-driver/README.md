<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | n/a |
| <a name="provider_kubectl"></a> [kubectl](#provider\_kubectl) | n/a |
| <a name="provider_null"></a> [null](#provider\_null) | n/a |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_ebs_role_label"></a> [ebs\_role\_label](#module\_ebs\_role\_label) | ..//aws/common-label | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_role.ebs_iam_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.sa_iam_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [helm_release.ebs-driver](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubectl_manifest.storage_class](https://registry.terraform.io/providers/gavinbunney/kubectl/latest/docs/resources/manifest) | resource |
| [null_resource.allow_volume_expansion](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.set_gp3_default_storage_class](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [time_sleep.wait_30_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |
| [aws_eks_cluster.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster) | data source |
| [aws_eks_cluster_auth.cluster_auth](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth) | data source |
| [aws_iam_policy_document.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_secretsmanager_secret.eks_cluster_secret](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.eks_cluster_secret_version](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_name"></a> [application\_name](#input\_application\_name) | Application name, e.g. 'api' | `string` | `"EKS"` | no |
| <a name="input_aws_ebs_csi_chart_version"></a> [aws\_ebs\_csi\_chart\_version](#input\_aws\_ebs\_csi\_chart\_version) | Chart version | `string` | n/a | yes |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS Region | `string` | n/a | yes |
| <a name="input_cluster_secret_key"></a> [cluster\_secret\_key](#input\_cluster\_secret\_key) | EKS Cluster Secret Key | `string` | `""` | no |
| <a name="input_email_for_tagging"></a> [email\_for\_tagging](#input\_email\_for\_tagging) | The email address of a group or team that owns the resource for tagging | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod' | `string` | n/a | yes |
| <a name="input_module_name_for_tagging"></a> [module\_name\_for\_tagging](#input\_module\_name\_for\_tagging) | The module name for tagging | `string` | `""` | no |
| <a name="input_namespace_service_accounts"></a> [namespace\_service\_accounts](#input\_namespace\_service\_accounts) | NameSpace and Service Account | `list(string)` | <pre>[<br>  "kube-system:ebs-csi-controller-sa",<br>  "kube-system:ebs-csi-node-sa"<br>]</pre> | no |
| <a name="input_ns"></a> [ns](#input\_ns) | Namespace | `string` | `"kube-system"` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | Product Name, e.g. 'mc','fb','dm', 'project' | `string` | n/a | yes |
| <a name="input_replica"></a> [replica](#input\_replica) | No of replica | `number` | `1` | no |
| <a name="input_stack_for_tagging"></a> [stack\_for\_tagging](#input\_stack\_for\_tagging) | Stack name for a collection of resources for tagging | `string` | `""` | no |
| <a name="input_vertical_for_tagging"></a> [vertical\_for\_tagging](#input\_vertical\_for\_tagging) | Stack name for a collection of resources for tagging | `string` | `"acme"` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
