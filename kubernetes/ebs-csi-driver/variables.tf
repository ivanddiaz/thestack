variable "product_name" {
  type        = string
  description = "Product Name, e.g. 'mc','fb','dm', 'project'"
}
variable "environment" {
  type        = string
  description = "Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod'"
}
variable "application_name" {
  type        = string
  default     = "EKS"
  description = "Application name, e.g. 'api'"
}
variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
}
variable "stack_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = ""
}
variable "module_name_for_tagging" {
  type        = string
  description = "The module name for tagging"
  default     = ""
}
variable "vertical_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = "acme"
}

variable "aws_ebs_csi_chart_version" {
  type        = string
  description = "Chart version"
}

variable "ns" {
  type        = string
  description = "Namespace"
  default     = "kube-system"
}

variable "replica" {
  type        = number
  description = "No of replica"
  default     = 1
}

variable "aws_region" {
  type        = string
  description = "AWS Region"
}

variable "cluster_secret_key" {
  description = "EKS Cluster Secret Key"
  type        = string
  default     = ""
}
/*variable "cluster_name" {
  type        = string
  description = "Name of the EKS Cluster"
}
variable "oidc_arn" {
  type        = string
  description = "OIDC Provider ARN"
}*/

variable "namespace_service_accounts" {
  type        = list(string)
  description = "NameSpace and Service Account"
  default     = ["kube-system:ebs-csi-controller-sa", "kube-system:ebs-csi-node-sa"]
}
