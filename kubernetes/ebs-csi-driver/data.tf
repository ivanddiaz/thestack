data "aws_secretsmanager_secret" "eks_cluster_secret" {
  name = local.cluster_secret_key
}

data "aws_secretsmanager_secret_version" "eks_cluster_secret_version" {
  secret_id = data.aws_secretsmanager_secret.eks_cluster_secret.id
}


data "aws_eks_cluster" "cluster" {
  name = local.cluster_name
}

data "aws_eks_cluster_auth" "cluster_auth" {
  name = data.aws_eks_cluster.cluster.name
}

locals {
  config_path          = "./config"
  max_session_duration = 36000
  cluster_secret_key   = (var.cluster_secret_key != "") ? var.cluster_secret_key : "${var.environment}/eks/cluster-info"
  oidc_arn             = jsondecode(data.aws_secretsmanager_secret_version.eks_cluster_secret_version.secret_string)["oidc_arn"]
  cluster_name         = jsondecode(data.aws_secretsmanager_secret_version.eks_cluster_secret_version.secret_string)["cluster_name"]
}