resource "helm_release" "ebs-driver" {
  name         = "aws-ebs-csi-driver"
  chart        = "https://github.com/kubernetes-sigs/aws-ebs-csi-driver/releases/download/helm-chart-aws-ebs-csi-driver-${var.aws_ebs_csi_chart_version}/aws-ebs-csi-driver-${var.aws_ebs_csi_chart_version}.tgz"
  namespace    = var.ns
  force_update = true

  values = [
    templatefile("values/default_value.yaml", { sa_role = aws_iam_role.ebs_iam_role.arn })
  ]
  set {
    name  = "controller.serviceAccount.create"
    value = "true"
  }

  set {
    name  = "replicaCount"
    value = var.replica
  }

  depends_on = [
    aws_iam_role.ebs_iam_role
  ]
}

resource "time_sleep" "wait_30_seconds" {
  depends_on      = [helm_release.ebs-driver]
  create_duration = "30s"
  triggers = {
    always_run = timestamp()
  }
}

resource "null_resource" "allow_volume_expansion" {
  depends_on = [helm_release.ebs-driver, time_sleep.wait_30_seconds]

  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region $AWS_REGION --name $CLUSTER_NAME;
      kubectl patch sc gp2 -p '{"allowVolumeExpansion": true}'
    EOT
    environment = {
      AWS_REGION   = var.aws_region
      CLUSTER_NAME = nonsensitive(data.aws_eks_cluster.cluster.name)
    }
  }
}

resource "kubectl_manifest" "storage_class" {
  depends_on = [helm_release.ebs-driver, time_sleep.wait_30_seconds]
  for_each   = toset(["gp3", "io2"])
  yaml_body = templatefile("manifests/storage_class.yaml", {
    type = each.key
  })
}

resource "null_resource" "set_gp3_default_storage_class" {
  depends_on = [
    helm_release.ebs-driver,
    time_sleep.wait_30_seconds,
    kubectl_manifest.storage_class
  ]
  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region $AWS_REGION --name $CLUSTER_NAME;
      kubectl patch sc gp3 -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
    EOT
    environment = {
      AWS_REGION   = var.aws_region
      CLUSTER_NAME = nonsensitive(data.aws_eks_cluster.cluster.name)
    }
  }
}



module "ebs_role_label" {
  source           = "..//aws/common-label"
  product_name     = var.product_name
  environment      = var.environment
  application_name = var.application_name
  attributes       = ["EBS-CSI-DRIVER", "SA"]
  delimiter        = "-"
  email            = var.email_for_tagging
  stack            = var.stack_for_tagging
  module_name      = var.module_name_for_tagging
  tags = {
    Vertical = var.vertical_for_tagging
  }
}

data "aws_iam_policy_document" "this" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [local.oidc_arn]
    }
    condition {
      test     = "StringEquals"
      variable = "${replace(local.oidc_arn, "/^(.*provider/)/", "")}:sub"
      values   = [for sa in var.namespace_service_accounts : "system:serviceaccount:${sa}"]
    }
  }
}


resource "aws_iam_role" "ebs_iam_role" {
  name                 = module.ebs_role_label.id
  assume_role_policy   = data.aws_iam_policy_document.this.json
  max_session_duration = local.max_session_duration
  tags                 = module.ebs_role_label.tags
}


resource "aws_iam_role_policy" "sa_iam_role" {
  name   = module.ebs_role_label.id
  role   = aws_iam_role.ebs_iam_role.id
  policy = file("${local.config_path}/ebs_driver_policy.json")
}
