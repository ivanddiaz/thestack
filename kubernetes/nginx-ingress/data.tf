locals {
  cf_sg_ssm          = "/cf/sg_list"
  cluster_secret_key = (var.cluster_secret_key != "") ? var.cluster_secret_key : "${var.environment}/eks/cluster-info"
  cluster_name       = jsondecode(data.aws_secretsmanager_secret_version.eks_cluster_info.secret_string)["cluster_name"]
  cni_host_network   = jsondecode(data.aws_secretsmanager_secret_version.eks_cluster_info.secret_string)["cni_host_network"]

  #certificate_arn    = var.certificate_arn != "" ? var.certificate_arn : jsondecode(data.aws_secretsmanager_secret_version.certificate_arn_version.secret_string)["certificate_arn"]
  ssm_alb_name = var.ssm_host_path != "" ? var.ssm_host_path : "/alb/internal"
  availability_zone_subnets = {
    for s in data.aws_subnet.private : s.availability_zone => s.id...
  }
}

data "aws_secretsmanager_secret" "eks_cluster_secret" {
  name = local.cluster_secret_key
}

data "aws_secretsmanager_secret_version" "eks_cluster_info" {
  secret_id = data.aws_secretsmanager_secret.eks_cluster_secret.id
}
data "aws_eks_cluster" "cluster" {
  name = local.cluster_name
}

data "aws_eks_cluster_auth" "cluster_auth" {
  name = data.aws_eks_cluster.cluster.name
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

data "aws_subnets" "private" {

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.selected.id]
  }
  tags = {
    Network = "Private"
  }
}

data "aws_subnet" "private" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.key
}

data "aws_ssm_parameter" "cf_sg" {
  count = var.internal_enable ? 0 : 1
  name  = local.cf_sg_ssm
}

/*data "aws_route53_zone" "sub_domain" {
  count        = var.internal_enable ? 1 : 0
  name         = var.sub_domain_name
  private_zone = false
}*/

data "kubernetes_service" "ingress_svc" {
  metadata {
    name      = "${var.helm_name}-ingress-nginx-controller"
    namespace = var.namespace
  }
  depends_on = [
    helm_release.nginx-ingress
  ]
  provider = kubernetes
}

data "aws_ssm_parameter" "certificate_arn" {
  name = var.certificate_secret_key
}
