variable "helm_name" {
  description = "Helm Name"
  type        = string
  default     = "nginx-ingress"
}

variable "cluster_secret_key" {
  description = "EKS Cluster Secret Key"
  type        = string
  default     = ""
}

variable "application_name" {
  type        = string
  description = "Application Name"
}

variable "chart_name" {
  type        = string
  description = "Chart name"
  default     = "ingress-nginx"
}

variable "chart_version" {
  type        = string
  description = "Chart Version"
  default     = "4.0.18"
}

variable "repository_name" {
  type        = string
  description = "Chart name"
  default     = "https://kubernetes.github.io/ingress-nginx"
}

variable "namespace" {
  type        = string
  description = "Kubernetes namespace"
  default     = "controller-internal"
}

variable "ingres_class_name" {
  type        = string
  description = "Ingress Class Name"
}

variable "product_name" {
  type        = string
  description = "Product Name"
}

variable "environment" {
  type        = string
  description = "Environment"
}

variable "product_used_by" {
  type        = string
  description = "Product Used By"
}

variable "email_for_tagging" {
  type        = string
  description = "Email for Tagging"
}

variable "stack_for_tagging" {
  type        = string
  description = "Stack for Tagging"
}

variable "connection_timeout" {
  type        = number
  description = "Connection Timeout"
  default     = 60
}

/*variable "certificate_arn" {
  type        = string
  description = "Certificate manager arn"
  default     = ""
}*/

variable "vpc_id" {
  type        = string
  description = "VPC Id"
}

variable "helm_install_timeout" {
  description = "install timeout"
  type        = number
  default     = 600
}
variable "drain_connection_enable" {
  type        = bool
  default     = true
  description = "Drain Connection Enable"
}
variable "drain_connection_timeout" {
  type        = number
  default     = 60
  description = "Drain Connection Timeout"
}
variable "internal_enable" {
  type        = bool
  description = "Internal Loadbalancer"
  default     = true
}

variable "load_balancer_type" {
  type        = string
  description = "Load Balancer Type"
}

variable "certificate_secret_key" {
  type        = string
  description = "Certificate Secret Key"
  default     = "/subdomain/certificate"
}

/*variable "sub_domain_name" {
  type        = string
  description = "SudDomain name"
}*/
variable "ssm_host_path" {
  type        = string
  description = "SSM Key"
  default     = ""
}
