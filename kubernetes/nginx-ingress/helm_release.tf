locals {
  # A load balancer cannot be attached to multiple subnets in the same Availability Zone
  # https://stackoverflow.com/questions/63727252/how-to-pick-distinct-subnets-from-all-the-available-zones-in-terraform
  nlb_config = {
    time_out     = var.connection_timeout,
    ssl_cert_arn = data.aws_ssm_parameter.certificate_arn.value,
    subnet_id = join(",",
      [for subnet_ids in local.availability_zone_subnets : subnet_ids[0]]
    ),
    drain_connection_enable  = var.drain_connection_enable,
    drain_connection_timeout = var.drain_connection_timeout
  }


  alb_config = {
    time_out                 = var.connection_timeout,
    ssl_cert_arn             = data.aws_ssm_parameter.certificate_arn.value,
    subnet_id                = join(",", data.aws_subnets.private.ids),
    drain_connection_enable  = var.drain_connection_enable,
    drain_connection_timeout = var.drain_connection_timeout,
    security_group           = module.node_security_group.this_security_group_id
  }
  external_alb_config = {
    time_out                 = var.connection_timeout,
    ssl_cert_arn             = data.aws_ssm_parameter.certificate_arn.value,
    subnet_id                = join(",", data.aws_subnets.private.ids),
    drain_connection_enable  = var.drain_connection_enable,
    drain_connection_timeout = var.drain_connection_timeout,
    security_group           = module.node_security_group.this_security_group_id
    cf_security_group        = (var.internal_enable) ? "" : data.aws_ssm_parameter.cf_sg[0].value
  }


  node_security_group_desc = "Internal ALB Security Group"
  ingress_cidr_blocks      = ["10.0.0.0/8"]
  ingress_rules            = ["http-80-tcp", "all-icmp", "https-443-tcp"]
  egress_rules             = ["all-all"]
}

resource "helm_release" "nginx-ingress" {
  name       = var.helm_name
  chart      = var.chart_name
  repository = var.repository_name
  namespace  = var.namespace
  version    = var.chart_version
  values = [
    (var.load_balancer_type == "nlb") ? templatefile("values/nlb-values.yaml", local.nlb_config) : (var.internal_enable) ? templatefile("values/alb-internal-values.yaml", local.alb_config) : templatefile("values/alb-external-values.yaml", local.external_alb_config)
  ]
  set {
    name  = "controller.ingressClass"
    value = var.ingres_class_name
  }
  set {
    name  = "controller.ingressClassResource.name"
    value = var.ingres_class_name
  }
  set {
    name  = "controller.hostNetwork"
    value = local.cni_host_network
  }
  set {
    name  = "controller.admissionWebhooks.port"
    value = local.cni_host_network == false ? "8443" : "8444"
  }
  timeout    = var.helm_install_timeout
  depends_on = [module.node_security_group]
}

module "node_security_group" {
  source                  = "..//aws/sg"
  environment             = var.environment
  product_name            = var.product_name
  application_name        = var.application_name
  vpc_id                  = var.vpc_id
  resource_type           = var.product_used_by
  email_for_tagging       = var.email_for_tagging
  stack_for_tagging       = var.stack_for_tagging
  description             = local.node_security_group_desc
  ingress_cidr_blocks     = local.ingress_cidr_blocks
  ingress_rules           = local.ingress_rules
  egress_rules            = local.egress_rules
  additional_tags         = { "kubernetes.io/cluster/${local.cluster_name}" : "owned" }
  egress_with_cidr_blocks = []
}

resource "aws_ssm_parameter" "interal_alb" {
  name  = local.ssm_alb_name
  type  = "String"
  value = data.kubernetes_service.ingress_svc.status[0].load_balancer[0].ingress[0].hostname
}
