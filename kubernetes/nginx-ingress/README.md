#Required Step
We need to do below activity before setting up Ingress

- Attach to the production AWS Account with Domain acme-data.com domain and validate using acm module
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | n/a |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_node_security_group"></a> [node\_security\_group](#module\_node\_security\_group) | ..//aws/sg | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_ssm_parameter.interal_alb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [helm_release.nginx-ingress](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [aws_eks_cluster.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster) | data source |
| [aws_eks_cluster_auth.cluster_auth](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth) | data source |
| [aws_secretsmanager_secret.eks_cluster_secret](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.eks_cluster_info](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [aws_ssm_parameter.certificate_arn](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssm_parameter) | data source |
| [aws_ssm_parameter.cf_sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssm_parameter) | data source |
| [aws_subnet.private](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |
| [aws_subnets.private](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnets) | data source |
| [aws_vpc.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |
| [kubernetes_service.ingress_svc](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data-sources/service) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_name"></a> [application\_name](#input\_application\_name) | Application Name | `string` | n/a | yes |
| <a name="input_certificate_secret_key"></a> [certificate\_secret\_key](#input\_certificate\_secret\_key) | Certificate Secret Key | `string` | `"/subdomain/certificate"` | no |
| <a name="input_chart_name"></a> [chart\_name](#input\_chart\_name) | Chart name | `string` | `"ingress-nginx"` | no |
| <a name="input_chart_version"></a> [chart\_version](#input\_chart\_version) | Chart Version | `string` | `"4.0.18"` | no |
| <a name="input_cluster_secret_key"></a> [cluster\_secret\_key](#input\_cluster\_secret\_key) | EKS Cluster Secret Key | `string` | `""` | no |
| <a name="input_connection_timeout"></a> [connection\_timeout](#input\_connection\_timeout) | Connection Timeout | `number` | `60` | no |
| <a name="input_drain_connection_enable"></a> [drain\_connection\_enable](#input\_drain\_connection\_enable) | Drain Connection Enable | `bool` | `true` | no |
| <a name="input_drain_connection_timeout"></a> [drain\_connection\_timeout](#input\_drain\_connection\_timeout) | Drain Connection Timeout | `number` | `60` | no |
| <a name="input_email_for_tagging"></a> [email\_for\_tagging](#input\_email\_for\_tagging) | Email for Tagging | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment | `string` | n/a | yes |
| <a name="input_helm_install_timeout"></a> [helm\_install\_timeout](#input\_helm\_install\_timeout) | install timeout | `number` | `600` | no |
| <a name="input_helm_name"></a> [helm\_name](#input\_helm\_name) | Helm Name | `string` | `"nginx-ingress"` | no |
| <a name="input_ingres_class_name"></a> [ingres\_class\_name](#input\_ingres\_class\_name) | Ingress Class Name | `string` | n/a | yes |
| <a name="input_internal_enable"></a> [internal\_enable](#input\_internal\_enable) | Internal Loadbalancer | `bool` | `true` | no |
| <a name="input_load_balancer_type"></a> [load\_balancer\_type](#input\_load\_balancer\_type) | Load Balancer Type | `string` | n/a | yes |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Kubernetes namespace | `string` | `"controller-internal"` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | Product Name | `string` | n/a | yes |
| <a name="input_product_used_by"></a> [product\_used\_by](#input\_product\_used\_by) | Product Used By | `string` | n/a | yes |
| <a name="input_repository_name"></a> [repository\_name](#input\_repository\_name) | Chart name | `string` | `"https://kubernetes.github.io/ingress-nginx"` | no |
| <a name="input_ssm_host_path"></a> [ssm\_host\_path](#input\_ssm\_host\_path) | SSM Key | `string` | `""` | no |
| <a name="input_stack_for_tagging"></a> [stack\_for\_tagging](#input\_stack\_for\_tagging) | Stack for Tagging | `string` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC Id | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
