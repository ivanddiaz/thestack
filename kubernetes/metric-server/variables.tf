variable "environment" {
  type        = string
  description = "Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod'"
}
variable "helm_name" {
  description = "helm chart name"
  type        = string
  default     = "metric-server"
}
variable "cluster_secret_key" {
  description = "EKS Cluster Secret Key"
  type        = string
  default     = ""
}

variable "chart_name" {
  type        = string
  description = "Chart name"
  default     = "metrics-server"
}

variable "chart_version" {
  type        = string
  description = "Chart Version"
  default     = "3.10.0"
}

variable "repository_name" {
  type        = string
  description = "Chart name"
  default     = "https://kubernetes-sigs.github.io/metrics-server/"
}

variable "namespace" {
  type        = string
  description = "Kubernetes namespace"
  default     = "kube-system"
}

variable "helm_install_timeout" {
  description = "install timeout"
  type        = number
  default     = 600
}

variable "replicas" {
  type        = number
  default     = 1
  description = "Number of replicas to run."
}

variable "prometheus_monitor" {
  type        = bool
  default     = true
  description = "Monitor health of metric server"
}

variable "update_strategy" {
  type        = object({})
  default     = {}
  description = "Deployment strategy"
}
