resource "helm_release" "metric-server" {
  name       = var.helm_name
  chart      = var.chart_name
  repository = var.repository_name
  namespace  = var.namespace
  version    = var.chart_version

  set {
    name  = "replicas"
    value = var.replicas
  }

  set {
    name  = "serviceMonitor.enabled"
    value = var.prometheus_monitor
  }

  set {
    name  = "hostNetwork.enabled"
    value = jsondecode(data.aws_secretsmanager_secret_version.cluster_info.secret_string)["cni_host_network"]
  }

  set {
    name  = "containerPort"
    value = local.cni_host_network == "false" ? 10250 : 10251
  }

  /* set {
    name  = "updateStrategy"
    value = jsonencode(var.update_strategy)
  }
*/


  timeout = var.helm_install_timeout
}