<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.8.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | ~> 2.8.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.metric-server](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [aws_eks_cluster.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster) | data source |
| [aws_eks_cluster_auth.cluster_auth](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth) | data source |
| [aws_secretsmanager_secret.cluster_info](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.cluster_info](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_chart_name"></a> [chart\_name](#input\_chart\_name) | Chart name | `string` | `"metrics-server"` | no |
| <a name="input_chart_version"></a> [chart\_version](#input\_chart\_version) | Chart Version | `string` | `"3.10.0"` | no |
| <a name="input_cluster_secret_key"></a> [cluster\_secret\_key](#input\_cluster\_secret\_key) | EKS Cluster Secret Key | `string` | `""` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod' | `string` | n/a | yes |
| <a name="input_helm_install_timeout"></a> [helm\_install\_timeout](#input\_helm\_install\_timeout) | install timeout | `number` | `600` | no |
| <a name="input_helm_name"></a> [helm\_name](#input\_helm\_name) | helm chart name | `string` | `"metric-server"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Kubernetes namespace | `string` | `"kube-system"` | no |
| <a name="input_prometheus_monitor"></a> [prometheus\_monitor](#input\_prometheus\_monitor) | Monitor health of metric server | `bool` | `true` | no |
| <a name="input_replicas"></a> [replicas](#input\_replicas) | Number of replicas to run. | `number` | `1` | no |
| <a name="input_repository_name"></a> [repository\_name](#input\_repository\_name) | Chart name | `string` | `"https://kubernetes-sigs.github.io/metrics-server/"` | no |
| <a name="input_update_strategy"></a> [update\_strategy](#input\_update\_strategy) | Deployment strategy | `object({})` | `{}` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
