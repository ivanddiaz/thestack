locals {
  iam_user_name       = upper("${data.aws_eks_cluster.cluster.name}-spark-hs")
  iam_group_name      = upper("${data.aws_eks_cluster.cluster.name}-spark-hs-group")
  iam_police_name     = upper("${data.aws_eks_cluster.cluster.name}-spark-hs-policy")
  iam_attachment_name = upper("${data.aws_eks_cluster.cluster.name}-spark-hs-attachment")
  config_path         = "config"
  helm_values_dir     = "values"
  chart_path          = "${path.module}/chart/spark-hs-chart-v${var.chart_version}" #TODO: Fix: Error: YAML parse error on spark-hs/templates/tests/.terragrunt-source-manifest: error converting YAML to JSON: yaml: invalid leading UTF-8 octet
  s3_endpoint         = "s3.${var.aws_region}.amazonaws.com"
  s3_logdir           = "s3a://${var.s3_bucket_name}/${var.s3_bucket_dir}/"
}

# IAM Users
resource "aws_iam_user" "iam_user_spark_hs" {
  name = local.iam_user_name
}

# IAM User Access Keys
resource "aws_iam_access_key" "iam_access_key" {
  user = aws_iam_user.iam_user_spark_hs.name
}

# IAM Group
resource "aws_iam_group" "aws_iam_group_spark_hs" {
  name = local.iam_group_name
}

# IAM Group membership
resource "aws_iam_group_membership" "aws_iam_group_membership_spark_hs" {
  name = "spark-hs_membership"
  users = [
    aws_iam_user.iam_user_spark_hs.name,
  ]
  group = aws_iam_group.aws_iam_group_spark_hs.name
}

# IAM Policy
resource "aws_iam_policy" "aws_iam_policy_spark_hs" {
  name = local.iam_police_name
  policy = templatefile("${local.config_path}/aws_iam_policy.json", {
    bucket_arn = data.aws_s3_bucket.glue_logs_bucket.arn
    bucket_dir = var.s3_bucket_dir
  })
}

# IAM Policy attachment
resource "aws_iam_policy_attachment" "aws_iam_spark_hs_attach" {
  name       = local.iam_attachment_name
  groups     = [aws_iam_group.aws_iam_group_spark_hs.name]
  policy_arn = aws_iam_policy.aws_iam_policy_spark_hs.arn
}

# Helm release for Spark history server
resource "helm_release" "spark_history_server" {
  name      = var.helm_name
  chart     = local.chart_path
  namespace = var.namespace
  version   = var.chart_version

  values = [
    templatefile("${local.helm_values_dir}/spark-hs.yml", {
      helm_name         = var.helm_name
      s3_endpoint       = local.s3_endpoint
      s3_logdir         = local.s3_logdir
      s3_access_key     = aws_iam_access_key.iam_access_key.id
      s3_secret         = aws_iam_access_key.iam_access_key.secret
      ingress_classname = var.ingress_class_name
      ingress_host_name = var.ingress_host_name
      }
    )
  ]

  timeout = var.helm_install_timeout
}
