<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.8.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | ~> 2.8.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_access_key.iam_access_key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_access_key) | resource |
| [aws_iam_group.aws_iam_group_spark_hs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group) | resource |
| [aws_iam_group_membership.aws_iam_group_membership_spark_hs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_membership) | resource |
| [aws_iam_policy.aws_iam_policy_spark_hs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy_attachment.aws_iam_spark_hs_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy_attachment) | resource |
| [aws_iam_user.iam_user_spark_hs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_route53_record.endpoint](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [helm_release.spark_history_server](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [time_sleep.wait_30_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |
| [aws_eks_cluster.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster) | data source |
| [aws_eks_cluster_auth.cluster_auth](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth) | data source |
| [aws_route53_zone.sub_domain](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |
| [aws_s3_bucket.glue_logs_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/s3_bucket) | data source |
| [aws_secretsmanager_secret.eks_cluster_info](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.cluster_name](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [kubernetes_ingress_v1.spark-hs-ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data-sources/ingress_v1) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS S3 endpoint region | `string` | `"us-east-1"` | no |
| <a name="input_chart_name"></a> [chart\_name](#input\_chart\_name) | Chart name | `string` | `"spark-history-server"` | no |
| <a name="input_chart_version"></a> [chart\_version](#input\_chart\_version) | Chart Version | `string` | `"0.8.9"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment name dev/qa/uat/prd | `string` | n/a | yes |
| <a name="input_helm_install_timeout"></a> [helm\_install\_timeout](#input\_helm\_install\_timeout) | Helm run timeout | `number` | `180` | no |
| <a name="input_helm_name"></a> [helm\_name](#input\_helm\_name) | Helm deployment name | `string` | `"spark-history-server"` | no |
| <a name="input_ingress_class_name"></a> [ingress\_class\_name](#input\_ingress\_class\_name) | Ingress Class Name | `string` | `"nginx"` | no |
| <a name="input_ingress_host_name"></a> [ingress\_host\_name](#input\_ingress\_host\_name) | Ingress host name | `string` | `"nginx.local"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Kubernetes namespace | `string` | `"default"` | no |
| <a name="input_s3_bucket_dir"></a> [s3\_bucket\_dir](#input\_s3\_bucket\_dir) | Directory where spark logs are located | `string` | n/a | yes |
| <a name="input_s3_bucket_name"></a> [s3\_bucket\_name](#input\_s3\_bucket\_name) | Name (short) of the s3 bucket | `string` | n/a | yes |
| <a name="input_sub_domain_name"></a> [sub\_domain\_name](#input\_sub\_domain\_name) | SudDomain name | `string` | n/a | yes |
| <a name="input_update_strategy"></a> [update\_strategy](#input\_update\_strategy) | Deployment strategy | `object({})` | `{}` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
