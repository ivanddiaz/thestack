//Environment inputs

variable "aws_region" {
  type        = string
  description = "AWS S3 endpoint region"
  default     = "us-east-1"
}
variable "environment" {
  type        = string
  description = "Environment name dev/qa/uat/prd"
}
variable "helm_install_timeout" {
  type        = number
  description = "Helm run timeout"
  default     = 180
}

variable "helm_name" {
  type        = string
  description = "Helm deployment name"
  default     = "spark-history-server"
}

variable "chart_name" {
  type        = string
  description = "Chart name"
  default     = "spark-history-server"
}

variable "chart_version" {
  type        = string
  description = "Chart Version"
  default     = "0.8.9"
}

variable "namespace" {
  type        = string
  description = "Kubernetes namespace"
  default     = "default"
}
variable "ingress_class_name" {
  description = "Ingress Class Name"
  default     = "nginx"
  type        = string
}

variable "ingress_host_name" {
  description = "Ingress host name"
  default     = "nginx.local"
  type        = string
}

variable "update_strategy" {
  type        = object({})
  default     = {}
  description = "Deployment strategy"
}

variable "s3_bucket_name" {
  type        = string
  description = "Name (short) of the s3 bucket"
}

variable "s3_bucket_dir" {
  type        = string
  description = "Directory where spark logs are located"
}

variable "sub_domain_name" {
  type        = string
  description = "SudDomain name"
}
