# Standard route53 DNS record pointing to an LB
resource "aws_route53_record" "endpoint" {
  zone_id = data.aws_route53_zone.sub_domain.zone_id
  name    = var.ingress_host_name
  type    = "CNAME"
  ttl     = 60
  records = [
    data.kubernetes_ingress_v1.spark-hs-ingress.status[0].load_balancer[0].ingress[0].hostname
  ]
}

resource "time_sleep" "wait_30_seconds" {
  create_duration = "30s"
}
