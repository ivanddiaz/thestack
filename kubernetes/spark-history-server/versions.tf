terraform {
  required_version = "~> 1.1.7"
  required_providers {
    aws  = "~> 4.5.0"
    helm = "~> 2.8.0"
  }
}