//Environment inputs

variable "aws_region" {
  type        = string
  description = "AWS S3 endpoint region"
  default     = "us-east-1"
}
variable "environment" {
  type        = string
  description = "Environment name dev/qa/uat/prd"
}
variable "helm_install_timeout" {
  type        = number
  description = "Helm run timeout"
  default     = 180
}

variable "helm_name" {
  type        = string
  description = "Helm deployment name"
  default     = "selenium-grid"
}

variable "chart_name" {
  type        = string
  description = "Chart name"
  default     = "selenium-grid"
}

variable "chart_version" {
  type        = string
  description = "Chart Version"
  default     = "0.10.0"
}

variable "namespace" {
  type        = string
  description = "Kubernetes namespace"
  default     = "default"
}

variable "ingress_enabled" {
  type        = bool
  description = "Enable ingress"
  default     = true
}

variable "ingress_class_name" {
  description = "Ingress Class Name"
  default     = "nginx"
  type        = string
}

variable "ingress_host_name" {
  description = "Ingress host name"
  default     = "selenium-grid.local"
  type        = string
}

variable "isolate_compontes" {
  description = "Isolate component"
  default     = true
  type        = bool
}

variable "sub_domain_name" {
  type        = string
  description = "SudDomain name"
}
