locals {
  config_path     = "config"
  helm_values_dir = "values"
  chart           = "https://github.com/SeleniumHQ/docker-selenium/releases/download/selenium-grid-${var.chart_version}/selenium-grid-${var.chart_version}.tgz"
}

# Helm release for Selenium grid
resource "helm_release" "selenium_grid" {
  name      = var.helm_name
  chart     = local.chart
  namespace = var.namespace

  values = [
    templatefile("${local.helm_values_dir}/selenium-grid.yml", {
      helm_name         = var.helm_name
      ingress_enabled   = var.ingress_enabled
      ingress_classname = var.ingress_class_name
      ingress_host_name = var.ingress_host_name
      isolate_compontes = var.isolate_compontes
      }
    )
  ]

  timeout = var.helm_install_timeout

  depends_on = [
    kubernetes_namespace.namespace
  ]
}

resource "kubernetes_namespace" "namespace" {
  metadata {

    labels = {
      name = var.namespace
    }

    name = var.namespace
  }
  provider = kubernetes
}
