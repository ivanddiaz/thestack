data "aws_secretsmanager_secret" "eks_cluster_info" {
  name = "${var.environment}/eks/cluster-info"
}

data "aws_secretsmanager_secret_version" "cluster_name" {
  secret_id = data.aws_secretsmanager_secret.eks_cluster_info.id
}

data "aws_eks_cluster" "cluster" {
  name = jsondecode(data.aws_secretsmanager_secret_version.cluster_name.secret_string)["cluster_name"]
}

data "aws_eks_cluster_auth" "cluster_auth" {
  name = data.aws_eks_cluster.cluster.name
}

data "aws_route53_zone" "sub_domain" {
  name         = var.sub_domain_name
  private_zone = false
}

data "kubernetes_ingress_v1" "influx_ingress" {
  metadata {
    name      = "selenium-ingress"
    namespace = var.namespace
  }
  provider = kubernetes
  depends_on = [
    helm_release.selenium_grid
  ]
}