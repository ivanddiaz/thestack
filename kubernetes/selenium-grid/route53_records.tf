# Standard route53 DNS record pointing to an LB
resource "aws_route53_record" "influx_endpoint" {
  zone_id = data.aws_route53_zone.sub_domain.zone_id
  name    = var.ingress_host_name
  type    = "CNAME"
  ttl     = 60
  records = [
    data.kubernetes_ingress_v1.influx_ingress.status[0].load_balancer[0].ingress[0].hostname
  ]
}
