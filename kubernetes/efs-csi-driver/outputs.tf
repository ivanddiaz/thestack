output "efs_iam_role" {
  value       = aws_iam_role.efs_iam_role.arn
  description = "EFS IAM Role"
}