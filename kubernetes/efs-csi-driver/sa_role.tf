module "efs_role_label" {
  source           = "..//aws/common-label"
  product_name     = var.product_name
  environment      = var.environment
  application_name = var.application_name
  attributes       = ["EFS-CSI-DRIVER", "SA"]
  delimiter        = "-"
  email            = var.email_for_tagging
  stack            = var.stack_for_tagging
  module_name      = var.module_name_for_tagging
  tags = {
    Vertical = var.vertical_for_tagging
  }
}

data "aws_iam_policy_document" "this" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [local.oidc_arn]
    }
    condition {
      test     = "StringEquals"
      variable = "${replace(local.oidc_arn, "/^(.*provider/)/", "")}:sub"
      values   = [for sa in var.namespace_service_accounts : "system:serviceaccount:${sa}"]
    }
  }
}


resource "aws_iam_role" "efs_iam_role" {
  name                 = module.efs_role_label.id
  assume_role_policy   = data.aws_iam_policy_document.this.json
  max_session_duration = local.max_session_duration
  tags                 = module.efs_role_label.tags
}


resource "aws_iam_role_policy" "sa_iam_role" {
  name   = module.efs_role_label.id
  role   = aws_iam_role.efs_iam_role.id
  policy = file("${local.config_path}/efs_driver_policy.json")
}
