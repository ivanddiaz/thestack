resource "helm_release" "efs-driver" {
  name      = "aws-efs-csi-driver"
  chart     = var.chart_path
  namespace = var.ns

  values = [
    templatefile("values/default_value.yaml", { sa_role = aws_iam_role.efs_iam_role.arn })
  ]
  set {
    name  = "controller.serviceAccount.create"
    value = "true"
  }

  set {
    name  = "replicaCount"
    value = var.replica
  }

  depends_on = [
    aws_iam_role.efs_iam_role
  ]
}