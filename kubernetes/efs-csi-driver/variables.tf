variable "product_name" {
  type        = string
  description = "Product Name, e.g. 'mc','fb','dm', 'project'"
}
variable "environment" {
  type        = string
  description = "Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod'"
}
variable "application_name" {
  type        = string
  default     = "EKS"
  description = "Application name, e.g. 'api'"
}
variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
}
variable "stack_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = ""
}
variable "module_name_for_tagging" {
  type        = string
  description = "The module name for tagging"
  default     = ""
}
variable "vertical_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = "acme"
}
variable "cluster_secret_key" {
  description = "EKS Cluster Secret Key"
  type        = string
  default     = ""
}
variable "chart_path" {
  type        = string
  description = "Chart Path"
  default     = "https://github.com/kubernetes-sigs/aws-efs-csi-driver/releases/download/helm-chart-aws-efs-csi-driver-2.5.0/aws-efs-csi-driver-2.5.0.tgz"
}
variable "ns" {
  type        = string
  description = "Namespace"
  default     = "kube-system"
}
variable "replica" {
  type        = number
  description = "No of replica"
  default     = 1
}
variable "aws_region" {
  type        = string
  description = "AWS Region"
}
variable "namespace_service_accounts" {
  type        = list(string)
  description = "NameSpace and Service Account"
  default     = ["kube-system:efs-csi-controller-sa", "kube-system:efs-csi-node-sa"]
}
