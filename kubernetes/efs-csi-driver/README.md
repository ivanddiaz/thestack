<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.8.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | ~> 2.8.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_efs_role_label"></a> [efs\_role\_label](#module\_efs\_role\_label) | ..//aws/common-label | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_role.efs_iam_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.sa_iam_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [helm_release.efs-driver](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [aws_eks_cluster.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster) | data source |
| [aws_eks_cluster_auth.cluster_auth](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth) | data source |
| [aws_iam_policy_document.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_secretsmanager_secret.eks_cluster_secret](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.eks_cluster_secret_version](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_name"></a> [application\_name](#input\_application\_name) | Application name, e.g. 'api' | `string` | `"EKS"` | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS Region | `string` | n/a | yes |
| <a name="input_chart_path"></a> [chart\_path](#input\_chart\_path) | Chart Path | `string` | `"https://github.com/kubernetes-sigs/aws-efs-csi-driver/releases/download/helm-chart-aws-efs-csi-driver-2.5.0/aws-efs-csi-driver-2.5.0.tgz"` | no |
| <a name="input_cluster_secret_key"></a> [cluster\_secret\_key](#input\_cluster\_secret\_key) | EKS Cluster Secret Key | `string` | `""` | no |
| <a name="input_email_for_tagging"></a> [email\_for\_tagging](#input\_email\_for\_tagging) | The email address of a group or team that owns the resource for tagging | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod' | `string` | n/a | yes |
| <a name="input_module_name_for_tagging"></a> [module\_name\_for\_tagging](#input\_module\_name\_for\_tagging) | The module name for tagging | `string` | `""` | no |
| <a name="input_namespace_service_accounts"></a> [namespace\_service\_accounts](#input\_namespace\_service\_accounts) | NameSpace and Service Account | `list(string)` | <pre>[<br>  "kube-system:efs-csi-controller-sa",<br>  "kube-system:efs-csi-node-sa"<br>]</pre> | no |
| <a name="input_ns"></a> [ns](#input\_ns) | Namespace | `string` | `"kube-system"` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | Product Name, e.g. 'mc','fb','dm', 'project' | `string` | n/a | yes |
| <a name="input_replica"></a> [replica](#input\_replica) | No of replica | `number` | `1` | no |
| <a name="input_stack_for_tagging"></a> [stack\_for\_tagging](#input\_stack\_for\_tagging) | Stack name for a collection of resources for tagging | `string` | `""` | no |
| <a name="input_vertical_for_tagging"></a> [vertical\_for\_tagging](#input\_vertical\_for\_tagging) | Stack name for a collection of resources for tagging | `string` | `"acme"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_efs_iam_role"></a> [efs\_iam\_role](#output\_efs\_iam\_role) | EFS IAM Role |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
