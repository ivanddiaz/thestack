terraform {
  required_version = "~> 1.1.7"
  required_providers {
    aws = "~> 4.5.0"
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.17.0"
    }
  }
}