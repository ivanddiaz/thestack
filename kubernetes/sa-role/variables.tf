variable "product_name" {
  type        = string
  description = "Product Name, e.g. 'mc','fb','dm'"
}

variable "environment" {
  type        = string
  description = "Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod'"
}

variable "application_name" {
  type        = string
  description = "Application name, e.g. 'api'"
}

variable "email_for_tagging" {
  type        = string
  description = "The email address for tagging the resources"
}

variable "vertical_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = "acme"
}

variable "namespace" {
  type        = string
  description = "EKS cluster Name Space"
  default     = "airflow"
}

variable "service_account" {
  type        = string
  description = "Service Account Name"
}

variable "policy_string" {
  type        = string
  description = "Policy Definition of IRSA (IAM Role for Service Account) for the POD"
}
