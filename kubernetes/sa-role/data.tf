locals {
  stack                = "EKS-Service-Role"
  module_name          = "Service Role"
  config_path          = "./config"
  max_session_duration = 36000
  oidc_arn             = jsondecode(data.aws_secretsmanager_secret_version.eks_cluster_info.secret_string)["oidc_arn"]
}

data "aws_caller_identity" "current" {}

data "aws_secretsmanager_secret" "eks_cluster_secret" {
  name = "${var.environment}/eks/cluster-info"
}

data "aws_secretsmanager_secret_version" "eks_cluster_info" {
  secret_id = data.aws_secretsmanager_secret.eks_cluster_secret.id
}

data "aws_eks_cluster" "cluster" {
  name = jsondecode(data.aws_secretsmanager_secret_version.eks_cluster_info.secret_string)["cluster_name"]
}

data "aws_eks_cluster_auth" "cluster_auth" {
  name = data.aws_eks_cluster.cluster.name
}

