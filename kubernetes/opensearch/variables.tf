//Environment inputs
variable "aws_region" {
  type        = string
  description = "AWS Region"
}
variable "environment" {
  type        = string
  description = "Environment name dev/qa/uat/prd"
}

variable "product_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}
variable "helm_install_timeout" {
  type        = number
  description = "Helm run timeout"
  default     = 300
}
variable "opensearch_solution_name" {
  type        = string
  default     = "opensearch"
  description = "Solution name: Engine cluster + Dashboards"
}

variable "repository_name" {
  type        = string
  description = "Repository name"
  default     = "https://opensearch-project.github.io/helm-charts/"
}
variable "chart_name" {
  type        = string
  description = "Chart name"
  default     = "opensearch"
}

variable "chart_name_dashboards" {
  type        = string
  description = "Chart name"
  default     = "opensearch-dashboards"
}

variable "chart_version" {
  type        = string
  description = "Chart Version"
  default     = "1.18.0"
}

variable "chart_version_dashboards" {
  type        = string
  description = "Chart Version"
  default     = "1.11.0"
}

variable "namespace" {
  type        = string
  description = "Kubernetes namespace"
  default     = "default"
}

variable "opensearch_replicas" {
  type        = number
  default     = 3
  description = "Number of opensearch (elasticsearch fork) replicas to run."
  validation {
    condition     = can(regex("^(3|5|7|9|11|13|15|17|19|21)$", var.opensearch_replicas))
    error_message = "Must be an odd number between 3 and 21."
  }
}

variable "opensearch_persistence_size" {
  type        = number
  default     = 8
  description = "Pod Volume size (Gibibytes)"
}

variable "opensearch_persistence_extended_size" {
  type        = number
  default     = 0
  description = "Pod Volume extended size (Gibibytes)"
}

variable "opensearch_persistence_extended_type" {
  type        = string
  default     = null
  description = "EBS volumetype"
}

variable "opensearch_persistence_extended_throughput" {
  type        = string
  default     = null
  description = "EBS Throughput in MiB/s"
}

variable "opensearch_persistence_extended_iops" {
  type        = string
  default     = null
  description = "EBS IOPS"
}

variable "opensearch_minimumMasterNodes" {
  type        = number
  default     = 1
  description = "Minimal amount opensearch (elasticsearch fork) of master nodes"
}

variable "opensearch_dashboards_replicaCount" {
  type        = number
  default     = 1
  description = "Number of opensearch_dashboards (kibana fork) replicas to run."
}

variable "ingress_class_name" {
  description = "Ingress Class Name"
  default     = "nginx"
  type        = string
}

variable "domain_name" {
  description = "Ingress host name"
  default     = "opensearch.local"
  type        = string
}
variable "opensearch_subdomain" {
  description = "Ingress Openserach engine path"
  default     = "opensearch"
  type        = string
}

variable "dashboards_subdomain" {
  description = "Ingress Openserach dashboards path"
  default     = "opensearch-dashboards"
  type        = string
}

variable "update_strategy" {
  type        = object({})
  default     = {}
  description = "Deployment strategy"
}

variable "opensearch_spec_cpu" {
  type        = number
  default     = 0.5
  description = "vCPUs guaranteed"
}

variable "opensearch_spec_memory" {
  type        = number
  default     = 2
  description = "Memory limit (Gibibytes)"
}

variable "service_accounts" {
  type = map(any)
  # Example: 
  # <account_name> = <role_name>
  # service_accounts = {
  #   "svc_writer" = "all_access"
  #   "svc_reader" = "readall"
  # }
  default     = {}
  description = "Map user:role for service accounts - Ingress reachability is required"
}

variable "okta_opensearch_logo_path" {
  type        = string
  description = "The opensearch logo for okta app (must be declared in terragrunt or terraform infrastructure) "
  default     = null
}
variable "sub_domain_name" {
  type        = string
  description = "SudDomain name"
}
