## Okta Integration (authorization claim)

> 💡 This integration is already created at https://valence-admin.okta.com/.

This module requiere a customized token, check this link for more information
<https://developer.okta.com/docs/guides/customize-tokens-returned-from-okta/main/>

Use the following information to create the claim.

| Field                 | Value                     |
| --------------------- | ------------------------- |
| Claim Name            | opensearch_roles          |
| Include in token type | ID Token - Always         |
| Value Type            | Groups                    |
| Filter                | Starts with: OpenSearch\* |
| Include in            | Any Scope                 |

> \* Constant value from local.okta_groups_prefix in main.tf

### Bugs:

Logout is broken:
<https://github.com/opensearch-project/security-dashboards-plugin/issues/840>

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |
| <a name="requirement_elasticsearch"></a> [elasticsearch](#requirement\_elasticsearch) | ~> 2.0.7 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.8.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |
| <a name="provider_elasticsearch"></a> [elasticsearch](#provider\_elasticsearch) | ~> 2.0.7 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | ~> 2.8.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |
| <a name="provider_null"></a> [null](#provider\_null) | n/a |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_okta_admin_group"></a> [okta\_admin\_group](#module\_okta\_admin\_group) | ..//okta/group | n/a |
| <a name="module_okta_oauth_app"></a> [okta\_oauth\_app](#module\_okta\_oauth\_app) | ..//okta/oauth_app | n/a |
| <a name="module_okta_reader_group"></a> [okta\_reader\_group](#module\_okta\_reader\_group) | ..//okta/group | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_route53_record.dashboards](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_record.endpoint](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_secretsmanager_secret.aws_secret_opensearch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret) | resource |
| [aws_secretsmanager_secret.aws_secret_opensearch_svc_acocunts](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret) | resource |
| [aws_secretsmanager_secret_version.aws_secret_version_opensearch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_version) | resource |
| [aws_secretsmanager_secret_version.aws_secret_version_opensearch_svc_accounts](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_version) | resource |
| [elasticsearch_opensearch_user.user](https://registry.terraform.io/providers/phillbaker/elasticsearch/latest/docs/resources/opensearch_user) | resource |
| [helm_release.opensearch](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.opensearch_dashboards](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [null_resource.annotate_pvc_iops](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.annotate_pvc_throughput](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.migrate_pvc](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.refresh_dashboards](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.resize_pvc](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.update_security_config](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [random_password.account_passwords](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_password.admin_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_password.dashboards_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [time_sleep.wait_30_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |
| [aws_eks_cluster.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster) | data source |
| [aws_eks_cluster_auth.cluster_auth](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth) | data source |
| [aws_route53_zone.sub_domain](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |
| [aws_secretsmanager_secret.eks_cluster_info](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.cluster_name](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [kubernetes_ingress_v1.dashboards_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data-sources/ingress_v1) | data source |
| [kubernetes_ingress_v1.endpoint_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data-sources/ingress_v1) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS Region | `string` | n/a | yes |
| <a name="input_chart_name"></a> [chart\_name](#input\_chart\_name) | Chart name | `string` | `"opensearch"` | no |
| <a name="input_chart_name_dashboards"></a> [chart\_name\_dashboards](#input\_chart\_name\_dashboards) | Chart name | `string` | `"opensearch-dashboards"` | no |
| <a name="input_chart_version"></a> [chart\_version](#input\_chart\_version) | Chart Version | `string` | `"1.18.0"` | no |
| <a name="input_chart_version_dashboards"></a> [chart\_version\_dashboards](#input\_chart\_version\_dashboards) | Chart Version | `string` | `"1.11.0"` | no |
| <a name="input_dashboards_subdomain"></a> [dashboards\_subdomain](#input\_dashboards\_subdomain) | Ingress Openserach dashboards path | `string` | `"opensearch-dashboards"` | no |
| <a name="input_domain_name"></a> [domain\_name](#input\_domain\_name) | Ingress host name | `string` | `"opensearch.local"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment name dev/qa/uat/prd | `string` | n/a | yes |
| <a name="input_helm_install_timeout"></a> [helm\_install\_timeout](#input\_helm\_install\_timeout) | Helm run timeout | `number` | `300` | no |
| <a name="input_ingress_class_name"></a> [ingress\_class\_name](#input\_ingress\_class\_name) | Ingress Class Name | `string` | `"nginx"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Kubernetes namespace | `string` | `"default"` | no |
| <a name="input_okta_opensearch_logo_path"></a> [okta\_opensearch\_logo\_path](#input\_okta\_opensearch\_logo\_path) | The opensearch logo for okta app (must be declared in terragrunt or terraform infrastructure) | `string` | `null` | no |
| <a name="input_opensearch_dashboards_replicaCount"></a> [opensearch\_dashboards\_replicaCount](#input\_opensearch\_dashboards\_replicaCount) | Number of opensearch\_dashboards (kibana fork) replicas to run. | `number` | `1` | no |
| <a name="input_opensearch_minimumMasterNodes"></a> [opensearch\_minimumMasterNodes](#input\_opensearch\_minimumMasterNodes) | Minimal amount opensearch (elasticsearch fork) of master nodes | `number` | `1` | no |
| <a name="input_opensearch_persistence_extended_iops"></a> [opensearch\_persistence\_extended\_iops](#input\_opensearch\_persistence\_extended\_iops) | EBS IOPS | `string` | `null` | no |
| <a name="input_opensearch_persistence_extended_size"></a> [opensearch\_persistence\_extended\_size](#input\_opensearch\_persistence\_extended\_size) | Pod Volume extended size (Gibibytes) | `number` | `0` | no |
| <a name="input_opensearch_persistence_extended_throughput"></a> [opensearch\_persistence\_extended\_throughput](#input\_opensearch\_persistence\_extended\_throughput) | EBS Throughput in MiB/s | `string` | `null` | no |
| <a name="input_opensearch_persistence_extended_type"></a> [opensearch\_persistence\_extended\_type](#input\_opensearch\_persistence\_extended\_type) | EBS volumetype | `string` | `null` | no |
| <a name="input_opensearch_persistence_size"></a> [opensearch\_persistence\_size](#input\_opensearch\_persistence\_size) | Pod Volume size (Gibibytes) | `number` | `8` | no |
| <a name="input_opensearch_replicas"></a> [opensearch\_replicas](#input\_opensearch\_replicas) | Number of opensearch (elasticsearch fork) replicas to run. | `number` | `3` | no |
| <a name="input_opensearch_solution_name"></a> [opensearch\_solution\_name](#input\_opensearch\_solution\_name) | Solution name: Engine cluster + Dashboards | `string` | `"opensearch"` | no |
| <a name="input_opensearch_spec_cpu"></a> [opensearch\_spec\_cpu](#input\_opensearch\_spec\_cpu) | vCPUs guaranteed | `number` | `0.5` | no |
| <a name="input_opensearch_spec_memory"></a> [opensearch\_spec\_memory](#input\_opensearch\_spec\_memory) | Memory limit (Gibibytes) | `number` | `2` | no |
| <a name="input_opensearch_subdomain"></a> [opensearch\_subdomain](#input\_opensearch\_subdomain) | Ingress Openserach engine path | `string` | `"opensearch"` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | For bucket name as per naming standard | `string` | `"project"` | no |
| <a name="input_repository_name"></a> [repository\_name](#input\_repository\_name) | Repository name | `string` | `"https://opensearch-project.github.io/helm-charts/"` | no |
| <a name="input_service_accounts"></a> [service\_accounts](#input\_service\_accounts) | Map user:role for service accounts - Ingress reachability is required | `map(any)` | `{}` | no |
| <a name="input_sub_domain_name"></a> [sub\_domain\_name](#input\_sub\_domain\_name) | SudDomain name | `string` | n/a | yes |
| <a name="input_update_strategy"></a> [update\_strategy](#input\_update\_strategy) | Deployment strategy | `object({})` | `{}` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
