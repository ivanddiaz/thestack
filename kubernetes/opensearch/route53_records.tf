# Standard route53 DNS record pointing to an LB
resource "aws_route53_record" "endpoint" {
  zone_id = data.aws_route53_zone.sub_domain.zone_id
  name    = "${var.opensearch_subdomain}.${data.aws_route53_zone.sub_domain.name}"
  type    = "CNAME"
  ttl     = 60
  records = [
    data.kubernetes_ingress_v1.endpoint_ingress.status[0].load_balancer[0].ingress[0].hostname
  ]
}
# Standard route53 DNS record pointing to an LB
resource "aws_route53_record" "dashboards" {
  zone_id = data.aws_route53_zone.sub_domain.zone_id
  name    = "${var.dashboards_subdomain}.${data.aws_route53_zone.sub_domain.name}"
  type    = "CNAME"
  ttl     = 60
  records = [
    data.kubernetes_ingress_v1.dashboards_ingress.status[0].load_balancer[0].ingress[0].hostname
  ]
}