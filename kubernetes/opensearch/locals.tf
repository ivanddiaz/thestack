locals {
  helm_name                           = var.opensearch_solution_name
  helm_values_dir                     = "values"
  helm_name_dashboards                = "${var.opensearch_solution_name}-dashboards"
  opensearch_cluster_name             = "${var.opensearch_solution_name}-cluster"                             # Example clusterName: "opensearch-cluster"
  opensearch_nodegroup_name           = "master"                                                              # Example nodeGroup: "master"
  opensearch_masterservice_name       = "${local.opensearch_cluster_name}-${local.opensearch_nodegroup_name}" # Example masterService: "opensearch-cluster-master"
  opensearch_master_pod               = "${local.opensearch_masterservice_name}-0"
  opensearch_secret_name              = lower("${data.aws_eks_cluster.cluster.name}/${var.namespace}/${local.opensearch_cluster_name}")
  opensearch_secret_svc_accounts_name = lower("${data.aws_eks_cluster.cluster.name}/${var.namespace}/${local.opensearch_cluster_name}/service-accounts")
  opensearch_config_dir               = "config"
  opensearch_engine_fqdn              = "${var.opensearch_subdomain}.${var.domain_name}"
  opensearch_dashboards_fqdn          = "${var.dashboards_subdomain}.${var.domain_name}"
  opensearch_url                      = "https://${local.opensearch_engine_fqdn}"
  opensearch_dashboards_url           = "https://${local.opensearch_dashboards_fqdn}"
  opensearch_persistence_size         = var.opensearch_persistence_size
  opensearch_spec_cpu_mcores          = var.opensearch_spec_cpu * 1000
  okta_groups_prefix                  = "OpenSearch"
  okta_group_admin_name               = "${local.okta_groups_prefix}-Admins-${module.okta_oauth_app.label}"
  okta_group_reader_name              = "${local.okta_groups_prefix}-Readers-${module.okta_oauth_app.label}"
  okta_group_description              = "OpenSearch group for ${var.environment} environment"

  reserved_accounts = {
    superuser_role_name    = "admin"
    kibanaserver_role_name = "kibanaserver"
  }

  user_map = {
    for user, role in var.service_accounts :
    user => {
      account = user
      role    = role
    }
    if !contains(values(local.reserved_accounts), user) # This condition stops to trick reserved accounts
  }

  account_map = {
    for key, value in random_password.account_passwords :
    key => {
      account     = key
      role        = local.user_map[key].role
      password    = value.result
      bcrypt_hash = value.bcrypt_hash
    }
  }

  secret_admin_map = {
    role-admin-name               = "admin"
    role-admin-password           = random_password.admin_password.result
    role-admin-description        = "This is the opensearch administrator user"
    role-kibanaserver-name        = "kibanaserver"
    role-kibanaserver-password    = random_password.dashboards_password.result
    role-kibanaserver-description = "Service account: Allows to connect opensearch-dashboards with cluster engine"
    opensearch-url                = local.opensearch_engine_fqdn
    opensearch-dashboards-url     = local.opensearch_dashboards_url
  }

  secret_service_accounts_helper_map = {
    admin                     = "The admin account is the super user reserved account access is forbidden"
    kibanaserver_disclaimer   = "The kibanaserver account is the opensearch-dashboards reserved account access is forbidden"
    opensearch-url            = local.opensearch_url
    opensearch-dashboards-url = local.opensearch_dashboards_url
  }

  secret_service_accounts_map = {
    for account, value in local.account_map :
    account => value.password
  }

  # OpenSearch Values
  opensearch_template = templatefile("${local.helm_values_dir}/opensearch.yml", {
    clusterName        = local.opensearch_cluster_name
    instanceName       = local.helm_name
    nodeGroup          = local.opensearch_nodegroup_name
    masterService      = local.opensearch_masterservice_name
    replicas           = var.opensearch_replicas
    minimumMasterNodes = var.opensearch_minimumMasterNodes
    ingress_classname  = var.ingress_class_name
    ingress_host       = local.opensearch_engine_fqdn
    opensearch_config = indent(8,
      file("${local.opensearch_config_dir}/opensearch.yml")
    )
    internal_users_template = indent(8,
      templatefile("${local.opensearch_config_dir}/opensearch-security-internal_users.yml", {
        admin_hash      = random_password.admin_password.bcrypt_hash
        dashboards_hash = random_password.dashboards_password.bcrypt_hash
      })
    )
    internal_users_template = indent(8,
      templatefile("${local.opensearch_config_dir}/opensearch-security-internal_users.yml", {
        admin_hash      = random_password.admin_password.bcrypt_hash
        dashboards_hash = random_password.dashboards_password.bcrypt_hash
      })
    )
    security_config_template = indent(8,
      templatefile("${local.opensearch_config_dir}/opensearch-security-securityconfig.yml", {
        openid_connect_url = module.okta_oauth_app.openid_connect_url
      })
    )
    roles_mapping_template = indent(8,
      templatefile("${local.opensearch_config_dir}/opensearch-security-roles_mapping.yml", {
        admin_role  = module.okta_admin_group.group_name
        reader_role = module.okta_reader_group.group_name
      })
    )
    roles_template = indent(8,
      templatefile("${local.opensearch_config_dir}/opensearch-security-roles.yml", {
        reader_role = module.okta_reader_group.group_name
      })
    )
    size            = local.opensearch_persistence_size
    requests_cpu    = ceil(local.opensearch_spec_cpu_mcores)
    requests_memory = var.opensearch_spec_memory
    # How we calculate maxSkew?
    # https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/
    # Round upper ( # nodes / # subnets )
    # Example 7 nodes on 3 AZ
    # >  7/3          =  2.33333
    # >  ceil(2.3333) =  3
    # Then, karpenter will allocate maximun 3 pods per A-Z (3-3-1)
    maxSkew = ceil(var.opensearch_replicas / length(data.aws_eks_cluster.cluster.vpc_config[0]["subnet_ids"]))
    }
  )

  # OpenSearch Dashboards Values
  opensearch_dashboards_template = templatefile("${local.helm_values_dir}/opensearch-dashboards.yml", {
    replicaCount         = var.opensearch_dashboards_replicaCount
    opensearchHosts      = "https://${local.opensearch_masterservice_name}:9200"
    ingress_classname    = var.ingress_class_name
    ingress_host         = local.opensearch_dashboards_fqdn
    password             = random_password.dashboards_password.result
    admin_password       = random_password.admin_password.result
    host_alias           = local.opensearch_dashboards_fqdn
    openid_client_id     = module.okta_oauth_app.client_id
    openid_client_secret = module.okta_oauth_app.client_secret
    openid_connect_url   = module.okta_oauth_app.openid_connect_url
    openid_logout_url    = "https://${module.okta_oauth_app.okta_org_name}.${module.okta_oauth_app.okta_base_url}"
    }
  )
}
