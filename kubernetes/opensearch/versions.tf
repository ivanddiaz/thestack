terraform {
  required_version = "~> 1.1.7"
  required_providers {
    aws  = "~> 4.5.0"
    helm = "~> 2.8.0"
    elasticsearch = {
      source  = "phillbaker/elasticsearch"
      version = "~> 2.0.7"
    }
  }
}