# okta OAuth App
module "okta_oauth_app" {
  source       = "..//okta/oauth_app"
  label_prefix = var.opensearch_solution_name
  product_name = var.product_name
  environment  = var.environment
  logo         = var.okta_opensearch_logo_path
  login_uri    = "https://${local.opensearch_dashboards_fqdn}/auth/openid/login"
}

# okta Admin Group
module "okta_admin_group" {
  source            = "..//okta/group"
  group_name        = local.okta_group_admin_name
  app_id            = module.okta_oauth_app.id
  group_description = local.okta_group_description
  product_name      = var.product_name
  environment       = var.environment
}

# okta Reader Group
module "okta_reader_group" {
  source            = "..//okta/group"
  group_name        = local.okta_group_reader_name
  app_id            = module.okta_oauth_app.id
  group_description = local.okta_group_description
  environment       = var.environment
}

# Random passwords for reserved accounts
resource "random_password" "admin_password" {
  length           = 27
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "random_password" "dashboards_password" {
  length           = 27
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

# AWS Secrets
resource "aws_secretsmanager_secret" "aws_secret_opensearch" {
  name                    = local.opensearch_secret_name
  description             = "Opensearch initial bootstrapping passwords"
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret" "aws_secret_opensearch_svc_acocunts" {
  name                    = local.opensearch_secret_svc_accounts_name
  description             = "Opensearch initial bootstrapping passwords"
  recovery_window_in_days = 0
}

# AWS Secret versions
resource "aws_secretsmanager_secret_version" "aws_secret_version_opensearch" {
  secret_id     = aws_secretsmanager_secret.aws_secret_opensearch.id
  secret_string = jsonencode(local.secret_admin_map)
}

resource "aws_secretsmanager_secret_version" "aws_secret_version_opensearch_svc_accounts" {
  secret_id     = aws_secretsmanager_secret.aws_secret_opensearch_svc_acocunts.id
  secret_string = jsonencode(merge(local.secret_service_accounts_helper_map, local.secret_service_accounts_map))
}

# Helm release for Opensearch engine (Elasticsearch)
resource "helm_release" "opensearch" {
  name          = local.helm_name
  chart         = var.chart_name
  repository    = var.repository_name
  namespace     = var.namespace
  version       = var.chart_version
  wait_for_jobs = true
  values        = [local.opensearch_template]
  timeout       = (var.helm_install_timeout * var.opensearch_replicas)
}

# Helm release for Opensearch dashboards (Kibana)
resource "helm_release" "opensearch_dashboards" {
  depends_on   = [null_resource.update_security_config]
  name         = local.helm_name_dashboards
  chart        = var.chart_name_dashboards
  repository   = var.repository_name
  namespace    = var.namespace
  version      = var.chart_version_dashboards
  force_update = true
  values       = [local.opensearch_dashboards_template]
  timeout      = var.helm_install_timeout
}

# Passwords for service accounts
resource "random_password" "account_passwords" {
  for_each         = local.user_map
  length           = 27
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

# Update security config if opensearch config changes
resource "time_sleep" "wait_30_seconds" {
  depends_on      = [helm_release.opensearch]
  create_duration = "30s"
  triggers = {
    helm_changes = nonsensitive(sha256(jsonencode(helm_release.opensearch.values)))
  }
}
resource "null_resource" "update_security_config" {
  depends_on = [helm_release.opensearch, time_sleep.wait_30_seconds]

  triggers = {
    helm_changes = nonsensitive(sha256(jsonencode(helm_release.opensearch.values)))
  }
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region $AWS_REGION --name $CLUSTER_NAME;
      kubectl exec -i -t -n $NAMESPACE $POD_NAME -c $CONTAINER_NAME -- /usr/share/opensearch/plugins/opensearch-security/tools/securityadmin.sh -cd /usr/share/opensearch/plugins/opensearch-security/securityconfig -cacert /usr/share/opensearch/config/root-ca.pem -cert /usr/share/opensearch/config/kirk.pem -key /usr/share/opensearch/config/kirk-key.pem -rev -icl
    EOT
    environment = {
      AWS_REGION     = var.aws_region
      CLUSTER_NAME   = nonsensitive(data.aws_eks_cluster.cluster.name)
      POD_NAME       = local.opensearch_master_pod
      NAMESPACE      = var.namespace
      CONTAINER_NAME = var.chart_name
    }
  }
}

resource "null_resource" "refresh_dashboards" {
  depends_on = [helm_release.opensearch_dashboards, time_sleep.wait_30_seconds, null_resource.update_security_config]

  triggers = {
    helm_changes = nonsensitive(sha256(jsonencode(helm_release.opensearch.values)))
  }
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region $AWS_REGION --name $CLUSTER_NAME;
      kubectl delete pods -l $LABEL -n $NAMESPACE
    EOT
    environment = {
      AWS_REGION     = var.aws_region
      CLUSTER_NAME   = nonsensitive(data.aws_eks_cluster.cluster.name)
      LABEL          = "app=opensearch-dashboards"
      NAMESPACE      = var.namespace
      CONTAINER_NAME = var.chart_name
    }
  }
}


# This resource resizes the persistent volume as helm can't
resource "null_resource" "resize_pvc" {
  triggers = {
    changes = var.opensearch_persistence_extended_size
  }
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region ${var.aws_region} --name $CLUSTER_NAME;
      PV_LIST=$(kubectl get pvc --namespace ${var.namespace} | grep ${var.opensearch_solution_name} | awk '{print $1}' | tr \\n " ");
      if [ "${var.opensearch_persistence_extended_size}" != "0" ]; then
        for PV in $PV_LIST; do
          kubectl patch pvc $PV -p '{"spec":{"resources":{"requests":{"storage":"${var.opensearch_persistence_extended_size}Gi"}}}}' --namespace ${var.namespace}
        done;
      fi;
    EOT
    environment = {
      CLUSTER_NAME = nonsensitive(data.aws_eks_cluster.cluster.name)
    }
  }
}




# This resource migrate the persistent volume type
resource "null_resource" "migrate_pvc" {
  count = var.opensearch_persistence_extended_type != null ? 1 : 0
  triggers = {
    changes = var.opensearch_persistence_extended_type
  }
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region ${var.aws_region} --name $CLUSTER_NAME;
      PV_LIST=$(kubectl get pvc --namespace ${var.namespace} | grep ${var.opensearch_solution_name} | awk '{print $1}' | tr \\n " ");
      for PV in $PV_LIST; do
        kubectl patch pvc $PV -p '{"metadata": {"annotations":{"ebs.csi.aws.com/volumeType":"${var.opensearch_persistence_extended_type}"}}}' --namespace ${var.namespace}
      done;
    EOT
    environment = {
      CLUSTER_NAME = nonsensitive(data.aws_eks_cluster.cluster.name)
    }
  }
}

# This resource extends the EBS throughput
resource "null_resource" "annotate_pvc_throughput" {
  count = var.opensearch_persistence_extended_throughput != null ? 1 : 0
  triggers = {
    changes = var.opensearch_persistence_extended_throughput
  }
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region ${var.aws_region} --name $CLUSTER_NAME;
      PV_LIST=$(kubectl get pvc --namespace ${var.namespace} | grep ${var.opensearch_solution_name} | awk '{print $1}' | tr \\n " ");
      for PV in $PV_LIST; do
        kubectl patch pvc $PV -p '{"metadata": {"annotations":{"ebs.csi.aws.com/throughput":"${var.opensearch_persistence_extended_throughput}"}}}' --namespace ${var.namespace}
      done;
    EOT
    environment = {
      CLUSTER_NAME = nonsensitive(data.aws_eks_cluster.cluster.name)
    }
  }
}

# This resource extends the EBS iops
resource "null_resource" "annotate_pvc_iops" {
  count = var.opensearch_persistence_extended_iops != null ? 1 : 0
  triggers = {
    changes = var.opensearch_persistence_extended_iops
  }
  provisioner "local-exec" {
    command = <<-EOT
      aws eks update-kubeconfig --region ${var.aws_region} --name $CLUSTER_NAME;
      PV_LIST=$(kubectl get pvc --namespace ${var.namespace} | grep ${var.opensearch_solution_name} | awk '{print $1}' | tr \\n " ");
      for PV in $PV_LIST; do
        kubectl patch pvc $PV -p '{"metadata": {"annotations":{"ebs.csi.aws.com/iops":"${var.opensearch_persistence_extended_iops}"}}}' --namespace ${var.namespace}
      done;
    EOT
    environment = {
      CLUSTER_NAME = nonsensitive(data.aws_eks_cluster.cluster.name)
    }
  }
}

# Opensearch service accounts - Not working must enhance role mappings
resource "elasticsearch_opensearch_user" "user" {
  depends_on = [helm_release.opensearch]
  for_each   = local.account_map
  username   = each.value.account
  password   = each.value.password
  backend_roles = [
    each.value.role
  ]
}
