data "aws_caller_identity" "current" {}

data "aws_secretsmanager_secret" "eks_cluster_info" {
  name = "${var.environment}/eks/cluster-info"
}

data "aws_secretsmanager_secret_version" "cluster_name" {
  secret_id = data.aws_secretsmanager_secret.eks_cluster_info.id
}

data "aws_secretsmanager_secret" "airflow_auth_info" {
  name = "/${var.environment}/airflow/basic-auth"
}

data "aws_secretsmanager_secret_version" "airflow_auth_info" {
  secret_id = data.aws_secretsmanager_secret.airflow_auth_info.id
}

data "aws_eks_cluster" "cluster" {
  name = jsondecode(data.aws_secretsmanager_secret_version.cluster_name.secret_string)["cluster_name"]
}
data "aws_eks_cluster_auth" "cluster_auth" {
  name = data.aws_eks_cluster.cluster.name
}

data "aws_secretsmanager_secret" "datadog_secrets" {
  name = "/${var.environment}/datadog/api-keys"
}

data "aws_secretsmanager_secret_version" "datadog_secrets" {
  secret_id = data.aws_secretsmanager_secret.datadog_secrets.id
}

data "aws_secretsmanager_secret" "search_auth_info" {
  name = "project-${var.environment}-application-eks-cluster/search/opensearch-engine-cluster"
}

data "aws_secretsmanager_secret_version" "search_auth_info" {
  secret_id = data.aws_secretsmanager_secret.search_auth_info.id
}
