resource "helm_release" "datadog_agent" {
  namespace        = var.helm_chart_namespace
  create_namespace = true
  name             = var.helm_chart_name
  repository       = var.helm_repository
  chart            = var.helm_chart
  version          = var.helm_chart_version
  timeout          = 600
  values = [templatefile("values/datadog_agent.yml", {
    cluster_name        = lower(data.aws_eks_cluster.cluster.id)
    dd_env              = var.environment
    api_key             = nonsensitive(jsondecode(data.aws_secretsmanager_secret_version.datadog_secrets.secret_string)["api_key"])
    site                = nonsensitive(jsondecode(data.aws_secretsmanager_secret_version.datadog_secrets.secret_string)["site"])
    airflow_url         = nonsensitive(jsondecode(data.aws_secretsmanager_secret_version.airflow_auth_info.secret_string)["url"])
    airflow_user        = nonsensitive(jsondecode(data.aws_secretsmanager_secret_version.airflow_auth_info.secret_string)["user"])
    airflow_password    = nonsensitive(jsondecode(data.aws_secretsmanager_secret_version.airflow_auth_info.secret_string)["password"])
    snowflake_password  = nonsensitive(jsondecode(data.aws_secretsmanager_secret_version.datadog_secrets.secret_string)["datadog_snowflake_password"])
    snowflake_account   = var.snowflake_account
    snowflake_user      = var.snowflake_user
    snowflake_role      = var.snowflake_role
    opensearch_url      = "https://${nonsensitive(jsondecode(data.aws_secretsmanager_secret_version.search_auth_info.secret_string)["opensearch-url"])}"
    opensearch_user     = nonsensitive(jsondecode(data.aws_secretsmanager_secret_version.search_auth_info.secret_string)["role-admin-name"])
    opensearch_password = nonsensitive(jsondecode(data.aws_secretsmanager_secret_version.search_auth_info.secret_string)["role-admin-password"])
    apm_enable          = var.datadog_apm_enable

    })
  ]
}

resource "kubernetes_service" "agent_alias" {
  depends_on = [helm_release.datadog_agent]
  metadata {
    name      = "agent"
    namespace = var.helm_chart_namespace
  }
  spec {
    type          = "ExternalName"
    external_name = "${var.helm_chart_name}.${var.helm_chart_namespace}.svc.cluster.local"
  }
}

resource "kubernetes_service" "cluster_agent_alias" {
  depends_on = [helm_release.datadog_agent]
  metadata {
    name      = "cluster-agent"
    namespace = var.helm_chart_namespace
  }
  spec {
    type          = "ExternalName"
    external_name = "${var.helm_chart_name}-cluster-agent.${var.helm_chart_namespace}.svc.cluster.local"
  }
}
