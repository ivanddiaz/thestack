variable "aws_region" {
  type        = string
  description = "AWS region env config for the provider"
  default     = "us-east-1"
}

variable "company_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "acme"
}

variable "product_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "environment" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "testing"
}

variable "application_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
  default     = "coyote@acme.com"
}

variable "stack_for_tagging" {
  type        = string
  description = "The stack that owns the resource for tagging"
  default     = ""
}

variable "snowflake_user" {
  type        = string
  description = "User for datadog account usage queries to snowflake"
  default     = "DATADOG_USER"
}

variable "snowflake_role" {
  type        = string
  description = "Role for datadog account usage queries to snowflake"
  default     = "SF_DATADOG_FR"
}

variable "snowflake_account" {
  type        = string
  description = "Snowflake account to monitor usage"
}

variable "datadog_secrets" {
  description = "Datadog secrets"
  type        = string
  default     = "/datadog"
}

variable "helm_repository" {
  description = "Helm repository"
  type        = string
  default     = "https://helm.datadoghq.com"
}

variable "helm_chart" {
  description = "Helm chart"
  type        = string
  default     = "datadog"
}

variable "helm_chart_version" {
  description = "Helm chart version"
  type        = string
  default     = "3.49.4"
}

variable "helm_chart_namespace" {
  description = "Namespace for helm deployment"
  type        = string
  default     = "datadog"
}

variable "helm_chart_name" {
  description = "Names for helm deployment"
  type        = string
  default     = "datadog"
}

variable "datadog_apm_enable" {
  description = "This will enable or disable (true/false) the apm module in values/datadog_agent.yaml configuration file"
  type        = string
  default     = "true"
}
