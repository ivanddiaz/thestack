module "airflow_tags" {
  source           = "..//aws/common-label"
  product_name     = var.product_name
  environment      = var.environment
  application_name = var.application_name
  delimiter        = "-"
  email            = var.email_for_tagging
  stack            = local.stack
  module_name      = local.module_name
  tags = {
    Vertical = var.vertical_for_tagging
  }
}

resource "aws_efs_file_system" "airflow_file_system" {
  count            = var.efs_burst_mode ? 1 : 0
  creation_token   = local.file_system_name
  tags             = module.airflow_tags.tags
  performance_mode = "generalPurpose"
  throughput_mode  = "bursting"
  encrypted        = true
  lifecycle {
    ignore_changes = [
      throughput_mode,
    ]
  }
}

resource "aws_efs_file_system" "airflow_file_system_provisionedMode" {
  count                           = !(var.efs_burst_mode) ? 1 : 0
  creation_token                  = local.file_system_name
  tags                            = module.airflow_tags.tags
  performance_mode                = "generalPurpose"
  throughput_mode                 = "provisioned"
  provisioned_throughput_in_mibps = var.provisioned_throughput_in_mibps
  encrypted                       = true
}

module "efs_sg" {
  source              = "..//aws/sg"
  environment         = var.environment
  product_name        = var.product_name
  application_name    = "efs-mount"
  vpc_id              = var.vpc_id
  resource_type       = local.product_used_by
  email_for_tagging   = var.email_for_tagging
  stack_for_tagging   = local.stack
  description         = "EFS Security Group"
  ingress_cidr_blocks = local.ingress_cidr_blocks
  ingress_rules       = local.ingress_rules
}


resource "aws_efs_mount_target" "efs_storage_mount" {
  count           = length(local.efs_grouped_subnets)
  file_system_id  = local.file_system_id
  subnet_id       = local.efs_grouped_subnets[count.index]
  security_groups = [module.efs_sg.this_security_group_id]
}

resource "kubernetes_storage_class" "storage_class" {
  metadata {
    name = "efs-sc"
  }
  storage_provisioner = "efs.csi.aws.com"
}

resource "kubernetes_persistent_volume" "efs_pv" {
  metadata {
    name = "efs-pv-airflow"
    labels = {
      type = "efs-airflow"
    }
  }
  spec {
    capacity = {
      storage = "5Gi"
    }
    access_modes       = ["ReadWriteMany"]
    storage_class_name = kubernetes_storage_class.storage_class.metadata.0.name
    volume_mode        = "Filesystem"
    mount_options      = ["tls"]
    persistent_volume_source {
      csi {
        driver        = "efs.csi.aws.com"
        volume_handle = local.file_system_id
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "efs_pv_claim" {
  metadata {
    name      = local.pv_claim_name
    namespace = local.namespace
  }
  spec {
    access_modes       = ["ReadWriteMany"]
    storage_class_name = kubernetes_storage_class.storage_class.metadata.0.name
    resources {
      requests = {
        storage = "5Gi"
      }
    }
    selector {
      match_labels = {
        type = kubernetes_persistent_volume.efs_pv.metadata.0.labels.type
      }
    }
    volume_name = kubernetes_persistent_volume.efs_pv.metadata.0.name
  }
}

