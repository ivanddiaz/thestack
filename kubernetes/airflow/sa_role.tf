module "sa_role_label" {
  source           = "..//aws/common-label"
  product_name     = var.product_name
  environment      = var.environment
  application_name = var.application_name
  attributes       = ["SA"]
  delimiter        = "-"
  email            = var.email_for_tagging
  stack            = local.stack
  module_name      = local.module_name
  tags = {
    Vertical = var.vertical_for_tagging
  }
}




resource "aws_iam_role" "airflow_irsa_role" {
  name = module.sa_role_label.id
  assume_role_policy = templatefile("${local.config_path}/oidc_assume_role.json", {
    oidc_arn  = local.oidc_arn,
    oidc_name = replace(local.oidc_arn, "/^(.*provider/)/", ""),
  ns_sa = "airflow:airflow" })
  max_session_duration = local.max_session_duration
  tags                 = module.sa_role_label.tags
}


resource "aws_iam_role_policy" "sa_iam_role" {
  name = module.sa_role_label.id
  role = aws_iam_role.airflow_irsa_role.id
  policy = templatefile("${local.config_path}/airflow_policy.json", {
    account_id = data.aws_caller_identity.current.account_id
  })


}
