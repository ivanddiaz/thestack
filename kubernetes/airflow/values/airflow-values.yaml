# https://github.com/airflow-helm/charts/blob/main/charts/airflow/values.yaml
airflow:
  image:
    repository: ${airflow_image}
    tag: "${airflow_version}"
    pullSecret: secret-acme-devops
    uid: 50000
    gid: 0
  executor: KubernetesExecutor
  config:
    AIRFLOW__API__AUTH_BACKEND: airflow.api.auth.backend.basic_auth

    AIRFLOW_CORE_MIN_SERIALIZED_DAG_UPDATE_INTERVAL: 30
    AIRFLOW_CORE_MIN_SERIALIZED_DAG_FETCH_INTERVAL: 10
    AIRFLOW_CORE_MAX_NUM_RENDERED_TI_FIELDS_PER_TASK: 30

    AIRFLOW__CORE__DEFAULT_TIMEZONE: America/New_York
    AIRFLOW__CORE__LOAD_EXAMPLES: ${load_example}
    AIRFLOW__CORE__PARALLELISM: ${core_parallelism}
    AIRFLOW__CORE__KILLED_TASK_CLEANUP_TIME: 3600
    AIRFLOW__CORE__MAX_ACTIVE_TASKS_PER_DAG: 32
    AIRFLOW__CORE__DAGBAG_IMPORT_TIMEOUT: 300

    AIRFLOW__KUBERNETES__DELETE_WORKER_PODS: True
    AIRFLOW__KUBERNETES__DELETE_WORKER_PODS_ON_FAILURE: False

    AIRFLOW__SCHEDULER__JOB_HEARTBEAT_SEC: 30
    AIRFLOW__SCHEDULER__MAX_DAGRUNS_PER_LOOP_TO_SCHEDULE: 60
    AIRFLOW__SCHEDULER__MAX_DAGRUNS_TO_CREATE_PER_LOOP: 30
    AIRFLOW__SCHEDULER__PARSING_PROCESSES: 8
    AIRFLOW__SCHEDULER__SCHEDULE_AFTER_TASK_EXECUTION: False

    AIRFLOW__WEBSERVER__RBAC: True
    AIRFLOW__WEBSERVER__DEFAULT_UI_TIMEZONE: America/New_York
    AIRFLOW__WEBSERVER__BASE_URL: ${airflow_base_url}

    AIRFLOW__LOGGING__LOGGING_LEVEL: ${log_level}
    AIRFLOW__LOGGING__FAB_LOGGING_LEVEL: WARNING
    AIRFLOW__LOGGING__BASE_LOG_FOLDER: /opt/airflow/logs
    AIRFLOW__LOGGING__REMOTE_LOGGING: True
    AIRFLOW__LOGGING__REMOTE_BASE_LOG_FOLDER: ${s3_log_bucket}

    AWS_DEFAULT_REGION: us-east-1

    DD_AGENT_HOST: agent.datadog
    DD_ENV: ${environment}
    DD_SERVICE: airflow-core

    PYTHONPATH: /opt/airflow/dags/:$PYTHONPATH

    #Secret Backend SSM Param
    AIRFLOW__SECRETS__BACKEND: airflow.contrib.secrets.aws_systems_manager.SystemsManagerParameterStoreBackend
    AIRFLOW__SECRETS__BACKEND_KWARGS: '{"connections_prefix": "/airflow/connections", "variables_prefix": "/airflow/variables"}'

    #Datadog config
    AIRFLOW__METRICS__STATSD_ON: True
    AIRFLOW__METRICS__STATSD_HOST: agent.datadog
    AIRFLOW__METRICS__STATSD_PORT: 8125
    AIRFLOW__METRICS__STATSD_PREFIX: airflow

    #Airflow Var definitions
    AIRFLOW_VAR_ENVIRONMENT: ${environment}

  users:
    - username: admin
      password: ${airflow_admin_user_pwd}
      role: Admin
      email: admin@airflow.local
      firstName: Administrator
      lastName: Access
    - username: ${api_read_only_user}
      password: ${api_read_only_password}
      role: Viewer
      email: ${api_read_only_user}@airflow.local
      firstName: Read Only
      lastName: Access

  extraContainers:
    - name: s3-sync
      image: acme-devops-docker-local.jfrog.io/acme/airflow-dag-sync:1.0.0
      env:
        - name: S3_BUCKET_PATH
          value: ${s3_dag_path}
        - name: CODE_DESTINATION_PATH
          value: /opt/airflow/dags
        - name: SYNC_PERIOD
          value: "${dag_sync_interval}" # Quoted due to: json: cannot unmarshal number into Go struct field EnvVar.spec.template.spec.initContainers.env.value of type string
      volumeMounts:
        - mountPath: /opt/airflow/dags
          name: airflow-data

  kubernetesPodTemplate:
    stringOverride: |-
      apiVersion: v1
      kind: Pod
      metadata:
        name: dummy-name
      spec:
        volumes:
          - name: airflow-data
            persistentVolumeClaim:
              claimName: "${pv_claim_name}"
        restartPolicy: Never
        imagePullSecrets:
          - name: secret-acme-devops
        serviceAccountName: airflow
        securityContext:
          fsGroup: 0
        containers:
          - name: kubernetes-executor
            image: "${airflow_image}:${airflow_version}"
            imagePullPolicy: IfNotPresent
            securityContext:
              runAsUser: 50000
              runAsGroup: 0
            envFrom:
              - secretRef:
                  name: "${release_name}-config-envs"
            env:
              ## enable the `/entrypoint` db connection check
              - name: CONNECTION_CHECK_MAX_COUNT
                value: "20"
              ## KubernetesExecutor Pods use LocalExecutor internally
              - name: AIRFLOW__CORE__EXECUTOR
                value: LocalExecutor
              - name: DATABASE_PASSWORD
                valueFrom:
                  secretKeyRef:
                    name: airflow-secrets
                    key: postgresql-password
              - name: CONNECTION_CHECK_MAX_COUNT
                value: "0"
            volumeMounts:
              - mountPath: /opt/airflow/dags
                name: airflow-data

scheduler:
  replicas: 2
  extraVolumeMounts:
    - mountPath: /opt/airflow/dags
      name: airflow-data
  extraVolumes:
    - name: airflow-data
      persistentVolumeClaim:
        claimName: ${pv_claim_name}
  livenessProbe:
    timeoutSeconds: 60 # This is the new default in recern airflow versions

web:
  webserverConfig:
    stringOverride: |
      from airflow import configuration as conf
      from flask_appbuilder.security.manager import AUTH_OAUTH

      SQLALCHEMY_DATABASE_URI = conf.get('core', 'SQL_ALCHEMY_CONN')

      AUTH_TYPE = AUTH_OAUTH

      # registration configs
      AUTH_USER_REGISTRATION = True  # allow users who are not already in the FAB DB
      AUTH_USER_REGISTRATION_ROLE = "Public"  # this role will be given in addition to any AUTH_ROLES_MAPPING

      # the list of providers which the user can choose from
      OAUTH_PROVIDERS = [
          {
              'name': 'okta',
              'icon': 'fa-circle-o',
              'token_key': 'access_token',
              'remote_app': {
                  'client_id': '${client_id}',
                  'client_secret': '${client_secret}',
                  'api_base_url': '${api_base_url}',
                  'client_kwargs': {
                      'scope': 'openid profile email groups'
                  },
                  'access_token_url': '${access_token_url}',
                  'authorize_url': '${authorize_url}',
              }
          }
      ]

      # a mapping from the values of `userinfo["role_keys"]` to a list of FAB roles
      AUTH_ROLES_MAPPING = {
          "PELICAN_${environment}_AIRFLOW_USERS": ["User"],
          "PELICAN_${environment}_AIRFLOW_ADMINS": ["Admin"],
          "PELICAN_${environment}_AIRFLOW_VIEWER": ["Viewer"],
          "PELICAN_${environment}_AIRFLOW_OPS": ["Op"]
      }

      # if we should replace ALL the user's roles each login, or only on registration
      AUTH_ROLES_SYNC_AT_LOGIN = True

      # force users to re-auth after 30min of inactivity (to keep roles in sync)
      PERMANENT_SESSION_LIFETIME = 1800
  extraVolumeMounts:
    - mountPath: /opt/airflow/dags
      name: airflow-data
  extraVolumes:
    - name: airflow-data
      persistentVolumeClaim:
        claimName: ${pv_claim_name}

logs:
  persistence:
    existingClaim: efs-claim-airflow-logs

dags:
  persistence:
    existingClaim: efs-pv-claim-airflow
    accessMode: ReadWriteMany

ingress:
  enabled: true
  web:
    annotations:
      kubernetes.io/ingress.class: ${ingress_class_name}
    host: ${host_name}

serviceAccount:
  name: airflow
  annotations:
    eks.amazonaws.com/role-arn: ${airflow_role_arn}

externalDatabase:
  type: postgres
  host: ${database_host}
  database: ${database_name}
  user: ${db_user}
  passwordSecret: ${db_secret}

workers:
  enabled: false

flower:
  enabled: false

postgresql:
  enabled: false

redis:
  enabled: false
