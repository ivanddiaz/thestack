resource "helm_release" "airflow" {
  name       = local.release_name
  chart      = var.chart_name
  repository = var.repository
  namespace  = var.namespace
  version    = var.chart_version
  timeout    = 600

  values = [
    templatefile("values/airflow-values.yaml", {
      load_example           = var.load_example
      airflow_image          = local.airflow_image
      airflow_version        = local.airflow_version
      core_parallelism       = var.airflow_core_parallelism
      s3_log_bucket          = "s3://${module.s3_log_bucket.s3_bucket}"
      airflow_base_url       = var.airflow_base_url
      persistence_enable     = local.persistence_enable
      ingress_class_name     = var.ingress_class_name
      host_name              = var.host_name
      airflow_role_arn       = aws_iam_role.airflow_irsa_role.arn
      database_host          = jsondecode(data.aws_secretsmanager_secret_version.db_info_secret_version.secret_string)["database_host"]
      database_name          = jsondecode(data.aws_secretsmanager_secret_version.db_info_secret_version.secret_string)["database_name"]
      db_user                = var.db_user
      db_secret              = var.db_secret
      client_id              = okta_app_oauth.okta_airflow_app.client_id
      client_secret          = okta_app_oauth.okta_airflow_app.client_secret
      api_base_url           = jsondecode(data.aws_secretsmanager_secret_version.okta.secret_string)["api_base_url"],
      access_token_url       = jsondecode(data.aws_secretsmanager_secret_version.okta.secret_string)["access_token_url"],
      authorize_url          = jsondecode(data.aws_secretsmanager_secret_version.okta.secret_string)["authorize_url"],
      pv_claim_name          = local.pv_claim_name
      log_level              = var.log_level
      airflow_admin_user_pwd = random_password.airflow_password.result
      release_name           = local.release_name
      s3_dag_path            = local.s3_dag_path
      dag_sync_interval      = var.dag_sync_interval
      environment            = upper(var.environment)
      api_read_only_user     = random_string.api_viewer_username.result
      api_read_only_password = random_password.api_viewer_password.result
    })
  ]
  depends_on = [kubernetes_persistent_volume_claim.efs_pv_claim]
}

module "airflow_admin_secrets" {
  source                      = "..//aws/secrets"
  use_prefix                  = true
  secret_name                 = "basic-auth"
  secret_recovery_window_days = 0
  company_name                = "acme"
  environment                 = var.environment
  product_name                = "airflow"
  email_for_tagging           = "coyote@acme.com"
  stack_for_tagging           = "aws"
  terraform_user              = "IAC_USER"

  # Use this map as pipeline main input
  secrets = {
    comment     = "Airflow basic-auth for API access"
    description = "Airflow API access auth"
  }

  # Use this auxiliary map for resource generated secrets - keys must match with user_secrets_map
  resource_secrets = {
    user     = "admin"
    password = random_password.airflow_password.result
    url      = var.airflow_base_url
  }
}

resource "kubernetes_service" "api_alias" {
  depends_on = [helm_release.airflow]
  metadata {
    name      = local.cluster_endpoint_alias
    namespace = var.namespace
  }
  spec {
    type          = "ExternalName"
    external_name = local.cluster_endpoint_web
  }
}

