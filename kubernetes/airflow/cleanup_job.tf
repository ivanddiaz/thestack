resource "kubernetes_role" "airflow-pod-cleanup" {
  metadata {
    name      = "${var.namespace}-pod-cleanup"
    namespace = var.namespace
  }

  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list", "delete"]
  }
}

resource "kubernetes_service_account" "airflow-pod-cleanup" {
  metadata {
    name      = "${var.namespace}-pod-cleanup-sa"
    namespace = var.namespace
  }
}


resource "kubernetes_role_binding" "airflow-pod-cleanup" {
  metadata {
    name      = "${var.namespace}-pod-cleanup"
    namespace = var.namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "${var.namespace}-pod-cleanup"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "${var.namespace}-pod-cleanup-sa"
    namespace = var.namespace
  }
}

resource "kubectl_manifest" "airflow-pod-cleanup" {
  force_new = true
  yaml_body = templatefile("config/airflow-pod-cleanup.yml", {
    name            = "${var.namespace}-pod-cleanup"
    namespace       = var.namespace
    schedule        = "0 * * * *"
    service-account = "${var.namespace}-pod-cleanup-sa"
    }
  )
}
