data "aws_eks_cluster" "cluster_data" {
  name = local.cluster_name
}
data "aws_eks_cluster_auth" "auth_data" {
  name = local.cluster_name
}

data "aws_secretsmanager_secret" "db_info" {
  name = local.db_info
}

data "aws_secretsmanager_secret_version" "db_info_secret_version" {
  secret_id = data.aws_secretsmanager_secret.db_info.id
}

data "aws_secretsmanager_secret" "airflow_db_secret" {
  name = local.airflow_db_secret
}

data "aws_secretsmanager_secret_version" "airflow_db_pwd" {
  secret_id = data.aws_secretsmanager_secret.airflow_db_secret.id
}


data "aws_caller_identity" "current" {}


data "aws_secretsmanager_secret" "eks_cluster_secret" {
  name = local.cluster_secret_key
}

data "aws_secretsmanager_secret_version" "eks_cluster_secret_version" {
  secret_id = data.aws_secretsmanager_secret.eks_cluster_secret.id
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

data "aws_subnets" "private" {

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.selected.id]
  }
  tags = {
    Network = "Private"
  }
}

data "aws_subnet" "subnet_details" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.key
}

data "aws_s3_bucket" "code_lib_bucket" {
  bucket = local.code_bucket_name
}

locals {
  efs_availability_zones = distinct([for subnet in data.aws_subnet.subnet_details : subnet.availability_zone])
  efs_subnet_per_zone    = { for subnet in data.aws_subnet.subnet_details : subnet.availability_zone => subnet.id... }
  efs_grouped_subnets    = [for az, subnet_ids in local.efs_subnet_per_zone : subnet_ids[0]]
  file_system_name       = "AIRFLOW_DAG_FILE_SYSTEM"
  file_system_id         = var.efs_burst_mode ? aws_efs_file_system.airflow_file_system[0].id : aws_efs_file_system.airflow_file_system_provisionedMode[0].id
  airflow_dag_dir        = "airflow-dags"
  namespace              = "airflow"
  config_path            = "./config"
  max_session_duration   = 36000
  airflow_image          = "acme-devops-docker-local.jfrog.io/acme/airflowimage-base"
  airflow_version        = var.airflow_image_version
  persistence_enable     = "false"
  stack                  = "Airflow"
  module_name            = "Airflow"
  product_used_by        = "Airflow"
  ingress_cidr_blocks    = ["10.0.0.0/8"]
  #TODO- Need to check if we can restrict to the EKS Node Group
  ingress_rules = ["nfs-tcp"]

  db_info                    = "${var.environment}/rds/"
  airflow_db_secret          = "${var.environment}/rds/airflow-user"
  code_bucket_name           = lower("us-east-1-acme-${var.product_name}-${var.environment}-code-lib.s3.amazonaws.com")
  efs_sync_source_path       = "/jobs"
  pv_claim_name              = "efs-pv-claim-airflow"
  cluster_secret_key         = (var.cluster_secret_key != "") ? var.cluster_secret_key : "${var.environment}/eks/cluster-info"
  cluster_endpoint_web       = "${local.release_name}-web.${var.namespace}.svc.cluster.local"
  cluster_endpoint_alias     = "api"
  cluster_endpoint_web_alias = "${local.cluster_endpoint_alias}.${var.namespace}"

  lifecycle_rules = [
    {
      id                                     = "airflow-logs"
      enabled                                = true
      prefix                                 = null
      tags                                   = null
      status                                 = "Enabled"
      abort_incomplete_multipart_upload_days = 3
      expiration_config = [
        {
          days                         = null
          expired_object_delete_marker = true
        }
      ]
      noncurrent_version_expiration_config = [
        {
          days = 15
        }
      ]
      transitions_config = [
        {
          days          = 30
          storage_class = "INTELLIGENT_TIERING"
        }
      ]
      noncurrent_version_transitions_config = []
    }
  ]
  release_name = "airflow-acme"
  s3_dag_path  = "s3://us-east-1-acme-project-${var.environment}-code-lib.s3.amazonaws.com/jobs"
  oidc_arn     = jsondecode(data.aws_secretsmanager_secret_version.eks_cluster_secret_version.secret_string)["oidc_arn"]
  cluster_name = jsondecode(data.aws_secretsmanager_secret_version.eks_cluster_secret_version.secret_string)["cluster_name"]
  airflow_credential = {
    "airflow_password" = random_password.airflow_password.result
    "fernet_key"       = random_password.fernet_key.result
  }

}

resource "random_password" "airflow_password" {
  length           = 16
  special          = true
  override_special = "[]#%&?"
}

resource "random_password" "fernet_key" {
  length           = 16
  special          = true
  override_special = "[]#%&?"
}

data "aws_iam_role" "dev_admin_role" {
  name = var.devopsadmin_role
}

data "aws_secretsmanager_secret" "okta" {
  name = lower("/acme/okta/api/token")
}

data "aws_secretsmanager_secret_version" "okta" {
  secret_id = data.aws_secretsmanager_secret.okta.id
}

data "aws_route53_zone" "sub_domain" {
  name         = var.sub_domain
  private_zone = false
}

data "kubernetes_ingress_v1" "airflow_ingress" {
  metadata {
    name      = "${local.release_name}-web"
    namespace = var.namespace
  }
  depends_on = [
    helm_release.airflow,
    time_sleep.wait_45_seconds
  ]
  #provider = kubernetes
}

