# Standard route53 DNS record pointing to an ALB
resource "aws_route53_record" "airflow" {
  zone_id = data.aws_route53_zone.sub_domain.zone_id
  name    = var.host_name
  type    = "CNAME"
  ttl     = 60
  records = [
    data.kubernetes_ingress_v1.airflow_ingress.status[0].load_balancer[0].ingress[0].hostname
  ]
}

resource "time_sleep" "wait_45_seconds" {
  create_duration = "45s"
}
