module "s3_log_bucket" {
  source                  = "..//aws/s3"
  region                  = var.aws_region
  company_name            = var.company_name
  product_name            = var.product_name
  environment             = var.environment
  application_name        = join("-", [var.application_name, "log"])
  email_for_tagging       = var.email_for_tagging
  module_name_for_tagging = var.application_name
  lifecycle_rules         = local.lifecycle_rules
}
