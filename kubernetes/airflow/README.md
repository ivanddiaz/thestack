#Instruction
We need to do following steps manually before setting up Airflow

1. Create RDS Instance using rds module.
2. Verify secrete created for Airflow for the RDS connection under the namespace -
   {env}/rds/airflow_user
3. Create Airflow Secret key under the name - ariflow/fernetkey and define airflow-fernetKey and airflow-password
4. Create Secret for JFROG Docker Imager in the specific AWS account
5. Creat OKTA App for Airflow in Okta
6. Store the credential OKTA App in AWS

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.1.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.5.0 |
| <a name="requirement_kubectl"></a> [kubectl](#requirement\_kubectl) | >= 1.14.0 |
| <a name="requirement_okta"></a> [okta](#requirement\_okta) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.5.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | n/a |
| <a name="provider_kubectl"></a> [kubectl](#provider\_kubectl) | >= 1.14.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |
| <a name="provider_okta"></a> [okta](#provider\_okta) | >= 4.0.0 |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_airflow_admin_secrets"></a> [airflow\_admin\_secrets](#module\_airflow\_admin\_secrets) | ..//aws/secrets | n/a |
| <a name="module_airflow_tags"></a> [airflow\_tags](#module\_airflow\_tags) | ..//aws/common-label | n/a |
| <a name="module_efs_sg"></a> [efs\_sg](#module\_efs\_sg) | ..//aws/sg | n/a |
| <a name="module_s3_log_bucket"></a> [s3\_log\_bucket](#module\_s3\_log\_bucket) | ..//aws/s3 | n/a |
| <a name="module_sa_role_label"></a> [sa\_role\_label](#module\_sa\_role\_label) | ..//aws/common-label | n/a |
| <a name="module_secret"></a> [secret](#module\_secret) | ..//aws/secrets | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_efs_file_system.airflow_file_system](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_file_system) | resource |
| [aws_efs_file_system.airflow_file_system_provisionedMode](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_file_system) | resource |
| [aws_efs_mount_target.efs_storage_mount](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_mount_target) | resource |
| [aws_iam_role.airflow_irsa_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.sa_iam_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_route53_record.airflow](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_secretsmanager_secret.airflow_credential](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret) | resource |
| [aws_secretsmanager_secret_policy.airflow_credential_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_policy) | resource |
| [aws_secretsmanager_secret_version.airflow_credential_version](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_version) | resource |
| [helm_release.airflow](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubectl_manifest.airflow-pod-cleanup](https://registry.terraform.io/providers/gavinbunney/kubectl/latest/docs/resources/manifest) | resource |
| [kubernetes_persistent_volume.efs_pv](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/persistent_volume) | resource |
| [kubernetes_persistent_volume_claim.efs_pv_claim](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/persistent_volume_claim) | resource |
| [kubernetes_role.airflow-pod-cleanup](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role) | resource |
| [kubernetes_role_binding.airflow-pod-cleanup](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding) | resource |
| [kubernetes_secret.data_secrets](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_service.api_alias](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service) | resource |
| [kubernetes_service_account.airflow-pod-cleanup](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account) | resource |
| [kubernetes_storage_class.storage_class](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class) | resource |
| [okta_app_group_assignment.group_assignment](https://registry.terraform.io/providers/okta/okta/latest/docs/resources/app_group_assignment) | resource |
| [okta_app_oauth.okta_airflow_app](https://registry.terraform.io/providers/okta/okta/latest/docs/resources/app_oauth) | resource |
| [okta_group.airflow_group](https://registry.terraform.io/providers/okta/okta/latest/docs/resources/group) | resource |
| [random_password.airflow_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_password.api_viewer_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_password.fernet_key](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_string.api_viewer_username](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [time_sleep.wait_45_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_eks_cluster.cluster_data](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster) | data source |
| [aws_eks_cluster_auth.auth_data](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth) | data source |
| [aws_iam_role.dev_admin_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_role) | data source |
| [aws_route53_zone.sub_domain](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |
| [aws_s3_bucket.code_lib_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/s3_bucket) | data source |
| [aws_secretsmanager_secret.airflow_db_secret](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret.db_info](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret.eks_cluster_secret](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret.okta](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.airflow_db_pwd](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [aws_secretsmanager_secret_version.db_info_secret_version](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [aws_secretsmanager_secret_version.eks_cluster_secret_version](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [aws_secretsmanager_secret_version.okta](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [aws_subnet.subnet_details](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |
| [aws_subnets.private](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnets) | data source |
| [aws_vpc.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |
| [kubernetes_ingress_v1.airflow_ingress](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data-sources/ingress_v1) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_airflow_base_url"></a> [airflow\_base\_url](#input\_airflow\_base\_url) | Airflow Base URL | `string` | n/a | yes |
| <a name="input_airflow_core_parallelism"></a> [airflow\_core\_parallelism](#input\_airflow\_core\_parallelism) | This defines the maximum number of task instances that can run concurrently per scheduler in Airflow, regardless of the worker count. Generally this value, multiplied by the number of schedulers in your cluster, is the maximum number of task instances with the running state in the metadata database. Airflow default value is 32 | `number` | `128` | no |
| <a name="input_airflow_image_version"></a> [airflow\_image\_version](#input\_airflow\_image\_version) | acme-devops-docker-local.jfrog.io/acme/airflowimage-base container version | `string` | n/a | yes |
| <a name="input_application_name"></a> [application\_name](#input\_application\_name) | Application name, e.g. 'api' | `string` | `"airflow"` | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS region env config for the provider | `string` | n/a | yes |
| <a name="input_chart_name"></a> [chart\_name](#input\_chart\_name) | Chart name | `string` | `"airflow"` | no |
| <a name="input_chart_version"></a> [chart\_version](#input\_chart\_version) | Chart Version | `string` | `"8.5.2"` | no |
| <a name="input_cluster_secret_key"></a> [cluster\_secret\_key](#input\_cluster\_secret\_key) | EKS Cluster Secret Key | `string` | `""` | no |
| <a name="input_company_name"></a> [company\_name](#input\_company\_name) | Company name as per naming standard for S3 bucket | `string` | `"acme"` | no |
| <a name="input_dag_sync_interval"></a> [dag\_sync\_interval](#input\_dag\_sync\_interval) | DAG Sync Interval | `number` | `60` | no |
| <a name="input_db_secret"></a> [db\_secret](#input\_db\_secret) | Database Secret | `string` | `"airflow-secrets"` | no |
| <a name="input_db_user"></a> [db\_user](#input\_db\_user) | Database User | `string` | `"airflow_user"` | no |
| <a name="input_devopsadmin_role"></a> [devopsadmin\_role](#input\_devopsadmin\_role) | The DevAdmin Role name | `string` | `"DEVADMIN"` | no |
| <a name="input_efs_burst_mode"></a> [efs\_burst\_mode](#input\_efs\_burst\_mode) | BustMode for EFS Drive | `bool` | `true` | no |
| <a name="input_efs_data_sync_schedule"></a> [efs\_data\_sync\_schedule](#input\_efs\_data\_sync\_schedule) | EFS Data Sync | `string` | `"cron(0/5 * * * ? *)"` | no |
| <a name="input_email_for_tagging"></a> [email\_for\_tagging](#input\_email\_for\_tagging) | The email address for tagging the resources | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod' | `string` | n/a | yes |
| <a name="input_group_list"></a> [group\_list](#input\_group\_list) | List of groups | `list(any)` | `[]` | no |
| <a name="input_host_name"></a> [host\_name](#input\_host\_name) | host url | `string` | n/a | yes |
| <a name="input_ingress_class_name"></a> [ingress\_class\_name](#input\_ingress\_class\_name) | Ingress Class Name | `string` | n/a | yes |
| <a name="input_jfrog_devops_docker"></a> [jfrog\_devops\_docker](#input\_jfrog\_devops\_docker) | Airflow DB Secret | `string` | `"jfrog/devops/docker/credential"` | no |
| <a name="input_lifecycle_rules"></a> [lifecycle\_rules](#input\_lifecycle\_rules) | A data structure to create lifecycle rules for the bucket | <pre>list(object({<br>    id                                     = string<br>    prefix                                 = string<br>    tags                                   = map(string)<br>    enabled                                = bool<br>    abort_incomplete_multipart_upload_days = number<br>    expiration_config = list(object({<br>      days                         = number<br>      expired_object_delete_marker = bool<br>    }))<br>    noncurrent_version_expiration_config = list(object({<br>      days = number<br>    }))<br>    transitions_config = list(object({<br>      days          = number<br>      storage_class = string<br>    }))<br>    noncurrent_version_transitions_config = list(object({<br>      days          = number<br>      storage_class = string<br>    }))<br>  }))</pre> | `[]` | no |
| <a name="input_load_example"></a> [load\_example](#input\_load\_example) | Airflow default example | `bool` | `false` | no |
| <a name="input_log_level"></a> [log\_level](#input\_log\_level) | Log Level INFO/DEBUG/ERROR/WARN | `string` | `"INFO"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace | `string` | `"airflow"` | no |
| <a name="input_okta_app_config"></a> [okta\_app\_config](#input\_okta\_app\_config) | OKTA Application Config | <pre>object({<br>    okta_app_name             = string<br>    login_uri                 = string<br>    post_logout_redirect_uris = list(string)<br>    redirect_uris             = list(string)<br>    logo_uri                  = string<br>  })</pre> | n/a | yes |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | Product Name, e.g. 'mc','fb','dm' | `string` | n/a | yes |
| <a name="input_provisioned_throughput_in_mibps"></a> [provisioned\_throughput\_in\_mibps](#input\_provisioned\_throughput\_in\_mibps) | Provisioned Throughput MBps | `number` | `50` | no |
| <a name="input_repository"></a> [repository](#input\_repository) | Repository name | `string` | `"https://airflow-helm.github.io/charts"` | no |
| <a name="input_sub_domain"></a> [sub\_domain](#input\_sub\_domain) | Sub Domain | `string` | n/a | yes |
| <a name="input_vertical_for_tagging"></a> [vertical\_for\_tagging](#input\_vertical\_for\_tagging) | Stack name for a collection of resources for tagging | `string` | `"acme"` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC ID | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
