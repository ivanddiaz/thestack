variable "vpc_id" {
  type        = string
  description = "VPC ID"
}

variable "devopsadmin_role" {
  type        = string
  description = "The DevAdmin Role name"
  default     = "DEVADMIN"
}

variable "chart_name" {
  type        = string
  description = "Chart name"
  default     = "airflow"
}

variable "chart_version" {
  type        = string
  description = "Chart Version"
  default     = "8.5.2"
}

variable "repository" {
  type        = string
  description = "Repository name"
  default     = "https://airflow-helm.github.io/charts"
}
variable "namespace" {
  type        = string
  description = "Namespace"
  default     = "airflow"
}

variable "jfrog_devops_docker" {
  type        = string
  description = "Airflow DB Secret"
  default     = "jfrog/devops/docker/credential"
}

variable "load_example" {
  type        = bool
  description = "Airflow default example"
  default     = false
}

variable "log_level" {
  type        = string
  description = "Log Level INFO/DEBUG/ERROR/WARN"
  default     = "INFO"
}
variable "dag_sync_interval" {
  type        = number
  description = "DAG Sync Interval"
  default     = 60
}

variable "aws_region" {
  type        = string
  description = "AWS region env config for the provider"
}

variable "company_name" {
  type        = string
  description = "Company name as per naming standard for S3 bucket"
  default     = "acme"
}

variable "product_name" {
  type        = string
  description = "Product Name, e.g. 'mc','fb','dm'"
}

variable "environment" {
  type        = string
  description = "Environment, e.g. 'dev', 'qa', 'uat', 'staging', 'prod'"
}

variable "application_name" {
  type        = string
  description = "Application name, e.g. 'api'"
  default     = "airflow"
}

variable "email_for_tagging" {
  type        = string
  description = "The email address for tagging the resources"
}

variable "vertical_for_tagging" {
  type        = string
  description = "Stack name for a collection of resources for tagging"
  default     = "acme"
}
variable "efs_burst_mode" {
  type        = bool
  description = "BustMode for EFS Drive"
  default     = true

}

variable "provisioned_throughput_in_mibps" {
  type        = number
  description = "Provisioned Throughput MBps"
  default     = 50
}
variable "lifecycle_rules" {
  description = "A data structure to create lifecycle rules for the bucket"
  type = list(object({
    id                                     = string
    prefix                                 = string
    tags                                   = map(string)
    enabled                                = bool
    abort_incomplete_multipart_upload_days = number
    expiration_config = list(object({
      days                         = number
      expired_object_delete_marker = bool
    }))
    noncurrent_version_expiration_config = list(object({
      days = number
    }))
    transitions_config = list(object({
      days          = number
      storage_class = string
    }))
    noncurrent_version_transitions_config = list(object({
      days          = number
      storage_class = string
    }))
  }))
  default = []
}
variable "cluster_secret_key" {
  description = "EKS Cluster Secret Key"
  type        = string
  default     = ""
}

variable "airflow_base_url" {
  description = "Airflow Base URL"
  type        = string
}

variable "ingress_class_name" {
  description = "Ingress Class Name"
  type        = string
}

variable "host_name" {
  description = "host url"
  type        = string
}

variable "db_user" {
  description = "Database User"
  type        = string
  default     = "airflow_user"
}

variable "db_secret" {
  description = "Database Secret"
  type        = string
  default     = "airflow-secrets"
}

variable "efs_data_sync_schedule" {
  description = "EFS Data Sync"
  type        = string
  default     = "cron(0/5 * * * ? *)"
}

variable "okta_app_config" {
  description = "OKTA Application Config"
  type = object({
    okta_app_name             = string
    login_uri                 = string
    post_logout_redirect_uris = list(string)
    redirect_uris             = list(string)
    logo_uri                  = string
  })
}

variable "group_list" {
  type        = list(any)
  description = "List of groups"
  default     = []
}

variable "sub_domain" {
  type        = string
  description = "Sub Domain"
}

variable "airflow_image_version" {
  type        = string
  description = "acme-devops-docker-local.jfrog.io/acme/airflowimage-base container version"
}

variable "airflow_core_parallelism" {
  type        = number
  default     = 128
  description = "This defines the maximum number of task instances that can run concurrently per scheduler in Airflow, regardless of the worker count. Generally this value, multiplied by the number of schedulers in your cluster, is the maximum number of task instances with the running state in the metadata database. Airflow default value is 32"
}
