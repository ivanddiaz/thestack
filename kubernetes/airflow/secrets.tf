
resource "aws_secretsmanager_secret" "airflow_credential" {
  name        = "${var.environment}/airflow/credential"
  description = "Airflow Credential Information"
}

resource "aws_secretsmanager_secret_policy" "airflow_credential_policy" {
  secret_arn = aws_secretsmanager_secret.airflow_credential.arn
  policy     = <<POLICY
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Deny",
                "Principal": "*",
                "Action": [
                    "secretsmanager:GetSecretValue"
                ],
                "Resource": [
                    "*"
                ],
                "Condition": {
                    "StringNotLike": {
                        "aws:userId": [
                           "${data.aws_iam_role.dev_admin_role.unique_id}:*"
                        ]
                    }
                }
            }
        ]
    }
POLICY
}

resource "aws_secretsmanager_secret_version" "airflow_credential_version" {
  secret_id     = aws_secretsmanager_secret.airflow_credential.id
  secret_string = jsonencode(local.airflow_credential)
}

resource "kubernetes_secret" "data_secrets" {
  metadata {
    name      = "airflow-secrets"
    namespace = local.namespace
  }
  type = "Opaque"
  data = {
    postgresql-password = jsondecode(data.aws_secretsmanager_secret_version.airflow_db_pwd.secret_string)["password"]
    airflow-password    = random_password.airflow_password.result
    airflow-fernetKey   = random_password.fernet_key.result
  }
}


module "secret" {
  source                      = "..//aws/secrets"
  secret_name                 = "airflow/api"
  secret_recovery_window_days = 0
  terraform_user              = "IAC_USER"
  environment                 = var.environment
  secrets = {
    description = "Pelican API Access"
  }
  resource_secrets = {
    read-only-user     = random_string.api_viewer_username.result
    read-only-password = random_password.api_viewer_password.result
    endpoint           = "http://${local.cluster_endpoint_web_alias}:8080/api/v1"
  }
}


resource "random_string" "api_viewer_username" {
  length  = 23
  special = false
}

resource "random_password" "api_viewer_password" {
  length           = 53
  special          = true
  override_special = "-_=+"
}
