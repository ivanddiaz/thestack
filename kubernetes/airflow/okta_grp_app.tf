resource "okta_app_oauth" "okta_airflow_app" {
  label          = var.okta_app_config.okta_app_name
  type           = "web"
  consent_method = "REQUIRED"
  login_mode     = "SPEC"
  login_scopes   = []
  login_uri      = var.okta_app_config.login_uri
  grant_types = [
    "client_credentials", "implicit", "authorization_code", "refresh_token"
  ]
  redirect_uris             = var.okta_app_config.redirect_uris
  post_logout_redirect_uris = var.okta_app_config.post_logout_redirect_uris
  response_types            = ["code", "token", "id_token"]
  logo                      = var.okta_app_config.logo_uri

  hide_ios = false
  hide_web = false
  groups_claim {
    type        = "FILTER"
    filter_type = "REGEX"
    name        = "groups"
    value       = ".*"
  }
}
resource "okta_group" "airflow_group" {
  for_each    = { for group in var.group_list : group.name => group }
  name        = each.key
  description = each.value.description
}
resource "okta_app_group_assignment" "group_assignment" {
  for_each = { for group in var.group_list : group.name => group }
  app_id   = okta_app_oauth.okta_airflow_app.id
  group_id = okta_group.airflow_group[each.key].id
}
