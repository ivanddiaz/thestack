variable "environment" {
  type        = string
  description = "Environment name dev/qa/uat/prd"
}
variable "namespace" {
  type        = string
  default     = "monitoring"
  description = "Kubernetes namespace to use/create"
}
variable "stack_name" {
  type        = string
  default     = "prometheus-stack"
  description = "Name of the stack"
}
variable "chart_version" {
  type        = string
  default     = "51.2.0"
  description = "Helm chart version"
}
variable "helm_repository" {
  type        = string
  default     = "https://prometheus-community.github.io/helm-charts"
  description = "Helm repository"
}
variable "helm_chart" {
  type        = string
  default     = "kube-prometheus-stack"
  description = "Helm chart"
}
variable "timeout" {
  type        = number
  default     = 300
  description = "Timeout for helm chart"
}
variable "product_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "grafana_enabled" {
  type        = bool
  default     = false
  description = "Enable grafana into the k8s stack"
}

variable "alert_manager_enabled" {
  type        = bool
  default     = false
  description = "Enable alert manager into the k8s stack"
}

variable "cluster_secret_key" {
  description = "EKS Cluster Secret Key"
  type        = string
  default     = ""
}
