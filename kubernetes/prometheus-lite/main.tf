resource "helm_release" "prometheus-stack" {
  name       = var.stack_name
  chart      = var.helm_chart
  repository = var.helm_repository
  namespace  = var.namespace
  version    = var.chart_version
  values = [templatefile("./config/prometheus_values.yml", {
    alert_manager_enabled             = var.alert_manager_enabled
    grafana_enabled                   = var.grafana_enabled
    cni_host_network                  = local.cni_host_network
    prometheus_operator_service_type  = local.cni_host_network == "false" ? "ClusterIP" : "NodePort"
    prometheus_operator_internal_port = local.cni_host_network == "false" ? 10250 : 10252
  })]
  timeout = var.timeout
}
