data "aws_secretsmanager_secret" "cluster_info" {
  name = local.cluster_secret_key
}

data "aws_secretsmanager_secret_version" "cluster_info" {
  secret_id = data.aws_secretsmanager_secret.cluster_info.id
}

data "aws_eks_cluster" "cluster" {
  name = local.cluster_name
}

data "aws_eks_cluster_auth" "cluster_auth" {
  name = data.aws_eks_cluster.cluster.name
}

locals {
  cluster_secret_key = (var.cluster_secret_key != "") ? var.cluster_secret_key : "${var.environment}/eks/cluster-info"
  cluster_name       = jsondecode(data.aws_secretsmanager_secret_version.cluster_info.secret_string)["cluster_name"]
  cni_host_network   = jsondecode(data.aws_secretsmanager_secret_version.cluster_info.secret_string)["cni_host_network"]
}