terraform {
  required_version = "~> 1.1.7"
  required_providers {
    aws = "~> 4.5.0"
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14.0"
    }
  }
}
