resource "kubectl_manifest" "aws_auth_map" {
  force_new = true
  /*depends_on = [
    aws_eks_cluster.eks_cluster
  ]*/
  yaml_body = <<YAML
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${var.node_role_arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
    - rolearn: ${data.aws_iam_role.dev_admin_role.arn}
      username: eks-admin-user
      groups:
        - system:masters
    - rolearn: ${data.aws_iam_role.developer_role.arn}
      username: eks-developer-user
    - rolearn: ${data.aws_iam_role.reader_role.arn}
      username: eks-read-user
YAML
}