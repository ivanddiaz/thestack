variable "cluster_name" {
  type        = string
  description = "cluster name"
}

variable "node_role_arn" {
  type        = string
  description = "Node Role ARN"
}