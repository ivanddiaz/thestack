locals {
  dev_admin_role_name = "DEVADMIN"
  developer_role_name = "DEVELOPER"
  reader_role_name    = "READONLY"
}

data "aws_eks_cluster" "cluster" {
  name = var.cluster_name
}

data "aws_eks_cluster_auth" "cluster_auth" {
  name = data.aws_eks_cluster.cluster.name
}

data "aws_iam_role" "dev_admin_role" {
  name = local.dev_admin_role_name
}

data "aws_iam_role" "developer_role" {
  name = local.developer_role_name
}

data "aws_iam_role" "reader_role" {
  name = local.reader_role_name
}