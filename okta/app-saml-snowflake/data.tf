data "aws_secretsmanager_secret" "okta" {
  name = var.okta_api_token_secret
}

data "aws_secretsmanager_secret_version" "okta" {
  secret_id = data.aws_secretsmanager_secret.okta.id
}

data "aws_secretsmanager_secret" "snowflake-credential-metadata" {
  name = var.snowflake_credential_name
}

data "aws_secretsmanager_secret_version" "snowflake-credential" {
  secret_id = data.aws_secretsmanager_secret.snowflake-credential-metadata.id
}

data "aws_ssm_parameter" "oauth_refresh_token" {
  name = var.snowflake_refresh_token_key
}

# Future: If OKTA release a provisionin API this will help
# data "snowflake_system_generate_scim_access_token" "scim" {
#   integration_name = "OKTA_PROVISIONING"
# }

data "snowflake_current_account" "this" {}
