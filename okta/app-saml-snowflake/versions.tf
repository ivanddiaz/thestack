terraform {
  required_providers {
    okta = {
      source  = "okta/okta"
      version = "~> 3.42.0"
    }
    snowflake = {
      source  = "Snowflake-Labs/snowflake"
      version = "~> 0.56.3"
    }
  }
}