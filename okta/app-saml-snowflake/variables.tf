variable "okta_org_name" {
  type        = string
  description = "Okta org name"
  default     = "acme"
}

variable "okta_base_url" {
  type        = string
  description = "Okta base url"
  default     = "okta.com"
}

variable "environment" {
  type        = string
  description = "Environment"
}

variable "saml_app_hide_ios" {
  type        = bool
  default     = false
  description = "Do not display application icon on mobile app"
}

variable "saml_app_auto_submit_toolbar" {
  type        = bool
  default     = true
  description = "Display auto submit toolbar"
}

variable "saml_app_skip_users" {
  type        = bool
  default     = true
  description = "Indicator that allows the app to skip users sync"
}

variable "saml_app_skip_groups" {
  type        = bool
  default     = true
  description = "Indicator that allows the app to skip groups sync"
}

variable "saml_preconfigured_app" {
  type        = string
  default     = "snowflake"
  description = "Name of application from the Okta Integration Network"
}

variable "snowflake_user" {
  type        = string
  description = "Snowflake user name"
}

variable "snowflake_account" {
  type        = string
  description = "Snowflake user name"
}

variable "snowflake_role" {
  type        = string
  description = "Snowflake Role Name"
}

variable "snowflake_region" {
  type        = string
  description = "Snowflake Region"
}

variable "snowflake_credential_name" {
  type        = string
  description = "Snowflake Credential Name which is stored in AWS secret manager"
}

variable "snowflake_refresh_token_key" {
  type        = string
  description = "Snowflake Refresh Token SSM Parameter key"
}

variable "okta_api_token_secret" {
  type        = string
  default     = "/acme/okta/api/token"
  description = "Secret for OKTA API access"
}
