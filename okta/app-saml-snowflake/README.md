<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_okta"></a> [okta](#requirement\_okta) | ~> 3.42.0 |
| <a name="requirement_snowflake"></a> [snowflake](#requirement\_snowflake) | ~> 0.56.3 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_okta"></a> [okta](#provider\_okta) | ~> 3.42.0 |
| <a name="provider_snowflake"></a> [snowflake](#provider\_snowflake) | ~> 0.56.3 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [okta_app_saml.saml_app](https://registry.terraform.io/providers/okta/okta/latest/docs/resources/app_saml) | resource |
| [aws_secretsmanager_secret.okta](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret.snowflake-credential-metadata](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.okta](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [aws_secretsmanager_secret_version.snowflake-credential](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [aws_ssm_parameter.oauth_refresh_token](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ssm_parameter) | data source |
| [snowflake_current_account.this](https://registry.terraform.io/providers/Snowflake-Labs/snowflake/latest/docs/data-sources/current_account) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_environment"></a> [environment](#input\_environment) | Environment | `string` | n/a | yes |
| <a name="input_okta_api_token_secret"></a> [okta\_api\_token\_secret](#input\_okta\_api\_token\_secret) | Secret for OKTA API access | `string` | `"/acme/okta/api/token"` | no |
| <a name="input_okta_base_url"></a> [okta\_base\_url](#input\_okta\_base\_url) | Okta base url | `string` | `"okta.com"` | no |
| <a name="input_okta_org_name"></a> [okta\_org\_name](#input\_okta\_org\_name) | Okta org name | `string` | `"acme"` | no |
| <a name="input_saml_app_auto_submit_toolbar"></a> [saml\_app\_auto\_submit\_toolbar](#input\_saml\_app\_auto\_submit\_toolbar) | Display auto submit toolbar | `bool` | `true` | no |
| <a name="input_saml_app_hide_ios"></a> [saml\_app\_hide\_ios](#input\_saml\_app\_hide\_ios) | Do not display application icon on mobile app | `bool` | `false` | no |
| <a name="input_saml_app_skip_groups"></a> [saml\_app\_skip\_groups](#input\_saml\_app\_skip\_groups) | Indicator that allows the app to skip groups sync | `bool` | `true` | no |
| <a name="input_saml_app_skip_users"></a> [saml\_app\_skip\_users](#input\_saml\_app\_skip\_users) | Indicator that allows the app to skip users sync | `bool` | `true` | no |
| <a name="input_saml_preconfigured_app"></a> [saml\_preconfigured\_app](#input\_saml\_preconfigured\_app) | Name of application from the Okta Integration Network | `string` | `"snowflake"` | no |
| <a name="input_snowflake_account"></a> [snowflake\_account](#input\_snowflake\_account) | Snowflake user name | `string` | n/a | yes |
| <a name="input_snowflake_credential_name"></a> [snowflake\_credential\_name](#input\_snowflake\_credential\_name) | Snowflake Credential Name which is stored in AWS secret manager | `string` | n/a | yes |
| <a name="input_snowflake_refresh_token_key"></a> [snowflake\_refresh\_token\_key](#input\_snowflake\_refresh\_token\_key) | Snowflake Refresh Token SSM Parameter key | `string` | n/a | yes |
| <a name="input_snowflake_region"></a> [snowflake\_region](#input\_snowflake\_region) | Snowflake Region | `string` | n/a | yes |
| <a name="input_snowflake_role"></a> [snowflake\_role](#input\_snowflake\_role) | Snowflake Role Name | `string` | n/a | yes |
| <a name="input_snowflake_user"></a> [snowflake\_user](#input\_snowflake\_user) | Snowflake user name | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_certificate"></a> [certificate](#output\_certificate) | n/a |
| <a name="output_entity_url"></a> [entity\_url](#output\_entity\_url) | n/a |
| <a name="output_id"></a> [id](#output\_id) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
