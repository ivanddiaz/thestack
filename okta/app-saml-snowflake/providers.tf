provider "okta" {
  org_name  = jsondecode(data.aws_secretsmanager_secret_version.okta.secret_string)["okta_org_name"]
  base_url  = jsondecode(data.aws_secretsmanager_secret_version.okta.secret_string)["okta_base_url"]
  api_token = jsondecode(data.aws_secretsmanager_secret_version.okta.secret_string)["api_token"]
}

provider "snowflake" {
  username            = var.snowflake_user
  account             = var.snowflake_account
  region              = var.snowflake_region
  role                = var.snowflake_role
  oauth_client_id     = jsondecode(data.aws_secretsmanager_secret_version.snowflake-credential.secret_string)["SNOWFLAKE_OAUTH_CLIENT_ID"]
  oauth_client_secret = jsondecode(data.aws_secretsmanager_secret_version.snowflake-credential.secret_string)["SNOWFLAKE_OAUTH_CLIENT_SECRET"]
  oauth_refresh_token = data.aws_ssm_parameter.oauth_refresh_token.value
  oauth_endpoint      = jsondecode(data.aws_secretsmanager_secret_version.snowflake-credential.secret_string)["SNOWFLAKE_OAUTH_ENDPOINT"]
  oauth_redirect_url  = jsondecode(data.aws_secretsmanager_secret_version.snowflake-credential.secret_string)["SNOWFLAKE_OAUTH_REDIRECT_URL"]
}
