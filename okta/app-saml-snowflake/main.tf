locals {
  saml_app_env   = upper(var.environment)
  saml_app_label = "Snowflake-${local.saml_app_env}"
  saml_preconfigured_app_settings = jsonencode({
    subDomain = lower("${data.snowflake_current_account.this.account}.${var.snowflake_region}")
  })
}

resource "okta_app_saml" "saml_app" {
  preconfigured_app   = var.saml_preconfigured_app
  label               = local.saml_app_label
  hide_ios            = var.saml_app_hide_ios
  auto_submit_toolbar = var.saml_app_auto_submit_toolbar
  skip_users          = var.saml_app_skip_users
  skip_groups         = var.saml_app_skip_groups
  app_settings_json   = local.saml_preconfigured_app_settings

}



output "id" {
  value = okta_app_saml.saml_app.id
}

output "certificate" {
  value = okta_app_saml.saml_app.certificate
}

output "entity_url" {
  value = okta_app_saml.saml_app.entity_url
}

