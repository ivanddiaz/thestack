variable "okta_api_token_secret" {
  type        = string
  default     = "/acme/okta/api/token"
  description = "Secret for OKTA API access"
}

variable "environment" {
  type        = string
  description = "Environment"
}

variable "snowflake_account" {
  type        = string
  description = "Snowflake Account"
}

variable "company_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "acme"
}

variable "product_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "terraform_user" {
  type        = string
  description = "Terraform User"
  default     = "DEVADMIN"
}

variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
  default     = "coyote@acme.com"
}

variable "stack_for_tagging" {
  type        = string
  description = "The stack that owns the resource for tagging"
  default     = ""
}
