data "aws_secretsmanager_secret" "okta" {
  name = var.okta_api_token_secret
}

data "aws_secretsmanager_secret_version" "okta" {
  secret_id = data.aws_secretsmanager_secret.okta.id
}
