terraform {
  required_providers {
    okta = {
      source  = "okta/okta"
      version = "4.1.0"
    }
  }
}
