locals {
  app_name           = "Snowflake-${upper(var.environment)}-OAUTH"
  snowflake_url      = "https://${var.snowflake_account}.us-east-1.snowflakecomputing.com"
  policy_name        = "snowflake_${var.environment}_policy"
  policy_description = "Snowflake ${var.environment} OAuth App Policy"
  rule_name          = "snowflae_${var.environment}_rule"
  auth_server        = "snowflae_${var.environment}_auth"
  auth_server_desc   = "This is for Snowflake ${var.environment} account OAUTH authorization"
  secret_name        = "/snowflake/oauth-app"
}


resource "okta_app_oauth" "snowflake_app" {
  label = local.app_name
  type  = "native"
  app_links_json = jsonencode(
    {
      oidc_client_link = true
    }
  )
  pkce_required              = false
  grant_types                = ["authorization_code", "password", "refresh_token"]
  implicit_assignment        = false
  issuer_mode                = "DYNAMIC"
  token_endpoint_auth_method = "client_secret_basic"
  redirect_uris              = [local.snowflake_url]
  refresh_token_leeway       = 0
  refresh_token_rotation     = "STATIC"
  response_types = [
    "code",
  ]
}

resource "okta_auth_server" "auth_server" {
  name                      = local.auth_server
  audiences                 = [local.snowflake_url]
  description               = local.auth_server_desc
  issuer_mode               = "ORG_URL"
  credentials_rotation_mode = "AUTO"
}

resource "okta_auth_server_scope" "auth_server_scope" {
  auth_server_id = okta_auth_server.auth_server.id
  name           = "session:role-any"
  consent        = "IMPLICIT"
}

resource "okta_auth_server_policy" "auth_server_policy" {
  auth_server_id   = okta_auth_server.auth_server.id
  name             = local.policy_name
  description      = local.policy_description
  client_whitelist = ["ALL_CLIENTS"]
  priority         = 1
}

resource "okta_auth_server_policy_rule" "policy_rule" {
  auth_server_id       = okta_auth_server.auth_server.id
  policy_id            = okta_auth_server_policy.auth_server_policy.id
  status               = "ACTIVE"
  name                 = local.rule_name
  priority             = 1
  grant_type_whitelist = ["client_credentials", "password"]
  group_whitelist = [
    "EVERYONE",
  ]
  scope_whitelist = [
    "*",
  ]
  access_token_lifetime_minutes = 600
  type                          = "RESOURCE_ACCESS"
}


/*module "secret" {
  source                      = "../../acme/aws/secrets"
  secret_name                 = local.secret_name
  secret_recovery_window_days = 0
  company_name                = var.company_name
  environment                 = var.environment
  product_name                = var.product_name
  email_for_tagging           = var.email_for_tagging
  stack_for_tagging           = var.stack_for_tagging
  terraform_user              = var.terraform_user
  secrets                     = {
    description = "Snowflake OKTA OAUTH App"
  }
  resource_secrets = {
    SNOWFLAKE_OAUTH_CLIENT_ID     = okta_app_oauth.snowflake_app.id
    SNOWFLAKE_OAUTH_CLIENT_SECRET = okta_app_oauth.snowflake_app.client_secret
    SNOWFLAKE_OAUTH_ENDPOINT      = "${okta_auth_server.auth_server.issuer}/v1/token"
    SNOWFLAKE_OAUTH_REDIRECT_URL  = local.snowflake_url
    AUTHORIZATION                 = format("Basic %s",base64encode("${okta_app_oauth.snowflake_app.id}:${okta_app_oauth.snowflake_app.client_secret}"))
  }
}*/



#//aws secretsmanager delete-secret --force-delete-without-recovery --secret-id  /secret/application/
