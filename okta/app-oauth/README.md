<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_okta"></a> [okta](#requirement\_okta) | ~> 3.42.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_http"></a> [http](#provider\_http) | n/a |
| <a name="provider_okta"></a> [okta](#provider\_okta) | ~> 3.42.0 |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_secret"></a> [secret](#module\_secret) | ../../aws/secrets | n/a |

## Resources

| Name | Type |
|------|------|
| [okta_app_oauth.oauth_app](https://registry.terraform.io/providers/okta/okta/latest/docs/resources/app_oauth) | resource |
| [random_pet.pet](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/pet) | resource |
| [aws_secretsmanager_secret.okta](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.okta](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [http_http.openid_config](https://registry.terraform.io/providers/hashicorp/http/latest/docs/data-sources/http) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_company_name"></a> [company\_name](#input\_company\_name) | For bucket name as per naming standard | `string` | `"acme"` | no |
| <a name="input_email_for_tagging"></a> [email\_for\_tagging](#input\_email\_for\_tagging) | The email address of a group or team that owns the resource for tagging | `string` | `"coyote@acme.com"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | For bucket name as per naming standard | `string` | `"testing"` | no |
| <a name="input_issuer_ORG_URL"></a> [issuer\_ORG\_URL](#input\_issuer\_ORG\_URL) | This value is necessary for browser API access, okta provider doesn't give an output for this | `string` | `"https://acme.okta.com"` | no |
| <a name="input_label_prefix"></a> [label\_prefix](#input\_label\_prefix) | The Application's display name prefix | `string` | `null` | no |
| <a name="input_login_uri"></a> [login\_uri](#input\_login\_uri) | URI that initiates login. | `string` | `"https://example.com/login/okta"` | no |
| <a name="input_logo"></a> [logo](#input\_logo) | Local file path to the logo. The file must be in PNG, JPG, or GIF format, and less than 1 MB in size. | `string` | `null` | no |
| <a name="input_okta_api_token"></a> [okta\_api\_token](#input\_okta\_api\_token) | Okta API token | `string` | `null` | no |
| <a name="input_okta_base_url"></a> [okta\_base\_url](#input\_okta\_base\_url) | Okta base url | `string` | `"okta.com"` | no |
| <a name="input_okta_org_name"></a> [okta\_org\_name](#input\_okta\_org\_name) | Okta org name | `string` | `"acme"` | no |
| <a name="input_product_name"></a> [product\_name](#input\_product\_name) | For bucket name as per naming standard | `string` | `"project"` | no |
| <a name="input_redirect_uris"></a> [redirect\_uris](#input\_redirect\_uris) | (Optional) List of URIs for use in the redirect-based flow. This is required for all application types except service. | `list(any)` | `[]` | no |
| <a name="input_stack_for_tagging"></a> [stack\_for\_tagging](#input\_stack\_for\_tagging) | The stack that owns the resource for tagging | `string` | `""` | no |
| <a name="input_terraform_user"></a> [terraform\_user](#input\_terraform\_user) | Terraform User | `string` | `"IAC_USER"` | no |
| <a name="input_well_known_openid_config_path"></a> [well\_known\_openid\_config\_path](#input\_well\_known\_openid\_config\_path) | Well Known OpenID Connect provider path | `string` | `"/oauth2/default/.well-known/openid-configuration"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_api_url"></a> [api\_url](#output\_api\_url) | n/a |
| <a name="output_auth_url"></a> [auth\_url](#output\_auth\_url) | n/a |
| <a name="output_client_id"></a> [client\_id](#output\_client\_id) | n/a |
| <a name="output_client_secret"></a> [client\_secret](#output\_client\_secret) | n/a |
| <a name="output_id"></a> [id](#output\_id) | n/a |
| <a name="output_label"></a> [label](#output\_label) | n/a |
| <a name="output_okta_base_url"></a> [okta\_base\_url](#output\_okta\_base\_url) | n/a |
| <a name="output_okta_org_name"></a> [okta\_org\_name](#output\_okta\_org\_name) | n/a |
| <a name="output_openid_connect_url"></a> [openid\_connect\_url](#output\_openid\_connect\_url) | n/a |
| <a name="output_token_url"></a> [token\_url](#output\_token\_url) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
