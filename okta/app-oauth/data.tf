data "aws_secretsmanager_secret" "okta" {
  name = lower("/acme/okta/api/token")
}

data "aws_secretsmanager_secret_version" "okta" {
  secret_id = data.aws_secretsmanager_secret.okta.id
}

data "http" "openid_config" {
  url = local.openid_connect_url
  # Optional request headers
  request_headers = {
    Accept = "application/json"
  }
}
