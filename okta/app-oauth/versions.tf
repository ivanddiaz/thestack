terraform {
  required_providers {
    okta = {
      source  = "okta/okta"
      version = "~> 3.42.0"
    }
  }
}
