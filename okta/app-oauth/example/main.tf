module "okta_oauth_app" {
  source         = "../"
  label_prefix   = "dostuff"
  product_name   = "unicorn"
  environment    = "testing"
  okta_org_name  = "valence"
  okta_base_url  = "okta.com"
  okta_api_token = "******************************************"
  logo           = "example-logo.png"
}
