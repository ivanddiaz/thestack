locals {
  pet                = var.label_prefix != null ? title(var.label_prefix) : title(random_pet.pet.id)
  product_name       = title(var.product_name)
  environment        = title(var.environment)
  label              = "${local.pet}-${local.product_name}${local.environment}"
  tolist_login_uri   = [var.login_uri]
  redirect_uris      = setunion(local.tolist_login_uri, var.redirect_uris)
  secret_prefix      = "okta"
  secret_suffix      = var.label_prefix != null ? var.label_prefix : random_pet.pet.id
  secret_name        = "${local.secret_prefix}/${local.secret_suffix}"
  company_name       = lower(var.company_name)
  openid_connect_url = "${var.issuer_ORG_URL}${var.well_known_openid_config_path}"
  openid_config      = jsondecode(data.http.openid_config.response_body)
  auth_url           = local.openid_config.authorization_endpoint
  token_url          = local.openid_config.token_endpoint
  api_url            = local.openid_config.userinfo_endpoint
}

resource "random_pet" "pet" {
  length = 1
}

resource "okta_app_oauth" "oauth_app" {
  label          = local.label
  type           = "web"
  consent_method = "REQUIRED"
  login_mode     = "SPEC"
  login_scopes   = ["openid", "profile", "email", "address", "phone"]
  login_uri      = var.login_uri
  grant_types    = ["client_credentials", "implicit", "authorization_code", "refresh_token"]
  redirect_uris  = local.redirect_uris
  response_types = ["code", "token", "id_token"]
  logo           = var.logo
  hide_ios       = false
  hide_web       = false
  issuer_mode    = "ORG_URL"
  skip_users     = true
  skip_groups    = true
}

module "secret" {
  source                      = "../../aws/secrets"
  secret_name                 = local.secret_name
  secret_recovery_window_days = 0
  company_name                = var.company_name
  environment                 = var.environment
  product_name                = var.product_name
  email_for_tagging           = var.email_for_tagging
  stack_for_tagging           = var.stack_for_tagging
  terraform_user              = var.terraform_user
  secrets = {
    description = "OKTA Oauth integration details"
  }
  resource_secrets = {
    id                 = okta_app_oauth.oauth_app.id
    label              = okta_app_oauth.oauth_app.label
    client_id          = okta_app_oauth.oauth_app.client_id
    client_secret      = okta_app_oauth.oauth_app.client_secret
    issuer_ORG_URL     = var.issuer_ORG_URL
    auth_url           = local.auth_url
    token_url          = local.token_url
    api_url            = local.api_url
    openid_connect_url = local.openid_connect_url
    okta_org_name      = var.okta_org_name
    okta_base_url      = var.okta_base_url
  }
}

output "id" {
  value = okta_app_oauth.oauth_app.id
}
output "label" {
  value = okta_app_oauth.oauth_app.label
}
output "client_id" {
  value     = okta_app_oauth.oauth_app.client_id
  sensitive = true
}

output "client_secret" {
  value     = okta_app_oauth.oauth_app.client_secret
  sensitive = true
}
output "auth_url" {
  value = local.auth_url
}

output "token_url" {
  value = local.token_url
}

output "api_url" {
  value = local.api_url
}

output "openid_connect_url" {
  value = local.openid_connect_url
}

output "okta_org_name" {
  value = var.okta_org_name
}

output "okta_base_url" {
  value = var.okta_base_url
}