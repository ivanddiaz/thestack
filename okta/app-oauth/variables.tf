variable "okta_org_name" {
  type        = string
  description = "Okta org name"
  default     = "acme"
}

variable "okta_base_url" {
  type        = string
  description = "Okta base url"
  default     = "okta.com"
}

variable "okta_api_token" {
  type        = string
  description = "Okta API token"
  default     = null
}

variable "label_prefix" {
  type        = string
  description = "The Application's display name prefix"
  default     = null
}

variable "product_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}

variable "company_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "acme"
}

variable "terraform_user" {
  type        = string
  description = "Terraform User"
  default     = "IAC_USER"
}

variable "email_for_tagging" {
  type        = string
  description = "The email address of a group or team that owns the resource for tagging"
  default     = "coyote@acme.com"
}

variable "stack_for_tagging" {
  type        = string
  description = "The stack that owns the resource for tagging"
  default     = ""
}

variable "environment" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "testing"
}

variable "logo" {
  type        = string
  description = "Local file path to the logo. The file must be in PNG, JPG, or GIF format, and less than 1 MB in size."
  default     = null
}

variable "login_uri" {
  type        = string
  description = "URI that initiates login."
  default     = "https://example.com/login/okta"
}

variable "redirect_uris" {
  type        = list(any)
  description = "(Optional) List of URIs for use in the redirect-based flow. This is required for all application types except service."
  default     = []
}

variable "issuer_ORG_URL" {
  type        = string
  description = "This value is necessary for browser API access, okta provider doesn't give an output for this"
  default     = "https://acme.okta.com"
}

variable "well_known_openid_config_path" {
  type        = string
  description = "Well Known OpenID Connect provider path"
  default     = "/oauth2/default/.well-known/openid-configuration"
}
