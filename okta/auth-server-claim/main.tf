resource "okta_auth_server_claim" "claim" {
  auth_server_id    = data.okta_auth_server.server.id
  name              = var.claim_name
  claim_type        = var.claim_type
  value_type        = var.value_type
  group_filter_type = var.group_filter_type
  value             = var.value
}
