variable "okta_org_name" {
  type        = string
  description = "Okta org name"
  default     = "acme"
}

variable "okta_base_url" {
  type        = string
  description = "Okta base url"
  default     = "okta.com"
}

variable "environment" {
  type        = string
  description = "Environment"
}

variable "okta_api_token_secret" {
  type        = string
  default     = "/acme/okta/api/token"
  description = "Secret for OKTA API access"
}

variable "okta_auth_server_name" {
  type        = string
  default     = "default"
  description = "OKTA Authorization server name (Security API)"
}
variable "claim_name" {
  type        = string
  description = "The name of the claim."
}

variable "claim_type" {
  type        = string
  description = "Specifies whether the claim is for an access token RESOURCE or ID token IDENTITY."
  validation {
    condition     = can(regex("^(RESOURCE|IDENTITY)$", var.claim_type))
    error_message = "Must be RESOURCE or IDENTITY."
  }
}

variable "value_type" {
  type        = string
  description = "The type of value of the claim. It can be set to EXPRESSION or GROUPS."
  validation {
    condition     = can(regex("^(EXPRESSION|GROUPS)$", var.value_type))
    error_message = "Must be EXPRESSION or GROUPS."
  }
}

variable "value" {
  type        = string
  description = "The value of the claim."
}

variable "group_filter_type" {
  type        = string
  description = "Can be set to one of the following STARTS_WITH, EQUALS, CONTAINS, REGEX."
  validation {
    condition     = can(regex("^(STARTS_WITH|EQUALS|CONTAINS|REGEX)$", var.group_filter_type))
    error_message = "Must be STARTS_WITH, EQUALS, CONTAINS or REGEX."
  }
}
