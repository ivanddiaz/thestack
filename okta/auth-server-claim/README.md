<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_okta"></a> [okta](#requirement\_okta) | ~> 3.42.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_okta"></a> [okta](#provider\_okta) | ~> 3.42.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [okta_auth_server_claim.claim](https://registry.terraform.io/providers/okta/okta/latest/docs/resources/auth_server_claim) | resource |
| [aws_secretsmanager_secret.okta](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret) | data source |
| [aws_secretsmanager_secret_version.okta](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/secretsmanager_secret_version) | data source |
| [okta_auth_server.server](https://registry.terraform.io/providers/okta/okta/latest/docs/data-sources/auth_server) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_claim_name"></a> [claim\_name](#input\_claim\_name) | The name of the claim. | `string` | n/a | yes |
| <a name="input_claim_type"></a> [claim\_type](#input\_claim\_type) | Specifies whether the claim is for an access token RESOURCE or ID token IDENTITY. | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment | `string` | n/a | yes |
| <a name="input_group_filter_type"></a> [group\_filter\_type](#input\_group\_filter\_type) | Can be set to one of the following STARTS\_WITH, EQUALS, CONTAINS, REGEX. | `string` | n/a | yes |
| <a name="input_okta_api_token_secret"></a> [okta\_api\_token\_secret](#input\_okta\_api\_token\_secret) | Secret for OKTA API access | `string` | `"/acme/okta/api/token"` | no |
| <a name="input_okta_auth_server_name"></a> [okta\_auth\_server\_name](#input\_okta\_auth\_server\_name) | OKTA Authorization server name (Security API) | `string` | `"default"` | no |
| <a name="input_okta_base_url"></a> [okta\_base\_url](#input\_okta\_base\_url) | Okta base url | `string` | `"okta.com"` | no |
| <a name="input_okta_org_name"></a> [okta\_org\_name](#input\_okta\_org\_name) | Okta org name | `string` | `"acme"` | no |
| <a name="input_value"></a> [value](#input\_value) | The value of the claim. | `string` | n/a | yes |
| <a name="input_value_type"></a> [value\_type](#input\_value\_type) | The type of value of the claim. It can be set to EXPRESSION or GROUPS. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
