provider "okta" {
  org_name  = var.okta_org_name
  base_url  = var.okta_base_url
  api_token = jsondecode(data.aws_secretsmanager_secret_version.okta.secret_string)["api_token"]
}