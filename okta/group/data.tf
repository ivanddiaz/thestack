data "aws_secretsmanager_secret" "okta" {
  name = lower("/acme/okta/api/token")
}

data "aws_secretsmanager_secret_version" "okta" {
  secret_id = data.aws_secretsmanager_secret.okta.id
}
