variable "environment" {
  type        = string
  description = "Environment name dev/qa/uat/prd"
}

variable "product_name" {
  type        = string
  description = "For bucket name as per naming standard"
  default     = "project"
}
variable "okta_org_name" {
  type        = string
  description = "Okta org name"
  default     = "acme"
}

variable "okta_base_url" {
  type        = string
  description = "Okta base url"
  default     = "okta.com"
}

variable "okta_api_token" {
  type        = string
  description = "Okta API token"
  default     = null
}

variable "group_name" {
  type        = string
  description = "Name of the okta group"
  default     = null
}

variable "group_description" {
  type        = string
  description = "Description of the okta group"
  default     = null
}

variable "app_id" {
  type        = string
  description = "The ID of the application to assign a group to"
}
