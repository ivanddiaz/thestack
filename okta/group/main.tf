locals {
  group_name = var.group_name != "" ? var.group_name : random_pet.pet.id
}

resource "random_pet" "pet" {
  length = 2
}

resource "okta_group" "group" {
  name        = local.group_name
  description = var.group_description
  skip_users  = true
}

resource "okta_app_group_assignment" "assignment" {
  app_id   = var.app_id
  group_id = okta_group.group.id
}

output "group_name" {
  value = local.group_name
}